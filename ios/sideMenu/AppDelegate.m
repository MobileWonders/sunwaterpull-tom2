
#import "AppDelegate.h"
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>

@import Firebase;
@implementation AppDelegate

@synthesize oneSignal = _oneSignal;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  NSURL *jsCodeLocation;
  [self setupPushNotficiation];
  jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index.ios" fallbackResource:nil];

  self.oneSignal = [[RCTOneSignal alloc] initWithLaunchOptions:launchOptions appId:@"72c2b469-cc57-47a5-ab1a-87090ea3a95f" settings:@{kOSSettingsKeyAutoPrompt: @false}];

  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"sideMenu"
                                               initialProperties:nil
                                                   launchOptions:launchOptions];
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  return YES;
}
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
  return YES;
}

- (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection
{
  return YES;
}


-(void) setupPushNotficiation {
  if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
    UIUserNotificationType allNotificationTypes =
    (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
    UIUserNotificationSettings *settings =
    [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
  } else {
    // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
    // For iOS 10 display notification (sent via APNS)
    [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    UNAuthorizationOptions authOptions =
    UNAuthorizationOptionAlert
    | UNAuthorizationOptionSound
    | UNAuthorizationOptionBadge;
    [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
    }];
#endif
  }
  NSString *fcmToken = [FIRMessaging messaging].FCMToken;
  NSLog(@"FCM registration token: %@", fcmToken);
  [[UIApplication sharedApplication] registerForRemoteNotifications];
}


- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
  // Note that this callback will be fired everytime a new token is generated, including the first
  // time. So if you need to retrieve the token as soon as it is available this is where that
  // should be done.

  NSLog(@"FCM registration token: %@", fcmToken);

  // TODO: If necessary send token to application server.
}


- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
//  [FIRMessaging messaging]
}

- (void)goNativeStoryboard {

  UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
  UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"viewController"];
  vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;// UIModalTransitionStyleFlipHorizontal;
  [self.window.rootViewController presentViewController:vc animated:YES completion:NULL];

}

- (void)goNative {
  UIStoryboard *sb = [UIStoryboard storyboardWithName:@"threedeestoryboard" bundle:nil];
  UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"viewcontroller3d"];
  vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
  [self.window.rootViewController presentViewController:vc animated:YES completion:NULL];
}

-(void) gotoReactNative {
  [self.window.rootViewController dismissViewControllerAnimated:TRUE completion:NULL];
  printf("%s","completion gotoReactNative");
}


@end
