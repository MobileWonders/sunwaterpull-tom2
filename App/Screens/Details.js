//TODO
//Replace Alert Details with Locations
//For multiple locations, replace with text "Multiple Locations"

import React, { Component } from 'react';

import {
  shareOnFacebook,
  shareOnTwitter,
} from 'react-native-social-share';

import {
  Platform,
  StyleSheet,
  Text,
  View,
  List,
  Linking,
  WebView,
  FlatList,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  Button,
  ScrollView,
  Share,
  ActivityIndicator,
  ActionSheetIOS

} from 'react-native';
import Dimensions from 'Dimensions';
import FAIcon from 'react-native-vector-icons/FontAwesome'
import * as firebase from 'firebase';
import Spinner from 'react-native-loading-spinner-overlay';
import Hyperlink from 'react-native-hyperlink';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
import HTMLView from 'react-native-htmlview';
import Modal from 'react-native-simple-modal';


//const locationdetails = location;
export default class Details extends Component<{}> {
  static navigationOptions = ({navigation}) => ({
    //title: `${navigation.state.params.location}`,

    headerStyle:{

    backgroundColor: 'transparent',//Dimensions.get('window').height === 812?'blue':'red'
    position: 'absolute',
    height: Dimensions.get('window').height === 812? height*0.05 : height*0.05,
    top: 0,
    //bottom:height*.92,
    left: width*.03,
    right: 0,
    width: width*.2, //.9
    shadowColor: 'transparent',
    borderBottomWidth: 0,
    elevation:0,

    },

    headerTintColor: 'transparent',


  });

  constructor(props){
    super(props);
    this.initalState = {
      ImageURL : '',
      //reg : ''
      animating: true,
      url: ''
    }
    this.state = this.initalState;

  }

componentDidMount(){
  var alertID = this.props.navigation.state.params.id;
 var reg = this.props.navigation.state.params.location;
  var imgURL = this.props.navigation.state.params.images ;
  var locations = this.props.navigation.state.params.location;
  var loclist = this.props.navigation.state.params.filtered;
  var tweet = this.props.navigation.state.params.tweet;
  var message = this.props.navigation.state.params.message;

if(locations == "Multiple Locations"){
  //create new region array
  var regions = [];
  var found = false;
  for (var i = 0; i < loclist.length;i++){ /*each dam in loclist array*/
    found = false;
    //find region of each dam

    console.log(loclist[i] +" Dam");
    var full_dam = loclist[i] +" Dam";
    this.setState({ImageURL : full_dam});
    var r = this.getRegionfromDam(loclist[i]+" Dam");
    console.log(r);

          for (var j = 0; j < regions.length; j++){
              if (r == regions[j]){
                  found = true;
                  console.log(found);
              }
          }
          //if region is not already in region array,
          //add region to new region array
          if (!found){
              regions.push(r);

          }

  }
  console.log(regions);
  //print region array
  var reg_string = "";
  for (var k = 0; k < regions.length; k++){
    if (k < regions.length-1){
      reg_string = reg_string + regions[k] + ", ";
      console.log(k);
    }
    else{
      reg_string = reg_string + regions[k];
      console.log(k);
    }
}
  this.setState({reg:reg_string})
} else {
  var toJoin = [];
  var l = locations.split(',');
  for(var cs in l){
   console.log('cs:' + l[cs]);
    toJoin.push(this.getRegionfromDam(l[cs]));

    this.setState({ImageURL : l[cs]});

  }

  this.setState({reg: toJoin.join(',')});
}
}
/*
  var ImageURL;
  //console.log(ImageURL);
  if(this.props.navigation.state.params.images === "gs://sunwater-38a15.appspot.com/images/") imgURL = "gs://sunwater-38a15.appspot.com/images/ClaudeWhartonWeir.jpg";//ALEX TODO DEFAULT IMAGE
  firebase.storage().refFromURL(imgURL).getDownloadURL().then((url)=>{

                    this.setState({ImageURL : url});
                   console.log('DOwnload URL: ' + url);
                        }
                  , function(error){
                  //  console.log(error);
                  });
}
*/

getRegionfromDam(dam){
  switch(dam){
      case "Burdekin Falls Dam":
      case "Julius Dam":
      return "North Queensland";

      case "Beardmore Dam": //changed from ejbeardmore
      case "Leslie Dam":
      case "Coolmunda Dam":
      case "Bjelke-Petersen Dam":
      case "Boondooma Dam":
      case "Glenlyon Dam":
      return "Darling Downs South West";

      case "Cania Dam":
      case "Callide Dam":
      case "Kroombit Dam":
      case "Fairbairn Dam":
      return "Central Queensland";

      case "Fred Haigh Dam"://changed from fredhaigh
      case "Paradise Dam":
      case "Wuruma Dam":
      return "Wide Bay Burnett";

      case "Teemburra Dam":
      case "Kinchant Dam":
      case "Eungella Dam":
      case "Peter Faust Dam":
      return "Mackay, Isaac, Whitsunday";

      case "Tinaroo Falls Dam":
      return "Far North Queensland";

    default:
    return null;
  }
}

onError(error){
  this.setState({image: require('./MainScreen/SunWater_Logo_New.png')})
}

shareModal(){

//  alert('this');

    this.setState({showPopUp:true});


}

clicked(){
  var facebookShare = this.props.navigation.state.params.facebook;
  var twitterShare = this.props.navigation.state.params.twitter;

  console.log('fbshare: '+facebookShare);
  console.log('twshare: '+twitterShare);


  if(facebookShare == true && twitterShare == false)
{
  this.openFacebook();
}

if (twitterShare == true && facebookShare == false ){
  this.openTwitter();
}
//this is the break
if( facebookShare == true && twitterShare == true) {

  this.openAll();

}

if( facebookShare == false && twitterShare == false) {

  this.removeall();
}
}

 removeall() {
    (async () => {
      await this.both();
    })();
}


both() { //needs to be differentiated
  Share.share({
    message:this.props.navigation.state.params.subtitle+
    "\n Regions Affected: \n"+this.state.reg+
    "\n\n\nMessage: \n"+this.props.navigation.state.params.tweet.replace(/<[^>]+>/g, '')+"\n"+
      this.props.navigation.state.params.date,
      url: 'https://www.sunwaterapp.com.au/alert.php?udid='+this.props.navigation.state.params.id,
    title:   this.props.navigation.state.params.name
  },
{
  excludedActivityTypes: [
    'com.apple.UIKit.activity.PostToTwitter','com.apple.UIKit.activity.PostToFacebook']
}

)
}

openAll(){
  Share.share({
    message:this.props.navigation.state.params.subtitle+
    "\n Regions Affected: \n"+this.state.reg+
    //"\n\n\nMessage: \n"+this.props.navigation.state.params.message+"\n"+

    "\n\n\nMessage: \n"+this.props.navigation.state.params.tweet.replace(/<[^>]+>/g, '')+"\n"+
      this.props.navigation.state.params.date,
    url: 'https://www.sunwaterapp.com.au/alert.php?udid='+this.props.navigation.state.params.id,
    title:   this.props.navigation.state.params.name
  }

)
}

openFacebook(){
//alert('facebook exists');
      Share.share({
        //this.props.navigation.state.params.subtitle
        //this.props.navigation.state.params.date
          message:"Regions Affected: \n"+this.state.reg+
          //"\n\n\nMessage: \n"+this.props.navigation.state.params.message.replace(/<[^>]+>/g, '')+"\n",
          "\n\n\nMessage: \n"+this.props.navigation.state.params.tweet.replace(/<[^>]+>/g, '')+"\n",
         // url: 'https://rnd.rowland.com.au/SunwaterConsole/alert.php?udid='+this.props.navigation.state.params.id,
         url: 'https://www.sunwaterapp.com.au/alert.php?udid='+this.props.navigation.state.params.id,
          title:   this.props.navigation.state.params.name
        },
      {
        excludedActivityTypes: [
          'com.apple.UIKit.activity.PostToTwitter']
      },

      )

    }

openTwitter(){

    Share.share({
        message:  "\n\n\nMessage: \n"+this.props.navigation.state.params.tweet.replace(/<[^>]+>/g, '')+"\n",
        url: 'https://www.sunwaterapp.com.au/alert.php?udid='+this.props.navigation.state.params.id,
        //url: 'http://www.sunwater.com.au',
        title:   this.props.navigation.state.params.name
      },
      {
        excludedActivityTypes: [
          'com.apple.UIKit.activity.PostToFacebook']
      }
    )

  }

view(url){
  this.setState({url: url});
  Linking.openURL(url);
  this.setState({url: ''});
}
renderNavBar() {
  const { navigation } = this.props
  return (
    <View style={styles.navBar}>
    {/*BARS*/}
    <Image source = {require('./MainScreen/HeaderBar.png')} style = {{marginLeft: width*.1,position:'absolute'}}/>

    <TouchableOpacity
              style={{ backgroundColor:'transparent', flexDirection: 'row', alignItems: 'center',position:'absolute', height: height*.1, width: width*.3}}
              onPress={() => { this.props.navigation.navigate('HubScreen');}} >
              <FAIcon name='angle-left' size={35} style={{color: 'black',marginLeft:width*.05,marginTop: height*.02, paddingBottom:height*.009}} />
          </TouchableOpacity>


      </View>

  )
}
  render() {
    const htmlContent = this.props.navigation.state.params.message;



    return (
      (this.state.url!='')?
<WebView
    source={{html: this.state.url}}
    /> :
      <View>
 {this.renderNavBar()}


<Text style={{ backgroundColor: 'transparent',position: 'absolute',height: 50,top: height*0.03,width: width,borderBottomWidth:0,fontSize:20, fontWeight:'600',
    color:'white',
    fontFamily:'Rubik-Medium',
    textAlign:'center',
      zIndex:2,
marginTop: height*.0075,
    //borderColor:'black',
    //borderRadius: 1,
    }}
    >
{`${this.props.navigation.state.params.location}`}

</Text>


<TouchableOpacity
        style = {{justifyContent:'center',
         width:width*.2,
         height: height*.08,
          backgroundColor:'transparent',
          position:'absolute',
          zIndex:2,
           left: width*.8}} onPress={() => this.clicked()}>
<FAIcon name='share' size={25} style={{ justifyContent:'center',paddingBottom:height*.05,paddingLeft:width*.08,marginTop:height*.075,color: 'white' }} />
</TouchableOpacity>




<View style={{marginTop:Dimensions.get('window').height*.1}}>


<View style = {{width: width, height: height*.4, backgroundColor:'transparent'}}>
{console.log(this.state.animating)}
<ActivityIndicator color="black" animating={this.state.animating} size = 'large' style = {{position:'absolute', alignSelf:'center', marginTop:height*.125}}/>

{/*TODO*/}


{//./DamImages/CallideValley.png

  this.state.ImageURL==''?null:

  this.state.ImageURL=='Burdekin Falls Dam'?
   <Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
  source = {require("./DamImages/BurdekinFallsDam.jpg")}
  style = {styles.imagetop}>
  </Image>:

  this.state.ImageURL=='Julius Dam'?
   <Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
  source = {require("./DamImages/JuliusDam.jpg")}
  style = {styles.imagetop}>
  </Image>:

this.state.ImageURL=='Beardmore Dam'?
<Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
source = {require("./DamImages/BeardmoreDam.jpg")}
style = {styles.imagetop}>
</Image>:

this.state.ImageURL=='Leslie Dam'?
   <Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
  source = {require("./DamImages/LeslieDam.jpg")}
  style = {styles.imagetop}>
  </Image>:

this.state.ImageURL=='Coolmunda Dam'?
<Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
source = {require("./DamImages/CoolmundaDam.jpg")}
style = {styles.imagetop}>
</Image>:

this.state.ImageURL=='Bjelke-Petersen Dam'?
   <Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
  source = {require("./DamImages/Bjelke-PetersenDam.jpg")}
  style = {styles.imagetop}>
  </Image>:

this.state.ImageURL=='Boondooma Dam'?
  <Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
  source = {require("./DamImages/BoondoomaDam.jpg")}
  style = {styles.imagetop}>
  </Image> :

this.state.ImageURL=='Glenlyon Dam'?
<Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
source = {require("./DamImages/GlenlyonDam.jpg")}
style = {styles.imagetop}>
</Image>:

this.state.ImageURL=='Cania Dam'?
<Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
source = {require("./DamImages/CaniaDam.jpg")}
style = {styles.imagetop}>
</Image>:

  this.state.ImageURL=='Callide Dam'?
   <Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
  source = {require("./DamImages/CallideDam.jpg")}
  style = {styles.imagetop}>
  </Image>:

this.state.ImageURL=='Kroombit Dam'?
   <Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
  source = {require("./DamImages/KroombitDam.jpg")}
  style = {styles.imagetop}>
  </Image>:

this.state.ImageURL=='Fairbairn Dam'?
<Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
source = {require("./DamImages/FairbairnDam.jpeg")}
style = {styles.imagetop}>
</Image>:

this.state.ImageURL=='Fred Haigh Dam'?
   <Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
  source = {require("./DamImages/FredHaighDam.jpg")}
  style = {styles.imagetop}>
  </Image>:

this.state.ImageURL=='Paradise Dam'?
<Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
source = {require("./DamImages/ParadiseDam.jpg")}
style = {styles.imagetop}>
</Image>:

this.state.ImageURL=='Wuruma Dam'?
   <Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
  source = {require("./DamImages/WurumaDam.jpg")}
  style = {styles.imagetop}>
  </Image>:

this.state.ImageURL=='Teemburra Dam'?
<Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
source = {require("./DamImages/TeemburraDam.jpg")}
style = {styles.imagetop}>
</Image>:

this.state.ImageURL=='Kinchant Dam'?
   <Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
  source = {require("./DamImages/KinchantDam.jpg")}
  style = {styles.imagetop}>
  </Image>:

this.state.ImageURL=='Eungella Dam'?
<Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
source = {require("./DamImages/EungellaDam.jpg")}
style = {styles.imagetop}>
</Image>:

this.state.ImageURL=='Peter Faust Dam'?
   <Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
  source = {require("./DamImages/PeterFaustDam.jpg")}
  style = {styles.imagetop}>
  </Image>:

this.state.ImageURL=='Tinaroo Falls Dam'?
<Image onError={this.onError.bind(this)} onLoadStart = {(e) => this.setState({animating : true})} onLoad = {(e) => this.setState({animating : false})}
source = {require("./DamImages/TinarooFallsDam.jpg")}
style = {styles.imagetop}>
</Image>:

  null
}
        {console.log(this.state.ImageURL)}
</View>

        <View style = {{bottom: height*.085,marginLeft: width*.075, marginRight: width*.05}}>
        <Text style = {styles.title}>
          {this.props.navigation.state.params.name}
        </Text>

        <Text style = {styles.date}>
    {/*convert time to AEST*/}
        {this.props.navigation.state.params.time}, {this.props.navigation.state.params.date}

        </Text>

        </View>

  <View style = {{width:Dimensions.get('window').width*.95,marginLeft: width*.025, height:height*.39, bottom: height*.035}}>
        <ScrollView style = {{height:height, marginLeft: width*.05,width: width *.9,backgroundColor:'transparent', paddingRight: width*.075}}>



       <Text style = {styles.subtitle}>
          {this.props.navigation.state.params.subtitle}
      </Text>


          <HTMLView
        value={htmlContent}
        stylesheet={styles}
        onLinkPress = {(url) => this.view(url)}
      />

</ScrollView>

</View>

        </View>



     </View>

    );
  }
}




const styles = StyleSheet.create({
  background:{
flex:1,
position:'absolute',
width: Dimensions.get('window').width,
height: Dimensions.get('window').height

  },

  htmlContent:{
    color:'white'
  },

imagetop:{
  //maxHeight: Dimensions.get('window').height/3.5,
  //minWidth: Dimensions.get('window').width,
  //resizeMode: 'contain',
  //justifyContent: 'center',
  //alignItems: 'center',
  position:'absolute',
  height:'100%',
  width: Dimensions.get('window').width,
  bottom: height*.1
//  overflow: 'hidden',
  //borderColor:'transparent'
// backgroundColor:'black'
  //borderColor:'#2ED172',
  //borderWidth: 7,
  //borderRadius:95

},
date:{
  textAlign:'left',
  fontFamily:'Rubik-Regular',
  fontSize:14,
  color: '#00AFD1',
  backgroundColor:'transparent',



},
  title: {
    width: Dimensions.get('window').width*.9,
    textAlign: 'left',
    fontSize: 24,

    color:'#031E2F',
    backgroundColor:'transparent',
    fontFamily: 'Rubik-Bold',
    //marginLeft: Dimensions.get('window').width*.05,
    paddingTop:height*.03,
marginBottom: height*.0075
  },
  message:{
    fontSize:14,
    color:'#d5d5d5',
    fontFamily: 'Rubik-Regular',
  },
  locations:{
    //width: Dimensions.get('window').width/2,
    textAlign: 'center',
    fontSize:18,
   // marginLeft:  Dimensions.get('window').width/4,

        color:'black',
        backgroundColor:'transparent',
        fontFamily: 'Rubik-Regular',
  },
  p: {
    color:'#5f5f5f',
    fontSize:14,
    fontFamily: 'Rubik-Regular',
    lineHeight:18


  },
  strong: {
    fontWeight:'bold',
color:'#FFF'
        },
        ul:{
          color:'#fff',
        },
        a: {
          fontWeight: '400',
          fontFamily:'Rubik-Regular',
          //textDecorationLine: 'underline',
          color: '#EEAF00', // make links coloured pink
        },
        u:{
          color:'#fff',
          textDecorationLine: 'underline'
        },

  regions:{
    //width: Dimensions.get('window').width/2,
    textAlign: 'left',
    fontSize:14,
    marginTop: height*.01,
    //marginLeft:  Dimensions.get('window').width*.05,
    marginTop:  Dimensions.get('window').height*.02,
    width: width*.9,
        color:'black',
        lineHeight: 16,
        paddingBottom: height*.03,
        backgroundColor:'transparent',
        fontFamily: 'Rubik-Regular',
  },
  subtitle:{
    width: Dimensions.get('window').width*.9,
    textAlign: 'left',
    fontSize:18,
marginBottom:height*.01,
        color:'#031E2F',
        backgroundColor:'transparent',
        fontFamily: 'Rubik-Medium',




  },
  navBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: height*.1,
    alignItems: 'center',
    position: 'absolute',
    zIndex:1,
  //  paddingHorizontal: 25,
    //backgroundColor: '#4086c0'
    backgroundColor:'transparent',
   // marginBottom: height*.04,

 paddingBottom: height*.08,


},

});
