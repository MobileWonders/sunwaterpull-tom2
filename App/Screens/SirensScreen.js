import React, {Component} from 'react'

import { StyleSheet,Dimensions, View, Text, Animated } from 'react-native'

import Interactable from 'react-native-interactable'

import Menu from '../Component/Menu'
import Test from './MainScreen/Sirens'

const SideMenuWidth = Dimensions.get('window').width*0.8

export default class SirensScreen extends Component {
    static navigationOptions = {
       // title: 'SideMenu',
        header: null,
        gesturesEnabled: false
    }

    constructor(props) {
        super(props)
        this.state = {
            deltaX: new Animated.Value(-SideMenuWidth),
            menuOpened: false
        }
    }
    render() {
        return(
            <View style = {{flex: 1, justifyContent: 'center', backgroundColor: '#214559'}}>
                <Menu onItemSelected={this.onMenuItemSelected} />
                <Interactable.View
                    style = {{flex: 1}}
                    ref = 'menuInstance'
                    horizontalOnly = {true}
                    snapPoints = {[{x: 0, damping:0.6}, {x:SideMenuWidth, damping: 0.6}]}
                    boundaries = {{right: 0, left:0}} //prevents movements
                    initialPosition = {{x: 0}}
                    animatedValueX = {this.deltaX}
                    onSnap = {this.onStopInteraction}>
                    <Test style = {{backgroundColor: 'transparent'}} navigation = {this.onNavigation} onMenuPress = {this.onMenuPress} />
                </Interactable.View>
            </View>
        )
    }

    onMenuItemSelected = item => {
        console.log('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
        const menuOpened = false
        this.refs['menuInstance'].snapTo({index: 0})
        this.props.navigation.navigate(item);
    }

    onStopInteraction = (event, check) => {
        let menuOpened = true
        if(event.nativeEvent.index == 0) {
            menuOpened = false
        }
        this.setState((preState, props) => {
            return { menuOpened }
        })
    }

    onMenuPress = () => {
        const menuOpened = !this.state.menuOpened
        if(menuOpened) {
            this.refs['menuInstance'].snapTo({index: 1})
        } else {
            this.refs['menuInstance'].snapTo({index: 0})
        }
    }

    onNavigation = (item, value) => {
        this.props.navigation.navigate(item, value);
    }
}
