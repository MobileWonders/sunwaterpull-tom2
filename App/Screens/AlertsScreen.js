import React, {Component} from 'react'

import { StyleSheet, View, Text, Animated } from 'react-native'

import Interactable from 'react-native-interactable'
import FAIcon from 'react-native-vector-icons/FontAwesome'
import Menu from '../Component/Menu'
//import PushMenu from '../Component/Push'
import Alerts from './MainScreen/Alerts'


const SideMenuWidth = 300

export default class AlertsScreen extends Component {
    static navigationOptions = {
        //title: 'SideMenu',
        //title: 'Test',
        tabBarIcon:<FAIcon name='refresh' size={22} style={{ color: '#000' }}/>,
        header: null,
        gesturesEnabled: false
    }

    constructor(props) {
        super(props)
        this.state = {
            deltaX: new Animated.Value(-SideMenuWidth),
            menuOpened: false
        }
    }


    render() {

        return(
            <View style = {{flex: 1, justifyContent: 'center', backgroundColor: '#214559'}}>
                <Menu onItemSelected={this.onMenuItemSelected} />
                <Interactable.View
                    style = {{flex: 1}}
                    ref = 'menuInstance'
                    //  ref = 'pushInstance'
                    horizontalOnly = {true}
                    snapPoints = {[{x: 0, damping:0.6}, {x:SideMenuWidth, damping: 0.6}]}
                    boundaries = {{right: SideMenuWidth, left:0}}
                    initialPosition = {{x: 0}}
                    animatedValueX = {this.deltaX}
                    onSnap = {this.onStopInteraction}>
                    <Alerts
                        onItemSelected={this.onMenuItemSelected}
                        style = {{backgroundColor: '#4086c0'}}
                        navigation = {this.props.navigation}
                        onMenuPress = {this.onMenuPress}

                    />
                </Interactable.View>
            </View>
        )
    }



    onMenuItemSelected = (item, value) => {
         //alert(value.key);
        const menuOpened = false
        this.refs['menuInstance'].snapTo({index: 0})
        this.props.navigation.navigate(item,{value});
    }

    onStopInteraction = (event, check) => {
        let menuOpened = true
        if(event.nativeEvent.index == 0) {
            menuOpened = false
        }
        this.setState((preState, props) => {
            return { menuOpened }
        })
    }

    onMenuPress = () => {
        console.log('##############################:')
        const menuOpened = !this.state.menuOpened
        if(menuOpened) {
            this.refs['menuInstance'].snapTo({index: 1})
        } else {
            this.refs['menuInstance'].snapTo({index: 0})
        }
    }
}

/*

onMenuItemSelected = item => {
    const pushOpened = false
    this.refs['pushInstance'].snapTo({index: 1})
    this.props.navigation.navigate(item);
}

onStopInteraction = (event, check) => {
    let pushOpened = true
    if(event.nativeEvent.index == 1) {
        pushOpened = false
    }
    this.setState((preState, props) => {
        return { pushOpened }
    })
}

onMenuPress = () => {
    const pushOpened = !this.state.menuOpened
    if(menuOpened) {
        this.refs['pushInstance'].snapTo({index: 2})
    } else {
        this.refs['pushInstance'].snapTo({index: 1})
    }
}

onNavigation = item => {
    this.props.navigation.navigate(item);
}
}*/
