import React, {Component} from 'react'

import { StyleSheet, View, Text, Animated } from 'react-native'

import Interactable from 'react-native-interactable'

import Menu from '../Component/Menu'
import AR_annotations from './MainScreen/AR_annotations'

const SideMenuWidth = 300

export default class AR_annotationsScreen extends Component {
    static navigationOptions = {
       //title: '',
        header: null,
        gesturesEnabled: false
    }

    constructor(props) {
        super(props)
        this.state = {
            deltaX: new Animated.Value(-SideMenuWidth),
            menuOpened: false
        }
    }
    render() {
        return(
            <View style = {{flex: 1, justifyContent: 'center', backgroundColor: '#214559'}}>
                <Menu onItemSelected={this.onMenuItemSelected} />
                <Interactable.View
                    style = {{flex: 1}}
                    ref = 'menuInstance'
                    horizontalOnly = {true}
                    snapPoints = {[{x: 0, damping:0.6}, {x:SideMenuWidth, damping: 0.6}]}
                    boundaries = {{right: SideMenuWidth, left:0}}
                    /*boundaries = {{right: SideMenuWidth}}*/
                    initialPosition = {{x: 0}}
                    animatedValueX = {this.deltaX}
                    onSnap = {this.onStopInteraction}>
                    <AR_annotations
                    onItemSelected={this.onNavigation}
                    style = {{backgroundColor: '#FE6070'}} navigation = {this.props.navigation} onMenuPress = {this.onMenuPress} />
                </Interactable.View>
            </View>
        )
    }


    onMenuItemSelected = (item, value) => {
        //alert(value.key);
       const menuOpened = false
       this.refs['menuInstance'].snapTo({index: 0})
       this.props.navigation.navigate(item,{value});
   }

    onStopInteraction = (event, check) => {
        let menuOpened = true
        if(event.nativeEvent.index == 0) {
            menuOpened = false
        }
        this.setState((preState, props) => {
            return { menuOpened }
        })
    }

    onMenuPress = () => {
        const menuOpened = !this.state.menuOpened
        if(menuOpened) {
            this.refs['menuInstance'].snapTo({index: 1})
        } else {
            this.refs['menuInstance'].snapTo({index: 0})
        }
    }

    onNavigation = (item, value) => {
        this.props.navigation.navigate(item,{value});
    }
}
