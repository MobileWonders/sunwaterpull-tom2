//TODO

'use strict'
import React, { Component } from 'react'

import {Alert, StyleSheet,Dimensions, ActivityIndicator, Switch, NetInfo, TextInput, View, ListView,AsyncStorage, ScrollView, TouchableOpacity, Image, Text, TouchableHighlight, Button, Linking } from 'react-native'

import FAIcon from 'react-native-vector-icons/FontAwesome'
import Container from '../../Component/Container'
//import PercentageCircle from 'react-native-percentage-circle'
//import ProgressCircle from 'react-native-progress-circle'
import Modal from 'react-native-simple-modal'
import * as firebase from 'firebase';
import {NavigationActions} from 'react-navigation';
var DeviceInfo = require('react-native-device-info');
import AlertsScreen from '../AlertsScreen'
import Alerts from './Alerts'
import Drawer from 'react-native-drawer-menu';
import CustomMultiPicker from "react-native-multiple-select-list";
import Expand from 'react-native-simple-expand';
import OneSignal from 'react-native-onesignal';
import Carousel, {Pagination, getInputRangeFromIndexes} from 'react-native-snap-carousel';


const width = Dimensions.get("window").width
const height = Dimensions.get("window").height
const percentrate = 54
const buttonname ='Continue'
const sitevisited = true

export default class Tutorial extends Component {


    constructor(props){
        super(props);
        this.initialState = {
            sitevisited : true,
            gotoNext: false,
            dbConnectionFailed: false,
            status: false, //internet connectivitt
           //SPINNER
           animating: true,
            valID : '',
            deviceID : '',
            open: true,
            open1:false,
            isOpened:false,
            screen:'one',
            newMail:'',
            emailAddress:'',
            isEmailSelected:false, //email wizard toggle
            showPopUp:false, //show email popup
            isPushSelected:false, //push wizard toggle
            getTags:'',
            selectItems: [],
            selectItems1: [],
            newselectitems: [],
            key : 0,
            activeDotIndex: 0,
          activeSlide:0,
          isVisible:false,

            images: [
              require('./Image-1.jpg'),
              require('./Image-2.jpg'),
              require('./Image-3.jpg'),
              require('./Image-4.jpg'),

            ]

        }
        this.state = this.initialState;

    }




    renderItem ({item, index}) {

      return (


<View style={styles.slide}>

<Text style={styles.title}>{ item.title }</Text>
</View>


      );
  }



_loadInitialState = async () => {
    var success = true;
    try {
      var wiz= await AsyncStorage.getItem('WIZARD_KEY');
      console.log("WIZARD KEY BIATCH"+ wiz);
      if (wiz !== null){
        this.setState({Wizard : wiz.toString()});
        console.log(wiz);
      } else {
     //   alert('Initialized with no selection on disk.');
      }
    } catch (error) {
        success = false;
   // alert('AsyncStorage error: ' + error.message);
    }
    try {
        var term= await AsyncStorage.getItem('TERMS_KEY');
        if (term !== null){
          this.setState({Terms: term.toString()});
        } else {
       //   alert('Initialized with no selection on disk.');
        }
      } catch (error) {
        success = false;
     //   alert('AsyncStorage error: ' + error.message);
      }
  };

 async _setInitialState  (STORAGE_KEY) {
    try {
        console.log(STORAGE_KEY);
      var value = await AsyncStorage.setItem('STORAGE_KEY' ,STORAGE_KEY);
    } catch (error) {
   //   alert('AsyncStorage error: ' + error.message);
    }
  };

  async _setInitialWizard  (WIZARD_VALUE) {
    try {
      var value = await AsyncStorage.setItem('WIZARD_KEY' ,WIZARD_VALUE);
    } catch (error) {
   //   alert('AsyncStorage error: ' + error.message);
    }
  };

  async _setInitialTerms  (TERMS_VALUE) {
    try {
      var value = await AsyncStorage.setItem('TERMS_KEY' ,TERMS_VALUE);
    } catch (error) {
   //   alert('AsyncStorage error: ' + error.message);
    }
  };

componentDidUpdate(){


    if(this.state.gotoNext){ //skip T&C
        console.log("GO TO ALERTS");
        this.state.gotoNext = false;
        const {navigation} = this.props; //ALEX redirect
        navigation('HubScreen');

    }
    else{
        //this.setState({screen: 'one'});
    }
}

    render() {

      if(this.state.activeSlide == 9){

         this.state.isVisible = true;

      }else{
        this.state.isVisible = false
      }

        const model = DeviceInfo.getBrand();
        console.log(model);
        var check = model.indexOf("Apple");
        var apple =true;

        if(check == 0){
            apple = true;

        }else{
            apple = false;
        }


        const userList = {
            "1":"Beardmore Dam", //changed from ej beardmore (7)
            "2":"Bjelke-Petersen Dam",
            "3":"Boondooma Dam",
            "4":"Burdekin Falls Dam",
            "5":"Callide Dam",
            "6":"Cania Dam",
            "7":"Coolmunda Dam",
            "8":"Eungella Dam",
            "9":"Fairbairn Dam",
            "10":"Fred Haigh Dam",
            "11":"Glenlyon Dam",
            "12":"Julius Dam",
            "13":"Kinchant Dam",
            "14":"Kroombit Dam",
            "15":"Leslie Dam",
            "16":"Paradise Dam",
            "17":"Peter Faust Dam",
            "18":"Teemburra Dam",
            "19":"Tinaroo Falls Dam",
            "20":"Wuruma Dam",
          }
        const { onItemSelected } = this.props
        const drawStyles = {
            drawer: { shadowColor: 'black', shadowOpacity: 1, shadowRadius: 0},
            main: { paddingLeft: 0 }
        }
        const regionList = {
            "1":"Far North Queensland",
            "2":"North Queensland",
            "3":"Mackay, Isaac, Whitsunday",
            "4":"Central Queensland",
            "5":"Wide Bay Burnett",
            "6":"Darling Downs South West"
        }
      const { onRegionSelected } = this.props
      const drawStylesRegion = {
          drawer: { shadowColor: 'black', shadowOpacity: 1, shadowRadius: 0 },
          main: { paddingLeft: 0 }
      }


        return (
          //
          <Container style={styles.container} >
          {this.renderNavBar()}
          <Image source = {require('./gradient.jpg')} style ={{width: Dimensions.get('window').width,flex: 1}}>

<View style = {{width:width, height:height*.7, marginTop:height*.1, marginRight:width*.05,backgroundColor:'transparent'}}>
      < Carousel
       layout={'default'}
      removeClippedSubviews={false}
      inactiveSlideOpacity = {0.2}
      firstItem = {0}
      sliderWidth = {width}
      itemWidth = {width*.9}
      data = {this.state.images}
      renderItem = {this.renderItem}
      onSnapToItem={(index) => this.setState({ activeSlide: index }) }
   />

      { this.pagination }
{this.state.isVisible &&<View style = {{position:'absolute'}}>
<Text style = {{color:'white',marginLeft:width*.05, fontSize: 18, marginTop: height*.2}}>For more information regarding the sirens: </Text>

<TouchableOpacity onPress={() => Linking.openURL('http://www.sunwater.com.au/development/projects/emergency-warning-system-installation')}>

<Text style = {{fontSize:18,color:"white",textAlign:'center',marginTop:height*.1, textDecorationLine:'underline'}}>
Click here to go to the SunWater website</Text>

</TouchableOpacity>
<View style = {{flexDirection:'row'}}>
<Text style = {{color:'white', fontSize:18,marginLeft: width*.325, marginTop:height*.05}}>or call</Text>

<Text style = {{color:'white', fontSize:18, marginTop: height*.05, marginLeft: width*.02}} onPress={()=> Linking.openURL('tel:131589')}>131589</Text>
</View>
</View>}




      {/*<TouchableOpacity style = {{width:width/3, marginLeft: width*.2, marginTop: height*.8,position:'absolute',backgroundColor:'black'}} onPress={ () => this.play()}><Text style = {{color:'white'}}>Press to hear this siren</Text></TouchableOpacity>*/}

</View>
</Image>

          </Container>

)

}



renderItem = ({
  item,
  index
}) => {
  return ( < Image style = {
      styles.logoStyle
    }
    source = {
      item
    }
    />
  );
}

get pagination () {
  const { images, activeSlide } = this.state;

  return (
      <Pagination
        dotsLength={images.length}
        activeDotIndex={activeSlide}
        containerStyle={{
          position:'absolute',
          width:width*.1,
          backgroundColor: 'transparent',
          marginTop:height*.7,
          marginLeft: width*.425}}
        dotStyle={{
            width: 13,
            height: 13,
            borderRadius: 5,
            marginHorizontal: 8,
            backgroundColor: '#000'
        }}
        inactiveDotStyle={{
          width: 12,
          height: 12,
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
  );
}

renderNavBar() {

  return (
      <View style={styles.navBar}>
{/*BARS*/}
       <TouchableOpacity
              style={{ flexDirection: 'row', alignItems: 'center',marginTop:height*.005, height: height*.075,width: width*.2, backgroundColor:'transparent'}}
              onPress={() => { this.props.navigation('HubScreen');}} >
              <FAIcon name='home' size={25} style={{color: 'black',marginLeft:width*.07,paddingBottom:height*.009}} />
          </TouchableOpacity>


</View>


  )
}








selectAll(){
    console.log('all');
    Alert.alert(
        'Alert',
        'Would you like to subscribe to all alerts?',
        [
              {text: 'OK', onPress: () => this.acceptAll()},
              {text: 'Cancel', onPress: () => console.log('Cancel Pressed'),style: 'cancel'}              ]
      )


}

acceptAll(){
    this.setState(
        {
            selectItems: ["Beardmore Dam","Bjelke-Petersen Dam","Boondooma Dam","Burdekin Falls Dam","Callide Dam","Cania Dam","Coolmunda Dam","Eungella Dam","Fairbairn Dam","Fred Haigh Dam","Glenlyon Dam","Julius Dam","Kinchant Dam","Kroombit Dam","Leslie Dam","Paradise Dam","Peter Faust Dam","Teemburra Dam","Tinaroo Falls Dam","Wuruma Dam"],
            selectItems1: ["Far North Queensland","North Queensland","Mackay, Isaac, Whitsunday", "Central Queensland","Wide Bay Burnett","Darling Downs South West"],
        }
    );
    this.submit();
}
/*
  RegionsClick(){
    //this.setState({open: false, open1: !this.state.open1})
    this.setState({open: false})
    if(this.state.open1 == true){
      this.setState({open1: false})
    }
    else if(this.state.open1 == false){
      this.setState({open1: true})
    }
  }
  userClick(){ //dam
    //this.setState({open1: false, open: !this.state.open})
    this.setState({open1: false})
    if (this.state.open == true){
      this.setState({open: false})
    }
    else if(this.state.open == false){
      this.setState({open: true})
    }
}*/
    RegionsClick(){
        //alert(this.state.open + ', ' + !this.state.open1)
        this.setState({open1: !this.state.open1, open: false})
      }

      userClick(){ //dam
        //alert(!this.state.open + ', ' + this.state.open1)
        this.setState({open: !this.state.open, open1: false})
      }

/*
    {
        this.state.isOpened?
    <Drawer
                    isOpen={this.state.isOpened}
                    drawerPosition={Drawer.positions.Left}
                    //drawerPosition={Drawer.positions.Both}
                    drawWidth={300}
                    ref={(ref) => {this._drawer = ref}}
                >


            <View style={{height: height, backgroundColor: 'white'}}/>
    </Drawer>:null
                }*/
                play(){
                  alert('this worked');
                }

acceptTerms(){
    AsyncStorage.setItem('TERMS_KEY','true'); //set wizard to FALSE
    this.setState({Terms:true});
    this.setState({screen: 'five'})
    //this.onTerms();
}

//onTerms(){
    //send to Wizard

   //      const _this = this
         //this.onShowSettings()
        // this.setState({isOpened: true})
    //     this.setState({screen: 'five'})
   //      setTimeout(function() {
          //_this.userClick();
   //      }, 100)


  //  }

onAccept(){
//send to Wizard

     const _this = this
     //this.onShowSettings()
     this.setState({isOpened: true})
     this.setState({screen: 'one'})
     setTimeout(function() {
      //_this.userClick();
     }, 100)


}

contFirst(){
    //send to Wizard
    {
        // const _this = this
         //this.onShowSettings()
       //  this.setState({isOpened: true})
         this.setState({screen: 'two'})
       //  setTimeout(function() {
          //_this.userClick();
       //  }, 100)
      }

    }

next(){

    this.setState({screen:'three'})
}

nextagain(){

    this.setState({screen:'four'})
}




    submitCommunication(){ //on push/email wizard submit
        //   NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
       //NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);

         //          NetInfo.isConnected.fetch().done(
           //            (isConnected) => { this.setState({ status: isConnected });
                       //ERROR
       //console.log("@@@@@@"+this.state.status);
       //if(this.state.status){
           if ((!this.state.isPushSelected)&&(!this.state.isEmailSelected)){
               alert('You must choose at least one method of notification for the alerts in the app to work');
           }
           else{
               this.setState({screen:'seven'})
           }

        }
//sidemenu(){ //on push/email wizard submit
  //      this.setState({screen:'five'})
   // }

alerts(){ //on push/email wizard submit
    //set wizard to FALSE
        AsyncStorage.setItem('WIZARD_KEY','true');

        this.setState({Wizard:true});

    //nav to alerts

        const {navigation} = this.props;
        navigation('HubScreen');
    }

    hub(){ //on push/email wizard submit
      //set wizard to FALSE
          AsyncStorage.setItem('WIZARD_KEY','true');

          this.setState({Wizard:true});

      //nav to alerts

          const {navigation} = this.props;
          navigation('HubScreen');
      }

//}
//else{
  //  alert('Remember to update your contact settings once you have a stable internet connection.');
    //this.setState({screen:'two'})
//}
//});
//NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);


submit(){ //on dams/regions wizard submit
//alert('You must choose at least one method of notification before continuing');
//NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
//NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);

  //          NetInfo.isConnected.fetch().done(
    //            (isConnected) => { this.setState({ status: isConnected });
//console.log('########'+isConnected);
//console.log(';;;;;;;;'+this.state.status);
//if(this.state.status){

      console.log('locas array: ' + locs);
      var dams = {};
      this.state.selectItems1.forEach((item)=>{
        switch (item) {
          case "Far North Queensland":{
           console.log("FNQ was clicked");
           dams["Far North Queensland"] = ["Tinaroo Falls Dam"];//changed from ej beardmore
           break;
          }
          case "North Queensland":{
           console.log("NQ was clicked");
           dams["North Queensland"] = ["Julius Dam", "Burdekin Falls Dam"];
           break;
          }
          case "Mackay, Isaac, Whitsunday":{
          console.log("M,I,W was clicked");
          dams["Mackay, Isaac, Whitsunday"] = ["Peter Faust Dam", "Eungella Dam", "Kinchant Dam", "Teemburra Dam"];
          break;
          }
          case "Central Queensland":{
            console.log("CQ was clicked");
            dams["Central Queensland"] = ["Fairbairn Dam", "Callide Dam", "Kroombit Dam", "Cania Dam"];
            break;
          }
        case "Wide Bay Burnett":{
          console.log("WB Burnett was clicked");
          dams["Wide Bay Burnett"] = ["Wuruma Dam", "Fred Haigh Dam", "Paradise Dam"];
          break;
        }
        case "Darling Downs South West":{
          console.log("DD SW was clicked");
          dams["Darling Downs South West"] = ["Boondooma Dam", "Bjelke-Petersen Dam", "Leslie Dam", "Coolmunda Dam", "Beardmore Dam","Glenlyon Dam"];
          break;
        }

      }
      });


var newselectitems = this.state.selectItems;
      for (var key in dams){
console.log(key + ' : ' + dams[key]);
for(var l in dams[key]){
  console.log(l + ' : ' + dams[key][l]);
  newselectitems.push(dams[key][l]);
}
      }
      this.setState({ selectItems: newselectitems });
      var locs = {};
      this.state.selectItems.forEach((item)=> {
        let sendItem = this.prepareSendItem.call(this, item);
        if (sendItem != null) {
          console.log('locs SendItem: ' + sendItem);
          let {key, value} = sendItem;
          locs[key] = item;
        }
      });

      const email = this.state.emailAddress || null;
      var updates = {};
console.log('Email to be submitted: ' + email);
var size = false;
for(var key in locs){
  size =true;
  break;
}

console.log('locs: ' + size);

 if (email !== null || size){
        firebase.database().ref(`server/saving-data/fireblog/subs/${this.state.deviceID}`).set({
          email: email,
          locs: locs,
          regions: dams,
          emailSub: this.state.isEmailSelected,
          push: this.state.isPushSelected
        });

        for(var key in locs){
            OneSignal.sendTag(key, key);
          }
          OneSignal.sendTags(dams);
          var { getTags } = this.state;
          this.setState({ getTags: locs });

        Alert.alert("Your alert settings have been updated")
        //ALEX --> cog alert
            this.setState({screen:'six'})
      }
else if (!size){
    alert('No Settings Selected');
}
/*

*/
}
//else{//no connection
  //  alert('No internet connection - alert settings could not be saved.');
    //ALEX cog instruction screen
    //    this.setState({screen:'four'})


//}





////////////////////END WIZARD
//alert('done!');
//});
//}

validateEmail(){
    let re = RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
    if (re.test(this.state.newMail)){
      //alert('confirm');
      this.onConfirmEmail();
    }
    else{
      alert('Invalid email address');
    }
  }

onConfirmEmail() {
  //  NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
  //  NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);

 //   NetInfo.isConnected.fetch().done(
 //       (isConnected) => { this.setState({ status: isConnected }); /*alert(this.state.status)*/

   // if (this.state.status){
        this.setState({emailAddress: this.state.newMail, showPopUp: false}, function SubmitonConfirm(){
        console.log('Email address:', this.state.emailAddress);
        console.log('EmailSub1: ', this.state.isEmailSelected);
        firebase.database().ref(`server/saving-data/fireblog/subs/${this.state.deviceID}/email`).set(this.state.emailAddress);
        firebase.database().ref(`server/saving-data/fireblog/subs/${this.state.deviceID}/emailSub`).set(this.state.isEmailSelected);
        Alert.alert('Your email setting has been saved');
        }.bind(this));
   // }
   // else{ //if not connected to internet
   //     alert('No internet connection - email settings could not be saved.');
   //     this.setState({isEmailSelected:false});
   // }
//});
   // NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
   }


   prepareSendItem(item) {
    switch (item) {

      case "Bjelke-Petersen Dam":
        console.log("Bjelke-Petersen was clicked");
        return {
          key: "Bjelke-Petersen",
          value: "Bjelke-Petersen"
        };
      case "Boondooma Dam":
        console.log("Boondooma was clicked");
        return {
          key: "Boondooma",
          value: "Boondooma",
        };
      case "Burdekin Falls Dam":
        console.log("Burdekin Falls was clicked");
        return {
          key: "Burdekin Falls",
          value: "Burdekin Falls"
        };
      case "Callide Dam":
        console.log("Callide was clicked");
        return {
          key: "Callide",
          value: "Callide"
        };
      case "Cania Dam":
        console.log("Cania was clicked");
        return {
          key: "Cania",
          value: "Cania"
        };
      case "Coolmunda Dam":
        console.log("Coolmunda was clicked");
        return {
          key: "Coolmunda",
          value: "Coolmunda"
        };
      case "Beardmore Dam": //changed from ej beadmore dam
        console.log("Beardmore was clicked");
        return {
          key: "Beardmore",
          value: "Beardmore"
        };
      case "Eungella Dam":
        console.log("Eungella was clicked");
        return {
          key: "Eungella",
          value: "Eungella"
        };
      case "Fairbairn Dam":
        console.log("Fairbairn was clicked");
        return {
          key: "Fairbairn",
          value: "Fairbairn"
        };

        case "Glenlyon Dam":
        console.log("Glenlyon was clicked");
        return {
          key: "Glenlyon",
          value: "Glenlyon"
        };
      case "Fred Haigh Dam":
        console.log("FredHaigh was clicked");
        return {
          key: "Fred Haigh", //changed from fredhaigh
          value: "Fred Haigh"
        };
      case "Julius Dam":
        console.log("Julius was clicked");
        return {
          key: "Julius",
          value: "Julius"
        };
      case "Kinchant Dam":
        console.log("Kinchant was clicked");
        return {
          key: "Kinchant",
          value: "Kinchant"
        };
      case "Kroombit Dam":
        console.log("Kroombit was clicked");
        return {
          key: "Kroombit",
          value: "Kroombit"
        };
      case "Leslie Dam":
        console.log("Leslie was clicked");
        return {
          key: "Leslie",
          value: "Leslie"
        };
      case "Paradise Dam":
        console.log("Paradise was clicked");
        return {
          key: "Paradise",
          value: "Paradise"
        };
      case "Peter Faust Dam":
        console.log("PeterFaust was clicked");
        return {
          key: "Peter Faust",
          value: "Peter Faust"
        };
      case "Teemburra Dam":
        console.log("Teemburra was clicked");
        return {
          key: "Teemburra",
          value: "Teemburra"
        };
      case "Tinaroo Falls Dam":
        console.log("Tinaroo Falls was clicked");
        return {
          key: "Tinaroo Falls",
          value: "Tinaroo Falls"
        };
      case "Wuruma Dam":
        console.log("Wuruma was clicked");
        return {
          key: "Wuruma",
          value: "Wuruma"
        };
        default:
        return null;
    }
    }

}//endclass

/////STYLES
const styles = StyleSheet.create({
    background:{
            flex:1,
            alignItems:'stretch',
            height:'auto',
            width: Dimensions.get('window').width,

    },
    container: {
        shadowColor: '#000000',
        shadowOpacity: 0.4,
        shadowOffset: { height: -5, width: -5 },
        shadowRadius: 10,

        //flex:1,
        //alignItems:'stretch',
        //height:'auto',
        //width: Dimensions.get('window').width,
        //position:'absolute',
        //height:'auto',
        //width:width
    },
    navBar: {
        height: height*.075,
        justifyContent: 'flex-start',
        //marginLeft: Dimensions.get('window').width/15,
        zIndex:1,
        width:width,
        backgroundColor: 'transparent',
        //backgroundColor:'transparent',
        display: 'flex',
        flexDirection: 'row',
        position:'absolute'
    },

    siteName: {
        marginTop: 30,
        width: 250
    },

    topMenu: {
        display: 'flex',
        flexDirection: 'row'
    },
    stretch: {
        width: 120,
        height: 50,
        // left:80,
        // position: 'absolute',
        // flex:1

    },
    buttonclick: {
       // flex:1,
       width: Dimensions.get('window').width,
       height: Dimensions.get('window').height*0.47,

        //width: '100%',
//        height:320,
        //position: 'absolute',



    },

    style1: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height*.47,

        // position:'absolute',
        // top:300,
        //width: '100%',
       // height:312,
   //     alignItems:'stretch',


    },
    style2: {
        width: Dimensions.get('window').width,
       height: Dimensions.get('window').height*.47,
       //marginBottom: 400,
       bottom:75,
       // top:300,
        //width: '100%',
       // height:312,
   //     alignItems:'stretch',


    },
    style3: {
        width: Dimensions.get('window').width/4,
       height: Dimensions.get('window').height/10,
       //marginBottom: 400,

       left:200,
        top:300,
        //width: '100%',
       // height:312,
      alignItems:'center',


    },
    text1:{
        color:'white',
        height: 'auto',
        fontSize:60,
        fontWeight:'bold',
       bottom:100,
        backgroundColor:'transparent',
       position:'relative',
       marginLeft: Dimensions.get('window').width*.1,
    },
    text2:{
        color:'white',
        height: 'auto',
        fontSize:60,
        fontWeight:'bold',
        bottom:200,
        backgroundColor:'transparent',
       position:'relative',
       marginLeft: Dimensions.get('window').width*.1,
    },
    text3:{
        color:'white',
        height: 'auto',
        fontSize:20,
        fontWeight:'bold',
        backgroundColor:'transparent',
       position:'relative',
       marginLeft: Dimensions.get('window').width*.1,
    },

    logo:{

        width: Dimensions.get('window').width/2,
        height: Dimensions.get('window').width/6,
        marginTop: Dimensions.get('window').height/4.5,
        marginLeft: Dimensions.get('window').width/4,
        alignItems:'center',
        justifyContent: 'center',
        position:'absolute',

            color: 'white',


    },

    button1:{
        backgroundColor:'white',
       //backgroundColor:('#003359'),
       shadowOffset:{  width: 0,  height: 2},
       shadowColor: 'rgba(0,0,0,0.16)',
       shadowOpacity: 1.0,
       shadowRadius:6,
       elevation: 5,
        //fontSize: 100,
        width: Dimensions.get('window').width/2,
        height: Dimensions.get('window').width/6,
        alignSelf:'center',
        justifyContent: 'center',
        borderRadius: 7,
        position:'relative'

        //flex: 1,
        //resizeMode: Image.resizeMode.contain,
    },
    buttonText:{

        color: '#5A5A5A',
        fontFamily: 'Rubik-Regular',
        fontSize: 16,
        textAlign:'center',
    },

    buttonModal:{
        backgroundColor:'white',
       //backgroundColor:('#003359'),
       shadowOffset:{  width: 0,  height: 2},
       shadowColor: 'rgba(0,0,0,0.16)',
       shadowOpacity: 1.0,
       shadowRadius:6,
        //fontSize: 100,
        width: Dimensions.get('window').width*.28,
        height: Dimensions.get('window').height*.075,
       // alignSelf:'center',
        justifyContent: 'center',
        borderRadius: 7,
        position:'relative',
        marginTop: height*0.02,
        marginBottom: height*0.02,

        //flex: 1,
        //resizeMode: Image.resizeMode.contain,
    },
wave:{
        position:'absolute',
        marginTop: height*.78,
        alignSelf:'center',
        width: width*1.1,


},

regionbutton: {
    //  flex: 1,
      justifyContent: 'center',

      alignItems: 'center',
      borderRadius: 15,
      height: height*.078,
      width:width*.4,
      marginTop:height*.02,
      marginBottom: height*.02,
   //   paddingTop:height*.005,
    //  paddingBottom: height*.005,
    },
  dambutton: {
marginBottom: height*.02,
height: height*.078,
width:width*.4,

 // backgroundColor:'#00AFD1',
 //flex: 1,
 justifyContent: 'center',
 alignItems: 'center',
  borderRadius: 15,
  marginTop:height*.02,
  //paddingTop:height*.005,
 // paddingBottom: height*.005,
},

textInput: {
    fontSize: 18,
    fontFamily:'Rubik-Regular',
    borderColor:'#e5e5e5',
    borderWidth:1,

    width: width*.7,
    height: height*.05,
    alignSelf:'center',
    color:'#5a5a5a',


  },

  modalInnerView: {
      alignItems: 'center',
      backgroundColor:'transparent',

  },

  emailConfirmButton: {
    backgroundColor: '#00AFD1',
    height: height*.08,
    width: width*.3,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    marginTop: 20,

  },

  emailConfirmText:{
    color: 'white',
    fontSize: 16,
    fontFamily:'Rubik-Regular',
    backgroundColor:'transparent'


  },

  filterButtons:{
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: Dimensions.get('window').height*.07,
    alignItems:'center',
    justifyContent:'center',
   // borderColor:'black',
   //borderWidth:1,
    backgroundColor: '#00AFD1'
   },
navyheading:{

    color: '#004960',
    textAlign:'left',
    fontSize: 18,
    fontFamily: 'Rubik-Regular',
    marginBottom:height*.01,
   // marginTop:height*.15,
},

navybody:{

    color: '#004960',
    textAlign:'left',
    fontSize: 16,
    fontFamily: 'Rubik-Regular',
   // marginBottom:height*.15,
   // marginLeft:width*.1,
    //marginRight:width*.1,
    marginTop:height*.05
},

modalHeading:{

    color: '#004960',
    textAlign:'left',
    fontSize: 16,
    fontFamily: 'Rubik-Regular',
    marginBottom:height*.05,
    marginLeft:width*.1,
    marginRight:width*.1,
    marginTop:height*.05,
    left: width*.02
},
greybody:{
    color: '#5A5A5A',
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    textAlign:'center',
    marginLeft:width*.1,
    marginRight:width*.1,
    marginBottom:height*.15

},


greybody1:{
    color: '#5A5A5A',
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    right: width*.1,
    //textAlign:'center',
    //marginLeft:width*.1,
    //marginRight:width*.1,
    marginBottom:height*.05,
    marginTop:height*.1

},

borderline:{
    marginLeft: width*.1,
     marginRight: width*.1,
     borderBottomColor:'#00AFD1',
     borderBottomWidth:1
},
logoStyle: {
marginTop:height*.005,
marginBottom: height*.05,
  width: width*.9,
  height: height*.8,
  resizeMode:'contain'
},

borderline1:{
    marginLeft: width*.075,
     marginRight: width*.075,
     borderBottomColor:'#EEAF00',
     borderBottomWidth:1}
})

{/*
<View style = {{flex: 1, backgroundColor:'white'}}>



<Image source = {this.state.sitevisited?require('./backgrounddark.png'):require('./backgrounddark.png')} style ={styles.background}>

{this.state.sitevisited?<View style = {{alignSelf:'center', top: Dimensions.get('window').height/8}}>


</View>:null}



<View style= {{alignContent:'center'}}>
<Image source = {require('./logo.png')}style = {{height: height/15, marginTop: height*.05,marginBottom: height*.05,alignSelf: 'center', width: width /2,position: 'relative',resizeMode:'contain'}}/>
</View>

{ !this.state.sitevisited?<View style = {{alignContent:'center'}}>
<View style = {[styles.borderline,{marginTop: height*.1}]}>

  <Text style={styles.navyheading}>
 Terms of Use</Text>
 </View>
 <Text style ={styles.navybody}>
 Please read the SunWater Terms of Use carefully before using this application. By accessing services through this application, you agree to observe these Terms of Use. If you do not accept all of these Terms of Use then please stop using this application immediately.

</Text>

<View style = {{marginTop: height*.15}}>

<TouchableOpacity onPress={() => Linking.openURL('http://www.sunwater.com.au/home/terms-of-use')}>

               <Text  style = {styles.greybody} >  View SunWater's Terms of Use</Text>
                  </TouchableOpacity>

</View>




  <TouchableOpacity style ={[styles.button1,{marginTop:height*0.0391}]} onPress={this.acceptTerms.bind(this)} >


              <Text style = {styles.buttonText}>{buttonname}
                  </Text>

                  </TouchableOpacity>


              </View> :null}
              {//ENd
              }

      </View>
*/}







//export default TeamScroll;
