//TODO

'use strict'
import React, { Component } from 'react'

import {Alert, StyleSheet,Dimensions, ActivityIndicator, Switch, NetInfo, TextInput, View, ListView,AsyncStorage, ScrollView, TouchableOpacity, TouchableWithoutFeedback,Keyboard,Image, Text, TouchableHighlight, Button, Linking } from 'react-native'

import FAIcon from 'react-native-vector-icons/FontAwesome'
import Container from '../../Component/Container'
//import PercentageCircle from 'react-native-percentage-circle'
//import ProgressCircle from 'react-native-progress-circle'
import Modal from 'react-native-simple-modal'
import * as firebase from 'firebase';
import {NavigationActions} from 'react-navigation';
var DeviceInfo = require('react-native-device-info');
import AlertsScreen from '../AlertsScreen'
import Alerts from './Alerts'
import Drawer from 'react-native-drawer-menu';
import CustomMultiPicker from "react-native-multiple-select-list";
import Expand from 'react-native-simple-expand';
import OneSignal from 'react-native-onesignal';



import Spinner from 'react-native-loading-spinner-overlay';
//import AlertsScreen from '../../Screens/AlertsScreen';

const width = Dimensions.get("window").width
const height = Dimensions.get("window").height
const percentrate = 54
const buttonname ='Continue'
const sitevisited = true

export default class Feedback extends Component {
//    static navigationOptions = ({navigation}) => ({
   /* static navigationOptions = {
        gesturesEnabled: false,
    }*/
     // });

    constructor(props){
        super(props);
        this.initialState = {
            sitevisited : true,
            gotoNext: false,
            dbConnectionFailed: false,
            status: false, //internet connectivitt
           //SPINNER
           animating: true,
            valID : '',
            deviceID : '',
            open: true,
            open1:false,
            isOpened:false,
            screen:'zero',
            newMail:'',
            emailAddress:'',
            isEmailSelected:false, //email wizard toggle
            showPopUp:false, //show email popup
            isPushSelected:false, //push wizard toggle
            getTags:'',
            selectItems: [],
            selectItems1: [],
            newselectitems: [],
            key : 0,
            name: '',
            email: '',
            message: '',

        }
        this.focusNextField = this.focusNextField.bind(this);
        this.inputs = {};
        this.state = this.initialState;
    }
    focusNextField(id) {
        this.inputs[id].focus();
      }
/*
    checkIfUserExists(userId) {
        var exists = false;
        firebase.database().ref(`server/saving-data/fireblog/subs`).once('value').then(snapshot => {
            //console.log('Subs Data: ' + snapshot + 'huh ' + snapshot.val);
            if(snapshot && snapshot.val){
                if(snapshot.hasChild(udid)){
                    console.log('haschild');
                    exists = true;
                }
            }
        });
        return exists;
    }

*/


    componentWillMount(){


    }




_loadInitialState = async () => {

  };

    componentDidMount() {

        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);

        NetInfo.isConnected.fetch().done(
          (isConnected) => { this.setState({ status: isConnected });

//VALUE
this._loadInitialState().then(success => {//GET ASYNC VARS FROM DEVICE and set to states

var udid = DeviceInfo.getUniqueID(); //get id from device
this.setState({deviceID: udid});

//if (this.state.Wizard){ //if Wizard is true
    if (this.state.status){ //if connected to internet
        firebase.database().ref(`server/saving-data/fireblog/subs`).once('value').then(snapshot => { //udid
            //console.log('Subs Data: ' + snapshot + 'huh ' + snapshot.val);
            if(snapshot && snapshot.val){
                if(snapshot.hasChild(udid)){
                    console.log('haschild');
    //Device ID is found in database!
    //===== check subscriptions and email are not null =====
    firebase.database().ref(`server/saving-data/fireblog/subs/${udid}`).once('value').then(snapshot => {
        //IF NO SUBSCRIPTIONS OR EMAIL
        if (((snapshot.val().email=="")||(snapshot.val().email==undefined)) && (snapshot.val().locs == undefined)) {
            AsyncStorage.setItem('WIZARD_KEY','false'); //set wizard to FALSE
            this.setState({Wizard:false});
        }
        else{
            AsyncStorage.setItem('WIZARD_KEY','true'); //set wizard to FALSE
            console.log("set wizard TRUE");
            this.setState({Wizard:true});
        }
    })//end firebase
    //================================================
                 }//end ID exists in db
            }
        })//end firebase
    }//end if Connected
//}//end if wizard


}); //END LOAD INITIAL STATE

});

}//end DIDMOUNT





    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);

    }


  handleConnectionChange = (isConnected) => {
    this.setState({ status: isConnected });

    console.log(`is connected: ${this.state.status}`);
}


componentDidUpdate(){
    /*
    setInterval(() => {
        this.setState({
          visible: !this.state.visible
        });
      },3000);
      */

}

submitFeedback(){
    var message=this.state.message;
    var name = this.state.name;
    var email=this.state.email;
    var date = new Date().getTime();

    console.log('checking connection');
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
    NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
                NetInfo.isConnected.fetch().done(
                    (isConnected) => { this.setState({ status: isConnected }); //alert(this.state.status)

if (this.state.status){

    let re = RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
    if (re.test(email)){//valid email
        if((name!= '')&&(message!= '')){ //valid name and message
            var update = {date, message};

            firebase.database().ref(`server/saving-data/fireblog/feedback/${this.state.deviceID}`).update({
                [date]:message,
            });
            if (email != ''){
                firebase.database().ref(`server/saving-data/fireblog/feedback/${this.state.deviceID}`).update({
                    email: this.state.email,
                });
            }
                if (name != ''){
                    firebase.database().ref(`server/saving-data/fireblog/feedback/${this.state.deviceID}`).update({
                        name: this.state.name,
                    });
            }
            //Alert.alert('Feedback successfully submitted')
            const { navigate } = this.props.navigation;
            Alert.alert(
                'Success',
                'Feedback submitted.',
                [
                    {text: 'OK', onPress: () => navigate('HubScreen')},
                ]
            );
            console.log('start navigating');


            console.log('end navigating');
        }
        else{
            /*
            Alert.alert(
                'Error',
                'Please ensure all fields are filled.',
                [
                    {text: 'OK', onPress: () => this.dismiss()},
                ]
            );*/
            alert('Please ensure all fields are filled.');
        }
     }
    else {
        /*
        Alert.alert(
            'Error',
            'Invalid email address.',
            [
                {text: 'OK', onPress: () => this.dismiss()},
            ]
        );*/
        alert('Invalid email address.');

    }

}
else{
    alert('No internet connection - feedback could not be sent.');
}

});

}


    render() {


        return (


            <Container style = {{backgroundColor:'#fff'}} >

            <View>
            {this.renderNavBar()}
                <View style = {{height:height*0.8,backgroundColor:'transparent'}}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} style={{backgroundColor:'transparent'}}>
<View>



                    <View style={{marginLeft:width*.085}}>
                        <Text style = {{marginTop: height*.05,color:'#031E2F', fontSize:18, fontFamily:'Rubik-Medium'}}>Please provide feedback below</Text>
                    </View>

                    <Text style = {[styles.greybody,{marginTop: height*.025,marginLeft: width*.045,paddingRight:width*0.07}]}>We'd like to hear from you how we can improve this app.</Text>
                    <TouchableOpacity onPressIn={this.submitFeedback.bind(this)} onPressOut={Keyboard.dismiss} title="Submit"
                        style={[styles.button1,{marginTop:height*.02}]}>
                                {/*backgroundColor:'#111028'}}>*/}
<Text style = {styles.buttonText}>Submit</Text>
                        </TouchableOpacity>
                        </View>
          </TouchableWithoutFeedback>
<View style={{height:height*0.8,marginTop: height*.02,backgroundColor:'transparent'}}>
            <ScrollView style={{paddingLeft:width*.0275, width: width,backgroundColor:'transparent'}} contentContainerStyle={{
      flexGrow: 1,
  }}>{/*marginBottom:height*.5, */}
                <Text style = {styles.greybody}>Your Name</Text>

<TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                 <TextInput
                   ref= {(na) => { this.name = na;
                }}
                autoCorrect={false}
                   onChangeText={(name) => this.setState({name})}
                   value={this.state.name}
                      returnKeyType= 'next'
                      onSubmitEditing={() => {
                        this.refs.secondinput.focus();
                      }}
                 style = {{borderRadius: 5,marginBottom: height*.02, height:height*.065,borderColor: '#a2a2a2',marginLeft: width*.05,width: width*.835, paddingRight: width*.05,backgroundColor:'transparent', borderWidth:1}}/>
                 </TouchableWithoutFeedback>
                <Text style = {styles.greybody}>Email</Text>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                 <TextInput
autoCorrect={false}
                   ref= 'secondinput'
                   onChangeText={(email) => this.setState({email})}
                   value={this.state.email}
                   onSubmitEditing={() => {
                    this.refs.thirdinput.focus();
                  }}
                  returnKeyType={ "next" }
                 style = {{borderRadius: 5, marginBottom: height*.02, height:height*.065,borderColor: '#a2a2a2',marginLeft: width*.05,width: width*.835, paddingRight: width*.05,backgroundColor:'transparent', borderWidth:1}}/>
                </TouchableWithoutFeedback>
                 <Text style = {styles.greybody}>Message</Text>
                 <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                 <TextInput
                  ref= 'thirdinput'
                  onChangeText={(message) => this.setState({message})}
                  value={this.state.message}
                  keyboardType='default'
                  multiline = {true}
                  returnKeyType = 'default'
                  autoCorrect={false}
                 numberOfLines = {20} style = {{marginBottom: height*0.5,fontSize: 17,borderRadius: 5,height :height*.15, marginLeft: width*.05,width: width*.835,backgroundColor:'white', borderColor:'#a2a2a2',borderWidth:1}}/>
                </TouchableWithoutFeedback>
</ScrollView>
</View>
</View>
{/*}
        <View style={{backgroundColor:'transparent'}}>
        <TouchableOpacity style ={[styles.button1]} onPress={() => {this.props.navigation.goBack()}} >
        <Text style = {styles.buttonText}>Return to Hub
        </Text>
        </TouchableOpacity>
        </View>
                */}
</View>

</Container>)}


renderNavBar() {
    const { navigation } = this.props
    return (
      <View style={styles.navBar}>
      {/*BARS*/}

      <Image source = {require('./HeaderBar.png')} style = {{marginTop: height*.075,marginLeft: width*.12}}/>

      <TouchableOpacity
                style={{ flexDirection: 'row', alignItems: 'center',marginTop:height*.05,position:'absolute', height: height*.1, width: width*.3}}
                onPress={() => { this.props.navigation.goBack();}} >

                <FAIcon name='home' size={30} style={{color: '#111D28',marginLeft:width*.05,marginTop: height*.02, paddingBottom:height*.009}} />
            </TouchableOpacity>




<Text style={styles.navyheading}>
Have your say</Text>


        </View>

    )
}
    renderHeader() {


        return (
            <TouchableOpacity
            style={{ flexDirection: 'row', alignItems: 'center',marginTop:height*.05, height: height*.1,left:width*.01, width: width*.2}}
        onPressIn={() => {this.props.navigation.goBack()/* this.props.onMenuPress();*/}} onPressOut={Keyboard.dismiss}>
            <FAIcon name='arrow-left' size={25} style={{color: '#000',marginLeft:width*.07,paddingBottom:height*.009}} />
        </TouchableOpacity>

        )
};


}//endclass

/////STYLES
const styles = StyleSheet.create({
    buttonSubmit: {
      marginBottom: 10,
      backgroundColor: '#2196F3',
    },

    testButton:{
        backgroundColor:'white',
        flex:1,
        top: 70,
        height: 'auto',
        width: 400,
        position: 'absolute',
        alignItems:'center'
    },
    testButton2:{
        backgroundColor:'white',
        flex:1,
        top: 190,
        height: 'auto',
        width: 400,
        position: 'absolute',
        alignItems:'center'
    },
    testButton3:{
        backgroundColor:'white',
        flex:1,
        top: 310,
        height: 'auto',
        width: 400,
        position: 'absolute',
        alignItems:'center',

    },
    regionbutton: {
    //  flex: 1,
      justifyContent: 'center',

      alignItems: 'center',
      borderRadius: 15,
      height: height*.078,
      width:width*.4,
      marginTop:height*.02,
      marginBottom: height*.02,
   //   paddingTop:height*.005,
    //  paddingBottom: height*.005,
    },
  dambutton: {
marginBottom: height*.02,
height: height*.078,
width:width*.4,

 // backgroundColor:'#00AFD1',
 //flex: 1,
 justifyContent: 'center',
 alignItems: 'center',
  borderRadius: 15,
  marginTop:height*.02,
  //paddingTop:height*.005,
 // paddingBottom: height*.005,
},


listStyle:{

    width: Dimensions.get('window').width,
    borderTopColor:'#fff',
    //borderBottomColor: '#71D7EB',
    borderBottomColor: '#3f98ac',
    paddingRight:width*.06,
    //borderTopWidth: 0.5,
    borderBottomWidth: 1,
    //marginLeft: 15,
    //marginTop:2,
    //minHeight: 90,
    backgroundColor:'rgba(0,0,0,0)',
    //borderRadius:10,
//    borderColor:'black',
    height: 'auto',
   // borderWidth: 1,
    //textAlign: 'center'
},

    container: {
        shadowColor: '#000000',
        shadowOpacity: 0.4,
        shadowOffset: { height: -5, width: -5 },
        shadowRadius: 10,
        backgroundColor: 'transparent'
    },
    containerbg: {
        flex: 1,
        position: 'relative',
        resizeMode: 'cover',
        backgroundColor: 'transparent'
    },
    navBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: height*.1,
        alignItems: 'center',
        position: 'relative',
      //  paddingHorizontal: 25,
        //backgroundColor: '#4086c0'
        backgroundColor:'transparent',
       // marginBottom: height*.04,

     paddingBottom: height*.08,


    },
    navyheading:{
        position:'absolute',
        color: 'white',
        textAlign:'left',
        fontSize: 18,
        fontFamily: 'Rubik-Regular',
        marginBottom:height*.01,
        marginLeft: width*.4,
        marginTop:height*.15,
      },

    siteName: {
        marginTop: 30,
        width: 250
    },
    logo: {
        color: 'white',
        marginTop: 10
    },
    stretch: {
        flex: 2,
        height: undefined,
        width: undefined,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundColor: 'transparent'
    },
    baseText: {
        fontFamily: 'Snell Roundhand',
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 30,
        color: 'white',
        position: 'relative',
    },
    topButtons: {
      // display: 'flex',
      // flex: 1,
      width: width,
      height: 'auto',
      flexDirection: 'row',
      position: 'relative',

    },
    topbutton1: {
      // flex:1,
      //  width: 180,
      width: width / 2,
      height: 'auto',
      position: 'relative',
      borderColor:'#fff',
      borderWidth:1,

   },

    topButtonImg: {
      width: width / 2,
      //height: 90,
      position: 'relative',
      height: width *0.45/2,
    },

   filterButtons:{
    position: 'relative',
    bottom: 0,
    left: 0,
    right: 0,
    height: Dimensions.get('window').height*.08,
    alignItems:'center',
    justifyContent:'center',
    marginBottom:height/3,
    marginTop:height*.02,
    //top: height*.81,
   // borderColor:'black',
   //borderWidth:1,
    backgroundColor: '#00AFD1'
   },

   topbutton2: {
    // flex:1,
    //  width: 180,
    width: width / 2,
     height:'auto',
     borderColor:'#fff',
     borderWidth:1,

    //  left:180,
    //  bottom: 90,
    //  position: 'relative',
 },

 topbutton3: {
  // flex:1,
   width: 60,
   height:60,
   top:10,
   left:100,
   position: 'absolute',
},

modalContainer: {

 backgroundColor:'black'

},

textInput: {
  fontSize: 18,
  fontFamily:'Rubik-Regular',
  borderColor:'#00AFD1',
  borderWidth:1,
  borderRadius: 15,
  width: width*.7,
  height: height*.07,
  alignSelf:'center',
  color:'#00AFD1',




},

modalInnerView: {
    alignItems: 'center',
},

emailConfirmButton: {
  backgroundColor: '#00AFD1',
  height: height*.08,
  width: width*.3,
  borderRadius: 15,
  justifyContent: 'center',
  alignItems: 'center',
  padding: 10,
  marginTop: 20,

},

emailConfirmText:{
  color: 'white',
  fontSize: 16,
  fontFamily:'Rubik-Regular',
  backgroundColor:'transparent'


},
button1:{
    backgroundColor:'#00AFD1',
   //backgroundColor:('#003359'),
   shadowOffset:{  width: 0,  height: 2},
   shadowColor: 'rgba(0,0,0,0.16)',
   shadowOpacity: 1.0,
   shadowRadius:6,
   elevation: 5,
    //fontSize: 100,
    width: Dimensions.get('window').width*.85,
    height: Dimensions.get('window').width/6,
    alignSelf:'center',
    justifyContent: 'center',
   // borderRadius: 7,
    position:'relative'

    //flex: 1,
    //resizeMode: Image.resizeMode.contain,
},
buttonText:{

    color: 'white',
    fontFamily: 'Rubik-Medium',
    fontSize: 16,
    textAlign:'center',


},

  borderline:{
    marginLeft: width*.08,
     marginRight: width*.08,
     borderBottomColor:'#00AFD1',
     borderBottomWidth:1},

  navybody:{

    color: '#004960',
    //textAlign:'left',
    fontSize: 14,
    fontFamily: 'Rubik-Regular',
   // marginBottom:height*.15,

  },
  greybody:{
    color: '#5A5A5A',
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    paddingLeft:width*0.045,
   // paddingRight:width*0.05

},
})
