//Commenting by sections
//DATA FROM FIREBASE
//WRITING ALERTS TO APP SCREEN
//TIMESTAMP
//ALERT BUTTONS
//OPEN SIDE MENU
//LIST OF PLACES TO CHOOSE ON FILTER MENU


//TODO first visit? bool for database
//Region remember
//Region write
//Exception for no image
//Top banner
//Accept terms to Database == true
//Accept terms to database from Offline mode





/////LOCAL STORAGE CODE FOR UDID
/*_loadInitialState = async () => {
    try {
      var value = await AsyncStorage.getItem('STORAGE_KEY');
      if (value !== null){
        this.setState({selectedValue: value});
        alert('Recovered selection from disk: ' + value);
        this.setState({valID : value.toString()});
      } else {
        alert('Initialized with no selection on disk.');
      }
    } catch (error) {
      alert('AsyncStorage error: ' + error.message);
    }

  };

 async _setInitialState  (STORAGE_KEY) {
    try {
        console.log(STORAGE_KEY);
      var value = await AsyncStorage.setItem('STORAGE_KEY' ,STORAGE_KEY);
    } catch (error) {
      alert('AsyncStorage error: ' + error.message);
    }
  };

  ==================================================

  onAccept(){
    console.log(this.state.deviceID);
    var uID = this.state.deviceID.toString();
    console.log('###################'+uID);

    this._setInitialState(uID).done();
    firebase.database().ref(`server/saving-data/fireblog/subs/${this.state.deviceID}`).set({
        email:''
      });
      const { navigation } = this.props;
      navigation('AlertsScreen');
}
  */






'use strict'
import React, { Component } from 'react'

import {
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Button,
  ScrollView,
  Switch,
  Image,
  Text,
  List,
  NetInfo,
  Alert,
  FlatList,
  Dimensions,
  RefreshControl,
  AsyncStorage,
  AppState
} from 'react-native'
import OneSignal from 'react-native-onesignal'
import FAIcon from 'react-native-vector-icons/FontAwesome'
import Container from '../../Component/Container'
import TestScreen from '../../Screens/TestScreen';
import * as firebase from 'firebase';
import { ListItem, Card } from 'react-native-elements';
import Drawer from 'react-native-drawer-menu';
import PushMenu from '../../Component/PushMenu';
import Menu from '../../Component/Menu';
import { SideMenu} from 'react-native-elements';
import CustomMultiPicker from "react-native-multiple-select-list";

import Modal from 'react-native-simple-modal';
import Spinner from 'react-native-loading-spinner-overlay';

import PropTypes from 'prop-types';

var DeviceInfo = require('react-native-device-info');
var offlineMode = ""
const width = Dimensions.get("window").width
const height = Dimensions.get("window").height


export default class AlertsScreen extends Component<{}> {
    static navigationOptions = {
      title: 'Alerts',

    gesturesEnabled : true

  };

    constructor(props) {
        super(props);

        this.initialState = {
          data: [],
          filterdata: undefined,
          switchValue: false,
          backgroundColourRegions: '#FFF',
          backgroundColourDams: '#00AFD1',
          ColourRegions: '#00AFD1',
          ColourDams: '#FFF',
          completedTutorial: '',
          isEmailSelected: false,
          isPushSelected: false,
          isOpened : false,
          lastRefreshed : '',
          position: 'left',
          filterFlag: false,
          filterKey: 'all',
          selectItems: [],
          selectItems1: [],
          getTags: [],
          isViewSelect: false,
          isRegionSelect: false,
          deviceID: '',
          emailAddress: '',
          open1: false,
          open: false,
          refreshing: false,
          pulling: true,
          filters: '',
          status : false,
        newMail: '',
        subs:[],
        subbedregions:[],
        appState: AppState.currentState,

          }
          //console.log('params: '+this.props.navigation.state.params);
          //console.log(this.props.navigation.state.params);
          //
          console.log('params: '+this.props.navigation.state.params);

        if ( this.props.navigation.state.params !== undefined) {

          //alert(this.props.navigation.state.params.value.key);
          //this.setState({open: true, open1: false});

          this.setState({ isOpened: !this.props.navigation.state.params.value.key });

          let open = true;
          let isOpened = !this.props.navigation.state.params.value.key;

          //OPENS FROM ALERT SETTINGS ON SIDE MENU
          this.initialState.open = open;
          this.initialState.isOpened = isOpened;
          this.initialState.pulling= true;
          this.initialState.status= false;
          this.initialState.filters= '';



        }
        else if (this.props.navigation.state.params == undefined){
         // console.log("this was undefined");
          this.initialState.pulling= true;
          this.initialState.status= false;
          this.initialState.filters= '';
          //this.initialState.isOpened = true;
        }
        else{
          this.initialState.pulling= true;
          this.initialState.status= false;
          this.initialState.filters= '';
          //console.log('both did not work out');
        }

        this.state = this.initialState;
      }
      /*
      updateState (data) {
        this.setState(data);
    }*/

    _onPress(){
        const newState = !this.state.toggle;
        this.setState({toggle:newState})

    }

    /*if (!this.state.dbConnectionFailed){
    firebase.database().ref(`server/saving-data/fireblog/subs/${this.state.deviceID}`).set({ //set ID to database
        email:''
      });
    }*/

    getNewTime(){
      var rawstamp = new Date();
      var timestamp = rawstamp.toLocaleTimeString([], {hour: 'numeric',minute:'numeric', hour12: true});// timestamp.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
  var month = (rawstamp.getMonth()+1).toString()
  var monthstring = ""
      if(month==1){
    monthstring = "Jan"
  } else if(month==2){
    monthstring = "Feb"
  } else if(month==3){
    monthstring = "Mar"
  } else if(month==4){
    monthstring = "Apr"
  } else if(month==5){
    monthstring = "May"
  } else if(month==6){
    monthstring = "Jun"
  } else if(month==7){
    monthstring = "Jul"
  } else if(month==8){
    monthstring = "Aug"
  } else if(month==9){
    monthstring = "Sep"
  } else if(month==10){
    monthstring = "Oct"
  } else if(month==11){
    monthstring = "Nov"
  } else if(month==12){
    monthstring = "Dec"}

      var datestamp = rawstamp.getDate().toString() + " " + monthstring + " " + rawstamp.getFullYear().toString();
      var constamp = timestamp +", " + datestamp;
      AsyncStorage.setItem('TIME_KEY' ,constamp);
      this.setState({lastRefreshed : constamp});
      //alert(rawstamp.toDateString() + "\n" + constamp);
    }

    fetchData = () =>{


  //OFFLINE
if (!this.state.status){
  AsyncStorage.getItem('fetchSubs', (err, result) => {
    //   console.log(result);
      if(!err){
        if (result){
          this.setState({subs:JSON.parse(result)});
        AsyncStorage.getItem('fetchData', (err, result) => {
          if(!err){
            if(result) {
              this.processFetchedData(JSON.parse(result)); //LOCAL STORAGE
            }
            else{
              console.log('NULL DATA STORAGE RESULT');
              this.setState({refreshing: false});
              this.setState({pulling: false});
            }
        //    this.setState({pulling: false});
          }
          else{
            console.log(err);
          }
        })
      }
      else{
        console.log('NULL SUB STORAGE RESULT');
        this.setState({refreshing: false});
        this.setState({pulling: false});
      }
      }
      else{
        console.log(err);
      }
    })

}
  //ONLINE
else{


   firebase.database().ref(`server/saving-data/fireblog/subs`).once('value').then(snapshot => {
          //console.log('Subs Data: ' + snapshot + 'huh ' + snapshot.val);
          //alert(snapshot);
          //alert(snapshot.val);
          if(snapshot && snapshot.val){
              if(snapshot.hasChild(this.state.deviceID)){ //if user exists then...
                  console.log('haschild');
                  //alert('exist true');


      //...continue to access user sub data in db
      firebase.database().ref(`server/saving-data/fireblog/subs/${this.state.deviceID}`).once('value').then(snapshot => {
        //console.log('Subs Data');
        //CHAO
        if ((snapshot.val().locs == undefined)) { //if ((snapshot.val().email=="") && (snapshot.val().locs == undefined)) { //ALEX
        //if ((snapshot.val().regions == undefined)) {

          this.setState({filters:false});
          this.setState({pulling:false});

          let val = snapshot.val();

          var subs = [];
          var selectitems = [];
          var selectitems1 = [];
          if(val.regions && val.regions !== undefined){
            Object.keys(val.regions).forEach(function(key){
              selectitems1.push(key);
                            });
          }
          this.setState({
            subs: subs,
            subbedregions:selectitems1,
            emailAddress: val.email,
          selectItems1 : selectitems1,
            selectItems: selectitems,
            isViewSelect: true,
            isRegionSelect: false,
            isPushSelected: val.push == undefined? false: val.push,
            isEmailSelected: val.emailSub == undefined? false: val.emailSub,
          });

        if(!this.state.isOpened) {
        Alert.alert(
                  'No Alert Filters Detected',
                  'Would you like to set up your alert settings?',
                  [
                        {text: 'OK', onPress: () => this.tutorial()},
                        {text: 'Cancel', onPress: () => Alert.alert('Select the cog icon to access your filter settings at any time.'), style: 'cancel'}              ]
                )
        }
      }
        else if (snapshot && snapshot.val()) {
          let val = snapshot.val();
          console.log('REFRESH WITH SUBS');
          console.log(val);
          var selectitems = [];
          var selectitems1 = [];
          var subs=[];

          Object.keys(val.locs).forEach(function (key) {
              subs.push(key);
              selectitems.push(val.locs[key]);
          });

          Object.keys(val.regions).forEach(function(key){
                selectitems1.push(key);
          });

          /* //ALEX CHANGE FRI 5:11pm
          if (val.locs && val.locs !== undefined) {
            Object.keys(val.locs).forEach(function (key) {
              subs.push(key);
            selectitems.push(val.locs[key]) });
          }
          if(val.regions && val.regions !== undefined){
              Object.keys(val.regions).forEach(function(key){
                selectitems1.push(key);
                              });
          }
          */
          this.setState({

            subs:subs,
            subbedregions:selectitems1,
            emailAddress: val.email,
          selectItems1 : selectitems1,
            selectItems: selectitems,
            isViewSelect: true,
            isRegionSelect: false,
            isPushSelected: val.push == undefined? false: val.push,
            isEmailSelected: val.emailSub == undefined? false: val.emailSub,
          });
          console.log('subs'+this.state.subs);
          this.setState({filters:true});
          this.setState({pulling:false});
        }
      })

    }
    else{//else if user doesnt exist
//      alert('user does not exist in db')
      //create user
        firebase.database().ref(`server/saving-data/fireblog/subs/${this.state.deviceID}`).set({ //set ID to database
          email:''
      });
      this.setState({filters:false});
      this.setState({pulling:false});

      var subs = [];
      var selectitems = [];
      var selectitems1 = [];

      this.setState({
        subs: [],
        subbedregions:[],
        emailAddress: '',
      selectItems1 : [],
        selectItems: [],
        isViewSelect: true,
        isRegionSelect: false,
        isPushSelected: false,
        isEmailSelected: false,
      });

      if(!this.state.isOpened) {
            Alert.alert(
              'No Alert Filters Detected',
              'Would you like to set up your alert settings?',
              [
                    {text: 'OK', onPress: () => this.tutorial()},
                    {text: 'Cancel', onPress: () => Alert.alert('Select the cog icon to access your filter settings at any time.'), style: 'cancel'}              ]
            )
          }
    }
}

});
    //continue to access post data in db
      console.log('yeah');
      firebase.database().ref('server/saving-data/fireblog/posts').once('value').then(snapshot => {
        if (snapshot && snapshot.val()) {
          let val = snapshot.val();

          let data = Object.keys(snapshot.val()).map(key => ({'id': key, ...val[key]}));

        data = data.reverse();
        console.log("============DATA================")
          console.log(data);
          this.setState({data});
          console.log(JSON.stringify(data));
          AsyncStorage.setItem('fetchData', JSON.stringify(val), (err, result) => { //LOCAL STORAGE
            //alert('locally stored');
            AsyncStorage.setItem('fetchSubs', JSON.stringify(this.state.subs), (err, result) => { //LOCAL STORAGE
              //alert('locally stored');
              console.log('fetched data and subs have been saved locally');
              console.log(this.state.data);
              console.log(this.state.subs);
              this.getNewTime();
            })
          })
        }
      })

     this.setState({refreshing: false});

    }//end if else internet
          } //end fetch data


tutorial(){
  this.setState({open:true});
  this.toggleMenu();

}

/*
  fetchSubs = () =>{
    console.log('yeahsubs');
    firebase.database().ref(`server/saving-data/fireblog/subs/${udid}`).once('value').then(snapshot => {
      console.log('Subs Data');
      if (snapshot && snapshot.val()) {
        let val = snapshot.val();
        var selectitems = this.state.selectItems;
        var selectitems1 = this.state.selectItems1;
        var subs = [];
        if (val.locs && val.locs !== undefined) {
          Object.keys(val.locs).forEach(function (key) {
          selectitems.push(val.locs[key]) });
          subs.push(key);
        }
        if(val.regions && val.regions !== undefined){
            Object.keys(val.regions).forEach(function(key){
              selectitems1.push(key);
                            });
        }
        this.setState({
          subbedregions:selectitems1,
          subs:subs,
          emailAddress: val.email,
        selectItems1 : selectitems1,
          selectItems: selectitems,
          isPushSelected: val.push == undefined? false: val.push,
          isEmailSelected: val.emailSub == undefined? false: val.emailSub,
        });

        //console.log('view selected: ', this.state.isViewSelect);
        //console.log('view selected: ', this.state.isRegionSelect);
        //console.log('selectedItem: ' + this.state.selectItems);
        //console.log('selectedRegions: ' + this.state.selectItems1);
      }
    })
    this.setState({refreshing: false});
    }
*/
          processFetchedData(val){
            let data = Object.keys(val).map(key => ({'id': key, ...val[key]}));
            data = data.reverse();
            console.log('archive data');
            console.log(data);
            console.log(this.state.subs);
            this.setState({data});
            this.setState({refreshing: false});
            this.setState({pulling: false});
          }
          //onValueChange={this.togglePushOption.bind(this)}

    _onRefresh() {
      //checkConnection();
      if (this.state.status){
      this.getNewTime();
      }
      this.setState({refreshing: true});
      this.fetchData();

    }

    _onSubRefresh() {
      console.log('subrefresh');
            this.setState({refreshing: true});
            this.fetchSubs();
                    }


      componentDidMount() {
        //alert('did mount');
        AppState.addEventListener('change', this._handleAppStateChange);


    NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);

    NetInfo.isConnected.fetch().done(
      (isConnected) => { this.setState({ status: isConnected }); this._refreshAlerts(); }
    );

  } //END COMPONENTdidMOUNT

  componentWillUnmount() {
    //alert('unmount');
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!');
      //alert('state change!');
      this.checkConnection();
      //this._refreshAlerts();

    }
    this.setState({appState: nextAppState});
  }

  handleConnectionChange = (isConnected) => {
    this.setState({ status: isConnected });
    this.setState({ pulling: true });
    this._refreshAlerts();
    console.log(`is connected: ${this.state.status}`);
}

checkConnection(){
  console.log('checking connection');
  NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
  NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
  NetInfo.isConnected.fetch().done(
    (isConnected) => { console.log(isConnected);this.setState({ status: isConnected }); }

  );

}

 _refreshAlerts() {

    var udid = DeviceInfo.getUniqueID();
    this.setState({ deviceID: udid });



//GET ONLINE/OFFLINE
//this.checkConnection();
//OFFLINE


if (!this.state.status){
  console.log(this.state.status);
 //DATA FROM FIREBASE
 AsyncStorage.getItem('fetchSubs', (err, result) => {
//   console.log(result);
  if(!err){
    if (result){
      this.setState({subs:JSON.parse(result)});
    AsyncStorage.getItem('fetchData', (err, result) => {
      if(!err){
        if(result) {
          this.processFetchedData(JSON.parse(result)); //LOCAL STORAGE
        }
        else{
          console.log('NULL DATA STORAGE RESULT');
          this.setState({refreshing: false});
          this.setState({pulling: false});
        }
    //    this.setState({pulling: false});
      }
      else{
        console.log(err);
      }
    })
  }
  else{
    console.log('NULL SUB STORAGE RESULT');
    this.setState({refreshing: false});
    this.setState({pulling: false});
  }
  }
  else{
    console.log(err);
  }
})

}
//ONLINE
else{

    AsyncStorage.getItem('TIME_KEY', (err, result) => {
      this.setState({lastRefreshed: result})
      //alert(result);
    });
this.setState({refreshing: true});

firebase.database().ref(`server/saving-data/fireblog/subs`).once('value').then(snapshot => {

      if(snapshot && snapshot.val){
          if(snapshot.hasChild(this.state.deviceID)){ //if user exists then...
              console.log('haschild');
              //alert('exist true');

  //...continue to access user sub data in db

    firebase.database().ref(`server/saving-data/fireblog/subs/${udid}`).once('value').then(snapshot => {
      //console.log('Subs Data');

      if ((snapshot.val().locs == undefined)) { //if ((snapshot.val().email=="") && (snapshot.val().locs == undefined)) { //ALEX
          let val = snapshot.val();
          this.setState({filters:false});
          this.setState({pulling:false});
          var subs = [];
          var selectitems = [];
          var selectitems1 = [];
          if(val.regions && val.regions !== undefined){
            Object.keys(val.regions).forEach(function(key){
              selectitems1.push(key);
                            });
          }
          this.setState({
            subs: subs,
            subbedregions:selectitems1,
            emailAddress: val.email,
          selectItems1 : selectitems1,
            selectItems: selectitems,
            isViewSelect: true,
            isRegionSelect: false,
            isPushSelected: val.push == undefined? false: val.push,
            isEmailSelected: val.emailSub == undefined? false: val.emailSub,
          });

      if(!this.state.isOpened) {
              Alert.alert(
                'No Alert Filters Detected',
                'Would you like to set up your alert settings?',
                [
                      {text: 'OK', onPress: () => this.tutorial()},
                      {text: 'Cancel', onPress: () => Alert.alert('Select the cog icon to access your filter settings at any time.'), style: 'cancel'}              ]
              )
            }

      }

      if (snapshot && snapshot.val()) {
        let val = snapshot.val();
        console.log('result');
        console.log(snapshot.val());
        var selectitems = []; //ALEX
        var selectitems1 = [];

        if (val.locs && val.locs !== undefined) {


          var subs = [];
          Object.keys(val.locs).forEach(function (key) {
            //console.log('key'+key);
            subs.push(key);
          selectitems.push(val.locs[key]) });
        }
       // console.log('locs');
        console.log(selectitems);
        if(val.regions && val.regions !== undefined){

            Object.keys(val.regions).forEach(function(key){
              selectitems1.push(key);
                            });
        }
       // console.log('locs2');
       // console.log(selectitems1);
       //console.log('subs: '+subs);
        this.setState({
          subs: subs,
          subbedregions:selectitems1,
          emailAddress: val.email,
        selectItems1 : selectitems1,
          selectItems: selectitems,
          isViewSelect: true,
          isRegionSelect: false,
          isPushSelected: val.push == undefined? false: val.push,
          isEmailSelected: val.emailSub == undefined? false: val.emailSub,
        });
        console.log('subs'+this.state.subs);
        this.setState({filters:true});
        this.setState({pulling:false});
        //console.log('view selected: ', this.state.isViewSelect);
        //console.log('view selected: ', this.state.isRegionSelect);
        //console.log('selectedItem: ' + this.state.selectItems);
        //console.log('selectedRegions: ' + this.state.selectItems1);
      }
    })
}
else{//else if user doesnt exist
  //alert('user does not exist in db')
  //create user
    firebase.database().ref(`server/saving-data/fireblog/subs/${this.state.deviceID}`).set({ //set ID to database
      email:''
  });
  //Alert.alert('user just created, no subs');

  //open cog
  var subs = [];
  var selectitems = [];
  var selectitems1 = [];

  this.setState({
    filters: false,
    subs: [],
    subbedregions:[],
    emailAddress: '',
  selectItems1 : [],
    selectItems: [],
    isViewSelect: true,
    isRegionSelect: false,
    isPushSelected: false,
    isEmailSelected: false,
  });
  this.setState({pulling:false});

  this.tutorial();

}
/*
    //DATA FROM FIREBASE
    AsyncStorage.getItem('fetchData', (err, result) => {
      if(!err){
        if(result) this.processFetchedData(JSON.parse(result)) //LOCAL STORAGE
      }
    })
*/
    console.log("----------Data from Firebase ------------");
    firebase.database().ref('server/saving-data/fireblog/posts').once('value').then(snapshot => {
      if (snapshot && snapshot.val()) {
        let val = snapshot.val();
        this.processFetchedData(val)
        //this.getNewTime();

        try{
          AsyncStorage.setItem('fetchData', JSON.stringify(val), (err, result) => { //LOCAL STORAGE
            //alert('locally stored');
            AsyncStorage.setItem('fetchSubs', JSON.stringify(this.state.subs), (err, result) => { //LOCAL STORAGE
              //alert('locally stored');
              console.log('fetched data and subs have been saved locally');
              console.log(this.state.data);
              console.log(this.state.subs);
              this.getNewTime();
            })
          })
        }catch(error){

        }
      }
    })

console.log('Posts Data:');


this.setState({refreshing: false});
}

});
}//end if
  }//end REFRESHALERTS

      keyExtractor = (item, message,index) => item.id;
    //  alert(facebook);
      onPress(id,name, subtitle,message, location,aud,images,time,date,filtered,facebook, twitter,tweet) {
        console.log('ONPRESS');

        const { navigate } = this.props.navigation;
        navigate('Details', {id,name, subtitle, message, location, aud,images,time,date,filtered, facebook, twitter,tweet});
       // console.log(facebook);
       console.log(id+name+subtitle+message+location+aud+images+time+date+filtered+facebook+twitter)
      }



//WRITING ALERTS TO APP SCREEN
      renderItem({item}) {
        //console.log('Render ITem');
     //   console.log(item.id);
//console.log(item);

var keys = Object.keys(item.locs);
var filtered = keys.filter(function(key) { return item.locs[key]});
//console.log(filtered);
//console.log(facebook);
//console.log(filtered.join());


var image = null;

//TIMESTAMP
//console.log('ITEM TIME' + item.time);
var x = (new Date()).getTime();
//console.log(x.toString());
var show = true;
var diff = (x-item.time)/1000;
var toShow = Math.round(diff)  + " seconds ago";
if(diff >60 ){
  diff = diff/60;
  if (Math.round(diff) == 1 ){
    toShow = Math.round(diff) + " minute ago";
  }
  else{
    toShow = Math.round(diff) + " minutes ago";
  }


if(diff > 60){
  diff = diff/60;
  if (Math.round(diff) == 1 ){
    toShow = Math.round(diff)  + " hour ago";
  }
  else{
    toShow = Math.round(diff)  + " hours ago";
  }


if(diff>24){
  diff = diff/24;
  if (Math.round(diff) == 1 ){
    toShow = Math.round(diff) + " day ago";
  }
  else{
    toShow = Math.round(diff) + " days ago";
  }


if(diff> (90))
{
 // console.log("3 months?");
  toShow = "";
  diff = -1;
  show = false;

}
}
}
}
//console.log('IMGSOURCE: ' + item.image);
var x = "";



var date = new Date(item.time);


////date.setUTCMilliseconds(item.time);
//console.log("name: " + item.title + "date: " + date.toUTCString() + "  tostring: " + date.toString() + " time: " + date.toTimeString());
var formatted = date.toLocaleTimeString([], {hour: 'numeric',minute:'numeric', hour12: true});

var month = (date.getMonth()+1).toString()
var monthstring = ""
    if(month==1){
  monthstring = "Jan"
} else if(month==2){
  monthstring = "Feb"
} else if(month==3){
  monthstring = "Mar"
} else if(month==4){
  monthstring = "Apr"
} else if(month==5){
  monthstring = "May"
} else if(month==6){
  monthstring = "Jun"
} else if(month==7){
  monthstring = "Jul"
} else if(month==8){
  monthstring = "Aug"
} else if(month==9){
  monthstring = "Sep"
} else if(month==10){
  monthstring = "Oct"
} else if(month==11){
  monthstring = "Nov"
} else if(month==12){
  monthstring = "Dec"}

  var formattedDate = date.getDate().toString() + " " + monthstring + " " + date.getFullYear().toString();



//alert(formatted);
//console.log('formatted: ' + formatted);


//SHOW
//get item.locations
//compare to this.state.selectItems

/*
console.log('items');

console.log('subs: '+this.state.selectItems);
*/
var items = this.state.subs;

//console.log(items);

var result = filtered.filter(function(n) {
  return items.indexOf(n) > -1;
});



if (!(result.length > 0)){
  show = false;
  //console.log('fail');
}
else{
//  console.log('success');
  //console.log('post locs: '+filtered);
  //console.log('subs: '+items);
  //console.log(result);
}

//test for show==false with dates
//console.log(item.time+" : "+new Date().getTime());
if (item.time > new Date().getTime()){
  show = false;
  //console.log('fail');
}
else{
  //console.log('success');
  //console.log('post locs: '+filtered);
  //console.log('subs: '+items);
  //console.log(result);
}

if (item.aud.indexOf("app") >= 0){
  //console.log('success app aud');
}
else{
  show = false;
  //console.log('fail app aud');
}



//ALERT BUTTONS
if(show){
   return (
//ALERTS DISPLAY CARD

        <ListItem style ={styles.listStyle}

          subtitle={

            <View>
              <View>

        {/*<Image source = {require('./bell-nofill2x.png')} style = {{ marginTop: height*.025, marginLeft: width*.01, position:'absolute'}}/>*/}
        </View>
             {/*} <View style = {{flexDirection: 'row',width:width*0.8, paddingRight:width*0.0008}}>
              <Text style ={{fontFamily: 'Rubik-Regular',textAlign:'right', marginTop: height*.002,marginBottom: height*.002, color:'white', fontSize:11, paddingRight:width*0.008,width:width*0.8 }}>{toShow}</Text>
          </View>*/}
                  <FAIcon name="bell-o" size={20} color='#00AFD1'style={{position:'absolute', marginTop: height*.02,paddingTop:height*.004 }}/>

        <View style = {{marginLeft: width*.01, marginTop:height*.025}}>

              <Text style = {{  paddingBottom: height*.002, height:'auto',textAlign:'left',fontFamily: 'Rubik-Regular', left: width*.075, color:'#002F5F',fontSize:18,lineHeight:24}}>{item.title}</Text>
              <View style = {{flexDirection:'row', marginTop: height*.015,marginBottom: height*.025,width:width*0.8, paddingLeft:width*0.04}}>
              <Text style ={{flexGrow:1,width:width*0.375,textAlign:'left',fontFamily: 'Rubik-Regular',left: width*.04,fontStyle: 'italic',color:'#002F5F', fontSize:14}}>{item.subtitle}</Text>
              <Text style ={{flex:1,fontFamily: 'Rubik-Regular',left: width*.04,color:'#002F5F', fontSize:11, marginTop:4,textAlign:'right' }}>{toShow}</Text>
            </View>
        </View>

            </View>
          }
//justifyContent: 'space-between',

          onPress={() => this.onPress(item.id,item.title,item.subtitle,item.message, filtered.length === 1?filtered.join() + ' Dam' : "Multiple Locations", item.aud, item.image, formatted, formattedDate, filtered,item.facebook, item.twitter, item.tweet)}

        />
      );
} else {
  return null;
}
      }


//OPEN SIDE MENU
      toggleMenu() {
        console.log('toggleMenu');
        this.setState({
            isOpened: ! this.state.isOpened
        });
        console.log('Update forced');
      }
      RegionsClick(){
        //alert(this.state.open + ', ' + !this.state.open1)
        this.setState({open1: !this.state.open1, open: false})
      }

      userClick(){ //dam
        //alert(!this.state.open + ', ' + this.state.open1)
        this.setState({open: !this.state.open, open1: false})
      }

      onShowSettings() {
        OneSignal.getTags((receivedTags) => {
          //console.log(receivedTags);
          var getTags = [];
          for (var tag in receivedTags) {
            getTags.push(tag);
          //Detect Location
            if(tag !== null){
              // selected = tag;
              // if ((email !== null)&&(this.state.emailSub == "yes")) {
              const subsDB = firebase.database().ref('server/saving-data/fireblog/subs');
              subsDB.on("value", function(snapshot) {
              // console.log(snapshot.val());
              }, function (errorObject) {
                console.log("The read failed: " + errorObject.code);
              });
            }
          }
          this.setState({getTags});
        })
      }

      onSelectRegion(res) {
        const _this = this
        this.setState({selectItems1: res});
        /*
        let temp = this.state.selectItems
        res.map(function(item, index){
          _this.prepareSendDamItem(item).map(function(item, index){ //cannot read map of null --ALEX
            temp.push(item)
          })
        })
        this.setState({selectItems: temp})
        */
      }

      render() {

        /*
<View>


            <Menu updateParentState={this.updateState.bind(this)} />
        </View>*/
        //alert(this.props.navigation.state.params.opened);
console.log('clicked');
//console.log(this.props.navigation.gesturesEnabled);

//LIST OF PLACES TO CHOOSE ON FILTER MENU
        const userList = {
            "1":"Beardmore Dam", //changed from ej beardmore (7)
            "2":"Bjelke-Petersen Dam",
            "3":"Boondooma Dam",
            "4":"Burdekin Falls Dam",
            "5":"Callide Dam",
            "6":"Cania Dam",
            "7":"Coolmunda Dam",
            "8":"Eungella Dam",
            "9":"Fairbairn Dam",
            "10":"Fred Haigh Dam",
            "11":"Glenlyon Dam",
            "12":"Julius Dam",
            "13":"Kinchant Dam",
            "14":"Kroombit Dam",
            "15":"Leslie Dam",
            "16":"Paradise Dam",
            "17":"Peter Faust Dam",
            "18":"Teemburra Dam",
            "19":"Tinaroo Falls Dam",
            "20":"Wuruma Dam",
          }
        const { onItemSelected } = this.props
        const drawStyles = {
            drawer: { shadowColor: 'black', shadowOpacity: 1, shadowRadius: 0},
            main: { paddingLeft: 0 }
        }
        const regionList = {
          "1":"Far North Queensland",
          "2":"North Queensland",
          "3":"Mackay, Isaac, Whitsunday",
          "4":"Central Queensland",
          "5":"Wide Bay Burnett",
          "6":"Darling Downs South West",

        }
      const { onRegionSelected } = this.props
      const drawStylesRegion = {
          drawer: { shadowColor: 'black', shadowOpacity: 1, shadowRadius: 0 },
          main: { paddingLeft: 0 }
      }
      //if(this.state.emailAddress==''){this.setState({emailAddress:'no address found'})}

      //alert(this.state.emailAddress);
        return (
//ALERT SETTINGS PANEL
            <Drawer
                isOpen={this.state.isOpened}
                drawerPosition={Drawer.positions.Left}
                //drawerPosition={Drawer.positions.Both}
                rightMenuContent={<PushMenu/>}
                drawWidth={300}
                ref={(ref) => {this._drawer = ref}}
            >
{

  (!(this.state.isOpened))? null:
    (this.state.pulling)?
     //   Spinner
     <View style={{height: height, backgroundColor: 'white'}}>
    <View style={{height:height*.06,marginTop:width*.058}}>
          <TouchableOpacity style={{position:'absolute',zIndex:10,left:width*.794,alignItems: 'flex-end', height: height*.05, width: width*.2}} onPress={() => this.setState({isOpened: false,open:false, open1:false})}>
      <FAIcon name="times" size={30} color='#fff'style={{ alignSelf:'center', paddingTop:height*.004 }}/>
    </TouchableOpacity>
    </View>
     <FlatList
     refreshControl={
       <RefreshControl
         refreshing={this.state.refreshing}
         onRefresh={this._onRefresh.bind(this)}/>
     }
     style={{height:height*0.5,zIndex:10}}
     />
     </View>
    : //((this.state.filters)||(!this.state.filters))?
     //may be subs, may be no subs. fill accordingly.
     (!this.state.status)?
      <View style={{height: height, backgroundColor: 'white'}}>
      <View style={{flexDirection: 'row', justifyContent: 'space-around', marginTop: height*0.01,paddingLeft: width*.025, paddingRight:width*.025, }}>

<Text style = {[styles.navyheading,{backgroundColor: 'transparent', position:'relative',  marginRight: width*.04, paddingTop: height*.01,marginTop: height*.025, paddingLeft:width*.01,}]}>
          Offline Mode
          </Text>

<TouchableOpacity style={{backgroundColor:'transparent', position:'relative',zIndex:10,alignItems: 'flex-end', marginTop: height*.025,height: height*.05, marginLeft: width*.07, width: width*.1, }} onPress={() => this.setState({isOpened: false,open:false, open1:false})}>
      <FAIcon name="times" size={30} color='#004960' style={{ alignSelf:'center', paddingTop:height*.004, zIndex:20,}}/>
    </TouchableOpacity>

</View>
{/*<View style={[styles.borderline,{paddingTop:height*0.002}]}>
    </View>*/}
    <Text style ={{textAlign:'center',width:width,marginTop:height*0.03}}>
Settings unavailable when offline. Please reconnect.
      </Text>
    <TouchableOpacity onPress={() => this.checkConnection()} style = {[styles.button1,{marginTop:height*0.03}]} title="Submit">
        <Text style ={styles.buttonText} >Refresh</Text>
</TouchableOpacity>
      </View>
      :

     <View style={{height: height, backgroundColor: 'white'}}>

 {this.renderNavBar1()}

<View style={{flexDirection: 'row', justifyContent: 'space-around', marginTop: height*0.01,paddingLeft: width*.025, paddingRight:width*.025, }}>

<Text style = {[styles.navyheading,{backgroundColor: 'transparent', position:'relative',  marginRight: width*.25, paddingTop: height*.01,marginTop: height*.025}]}>
          Select your alert locations
          </Text>


</View>
<View style={{paddingTop:height*0.002}}>
    </View>


    <View style={{flexDirection: 'row', justifyContent: 'space-around', paddingLeft: width*.025, paddingRight:width*.025}}>

      <TouchableOpacity style = {[styles.buttonModal, {backgroundColor: this.state.open == true ? '#00AFD1' : '#FFF'}]} onPress={() => this.userClick()}>
        <Text allowFontScaling= {false} style = {{fontFamily:'Rubik-Regular',position:'relative', fontSize:12,height:'auto', width: 'auto',backgroundColor:'transparent',color: this.state.open == true ? '#fff' : '#5A5A5A', textAlign:'center'}}>Select by dam</Text>
      </TouchableOpacity>

      <TouchableOpacity  style= {[styles.buttonModal, {backgroundColor: this.state.open1 == true ? '#00AFD1' : '#FFF'}]} onPress={() => this.RegionsClick()}>
        <Text allowFontScaling ={false} style = {{ fontFamily:'Rubik-Regular',position:'relative', fontSize:12,height:'auto', width:'auto',backgroundColor: 'transparent',color: this.state.open1  == true? '#FFF' : '#5A5A5A', textAlign:'center'}}>Select by region</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => this.selectAll()} style={[styles.buttonModal,{backgroundColor:"#031E2F"}]}>
      <Text style = {{ fontFamily:'Rubik-Regular',fontSize:12,position:'relative', height:'auto', width:'auto',backgroundColor: 'transparent',color: this.state.open1  == true? '#fff' : '#fff', textAlign:'center'}}>
Select all</Text></TouchableOpacity>
    </View>
    <View>
      <Text style ={{textAlign:'center',width:width}}>
Select from the following 19 dams and 6 regions:
      </Text>



    </View>

      <View style = {{marginTop: height*0.02}}>

        <View showsVerticalScrollIndicator = {true}  style = {{height: this.state.open ? null : 0, overflow: 'hidden', width:Dimensions.get('window').width*.95, paddingLeft:width*.05, bottom:8}}>

          <CustomMultiPicker
            options={userList}
            search={false} // should show search bar?
            multiple={true} //
            placeholder={"Search"}
            placeholderTextColor={'#00AFD1'}
            returnValue={"label"} // label or value
            callback={(res)=>{this.setState({selectItems: res}); }} // callback, array of selected items  //console.log(res);
            rowBackgroundColor={"#fff"}
            rowHeight={30}
            rowRadius={5}
            iconColor={"#004960"}
            iconSize={30}
            selectedIconName={"ios-checkmark-circle-outline"}
            unselectedIconName={"ios-radio-button-off-outline"}
            scrollViewHeight={Dimensions.get('window').height*0.3}
            selected={this.state.selectItems}

            />
        </View>
      <View style = {{height: this.state.open1 ? null : 0,  overflow: 'hidden', width:Dimensions.get('window').width*.95, paddingLeft:width*.05,bottom:8}} /*omg overflow hidden is missing piece!*/>
          <CustomMultiPicker
              options={regionList}
              search={false} // should show search bar?
              multiple={true} //
              placeholder={"Search"}
              placeholderTextColor={'#00AFD1'}
              returnValue={"label"} // label or value
              callback={(res)=>{this.onSelectRegion(res)}} // callback, array of selected items //console.log(res);
              rowBackgroundColor={"#fff"}
              rowHeight={30}
              rowRadius={5}
              iconColor={"#004960"}
              iconSize={30}
              selectedIconName={"ios-checkmark-circle-outline"}
              unselectedIconName={"ios-radio-button-off-outline"}
              scrollViewHeight={Dimensions.get('window').height*0.3}
              selected={this.state.selectItems1}
              />
        </View>


      <View>
        <View>
    <Text style = {[styles.navyheading,{paddingLeft:width*0.055,backgroundColor: 'transparent',  marginTop: height*0.025,marginLeft:width*.01}]}>
          Select your notification type
          </Text>
          </View>
          <View>
        </View>
    <View style={{height: height * 0.045, width:width, flexDirection: 'row', justifyContent: 'space-around'}}>
      <Text style = {[styles.buttonText,{color:'#5A5A5A', fontSize:14, fontFamily:'Rubik-Regular'}]}>Push notifications</Text>
      <Text style = {[styles.buttonText,{color:'#5A5A5A', fontSize:14, fontFamily:'Rubik-Regular'}]}>Email notifications</Text>

    </View>

    <View style={{height: height * 0.04,  width:width, flexDirection: 'row', justifyContent: 'space-around'}}>
    <Switch

          onValueChange={this.togglePushOption.bind(this)}
          value = {this.state.isPushSelected}
          onTintColor = {'#00AFD1'}
          />
      <Switch
          onValueChange={(value) => this.toggleEmailOption(value)}
          value = {this.state.isEmailSelected}
          onTintColor = {'#00AFD1'}
          />
    </View>
    </View>

    <View style ={{position:'relative',marginTop: height*.025, flexDirection: 'row', justifyContent:'center', alignContent:'center'}}>
    <Text style = {{marginTop:height*0.01,lineHeight:14,color:'#5a5a5a',textAlign:'center'}}>Current email: </Text>
    <Text style = {{marginTop:height*0.01,lineHeight:14,color:'#5A5A5A',textAlign:'center'}}>{(this.state.emailAddress!=='')?this.state.emailAddress:' No address found'}</Text>
      </View>
</View>
      <TouchableOpacity onPress={this.Submit.bind(this)} style = {[styles.button1,{marginTop:height*0.03}]} title="Submit">
        <Text style ={styles.buttonText}>Submit</Text>
</TouchableOpacity>

</View>

}



<Container style={[styles.container, this.props.style || {}]}>
               {/*} <Image source = {require('./backgrounddark.png')} style ={{width: Dimensions.get('window').width,flex: 1}}>*/}
                {/*<Image source = {require('./sunset-bg-light.jpg')} style ={{width: Dimensions.get('window').width,flex: 1}}>*/}
                <Image source = {require('../Images/MenuNavigationBackground.png')} style ={{width: Dimensions.get('window').width,flex: 1}}>

                              {this.renderNavBar()}

<View>
<Text style={{textAlign:'center',position:'relative',color:'#ffffff',fontFamily:'Rubik-Regular', fontSize:11,width:width, paddingLeft:width*0.03,marginBottom:height*0.05, zIndex:10, backgroundColor:'transparent',marginLeft:width*.2, marginTop: height*.02}}>LAST UPDATED: {this.state.lastRefreshed}</Text>
</View>

{

  (this.state.pulling)?
      //spinner
      <FlatList
      refreshControl={
        <RefreshControl
          refreshing={true}
          tintColor={"#FFF"}
          onRefresh={this._onRefresh.bind(this)}/>
      }
      style={{position:'relative'}}
      />
:

          ((this.state.subs != null)&& (this.state.data != null) && (this.state.subs.length > 0)&&(this.state.data.length > 0))? //subs
              <View>
                {(!this.state.status)?
                <View style={{flexDirection:'row',marginBottom:height*0.028,paddingRight:width*0.05,paddingLeft:width*0.06,}}>
                  <Text style={{flex:1,paddingTop:height*0.008,backgroundColor:'transparent', color:'white',justifyContent:'center',fontFamily:'Rubik-Regular', fontSize:16}}>
                  Offline Mode</Text>
                  <View style={{ paddingLeft:'40%',width:'50%',justifyContent:'flex-end', height:height*0.05,}}>
                  <TouchableOpacity style ={{flex:1,borderRadius:10,backgroundColor: 'rgba(255,255,255,0.3)',justifyContent:'center',alignContent:'center'}} onPress={() => this.checkConnection()} >
                                  <FAIcon name='refresh' size={15} style={{color: 'white',justifyContent:'center',alignContent:'center',textAlign:'center'}} onPress={() => this.checkConnection()} />
                </TouchableOpacity>
                </View>
                  </View>: null
              }
              <FlatList
                  data={this.state.data}
                  renderItem={this.renderItem.bind(this)}
                  keyExtractor={this.keyExtractor}
                  refreshControl={
                    <RefreshControl
                      refreshing={this.state.refreshing}
                      tintColor={"#FFF"}
                      onRefresh={this._onRefresh.bind(this)}/>
                  }
                  style={{position:'relative',height:height*0.765}}
            /></View>
          : //else no subs
              <View style={{marginTop:height*0.2,}}><Text style={{textAlign:'center',backgroundColor:'transparent', color:'white',fontFamily:'Rubik-Regular', fontSize:16}}>
              No alerts to display.{'\n\n'}Check your alert settings and internet connectivity.
              </Text>{/*alert(this.state.subs)*/}
              {(!this.state.status)?
                  <TouchableOpacity style ={{marginTop:height*0.05,alignSelf:'center', justifyContent:'center',borderRadius:10,width:width*.6, height:height*0.08,backgroundColor: 'rgba(0,0,0,0.3)',}} onPress={() => this.checkConnection()} >
                  <Text style = {{backgroundColor:'transparent',fontSize: 18,textAlign:'center', fontFamily: 'Rubik-Regular',color:"#ffffff"}}>Refresh Internet
                  </Text>
                  </TouchableOpacity> : null
              }

          </View>

}
                            </Image>

          </Container>

                  <Modal
                    animationDuration={200}
                    closeOnTouchOutside={true}
                    open={this.state.showPopUp}

                    modalDidClose={() => {
                      this.setState({showPopUp: false});
                    }}
                    modalStyle={{
                      borderRadius: 25,
                      height:height*.4,
                      backgroundColor: '#FFF',
                      borderColor: '#e5e5e5',borderWidth:1
                   }}
                  >

                      <View style={styles.modalInnerView}>
                        <View>
                          <Text allowFontScaling= {false} style = {styles.modalHeading}>
                          Enter email address
                            </Text>

                        <TextInput

                        keyboardType='email-address'
                        autoCapitalize= 'none'
                        autoCorrect={false}
                          returnKeyType='done'
                          defaultValue={(this.state.emailAddress!=='')?this.state.emailAddress:null}
                          placeholder={'Enter email'}
                          selectionColor='#004960'
                          onChangeText={(text) => this.setState({newMail: text})}
                          underlineColorAndroid='transparent'
                          style={styles.textInput}
                        />
                        </View>
                        <View style= {{ flexDirection: 'row', marginTop:height*.02}}>
                        <View style={{ position:'relative' ,right:width*.03}}>
                        <TouchableOpacity style={styles.buttonModal} onPress={() => this.setState({showPopUp: false, isEmailSelected:false})}>

                        <Text allowFontScaling= {false} style={[styles.buttonText,{color:'#5f5f5f'}]}>Cancel</Text>
                      </TouchableOpacity>
                      </View>
                      <View style={{ position:'relative', left: width*.02}}>
                      <TouchableOpacity style={styles.buttonModal} onPress={() => (this.state.newMail==''&&this.state.emailAddress!=='')?this.setState({newMail:this.state.emailAddress})&&this.validateEmail():this.validateEmail()}>
                        <Text allowFontScaling= {false} style={[styles.buttonText,{color:'#5f5f5f'}]}>Confirm</Text>
                      </TouchableOpacity>
                      </View>
                      </View>
                    </View>
                </Modal>
            </Drawer>
        );
    }

    validateEmail(){
      let re = RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
      if (re.test(this.state.newMail)){
        //alert('confirm');
        this.onConfirmEmail();
      }
      else {
        alert('Invalid email address');
      }
    }

/*
    handleConnectionChange = (isConnected) => {
      this.setState({ status: isConnected });
      //alert(this.state.status);
      console.log(`is connected: ${this.state.status}`);
      //alert(this.state.status);
}
*/
    onConfirmEmail() {

 console.log('checking connection');
 NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
 NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
      NetInfo.isConnected.fetch().done(
          (isConnected) => { this.setState({ status: isConnected }); /*alert(this.state.status)*/

      if (this.state.status){
          this.setState({emailAddress: this.state.newMail, showPopUp: false}, function SubmitonConfirm(){
          console.log('Email address:', this.state.emailAddress);
          console.log('EmailSub1: ', this.state.isEmailSelected);
          firebase.database().ref(`server/saving-data/fireblog/subs/${this.state.deviceID}/email`).set(this.state.emailAddress);
          firebase.database().ref(`server/saving-data/fireblog/subs/${this.state.deviceID}/emailSub`).set(this.state.isEmailSelected);
          Alert.alert('Your email setting has been saved');
          }.bind(this));
      }
      else{ //if not connected to internet
          alert('No internet connection - email settings could not be saved.');
          this.setState({isEmailSelected:false});
      }

  });

     }

    refresh(){

      OneSignal.getTags((receivedTags) => {
        // console.log(receivedTags);
        var getTags = [];
        for (var tag in receivedTags) {
          getTags.push(tag);
        }
        console.log(getTags);
        this.setState({getTags});
      })
      // alert("refreshed");
    }


toggleEmailOption(value){

  //NetInfo.isConnected.addEventListener('changeConnection', this.handleConnectionChange);
  console.log('checking connection');
  NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
  NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
              NetInfo.isConnected.fetch().done(
                  (isConnected) => { this.setState({ status: isConnected }); //alert(this.state.status)

  if (this.state.status){
      if(this.state.isEmailSelected){
          this.setState({isEmailSelected: false}, () => {
              firebase.database().ref(`server/saving-data/fireblog/subs/${this.state.deviceID}/emailSub`).set(this.state.isEmailSelected);
              Alert.alert('Email Notifications cancelled successfully');
          });
      } else {
          this.setState({isEmailSelected: true}, () => {
              this.setState({showPopUp:true});
              console.log('EmailSub2: ', this.state.isEmailSelected);
          });
      }
  }
  else{
      alert('No internet connection - email settings could not be saved.');
  }

  });

}

togglePushOption(){

console.log('checking connection');
  NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
  NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
    NetInfo.isConnected.fetch().done(
        (isConnected) => { this.setState({ status: isConnected }); //alert(this.state.status)


if (this.state.status){
    if(this.state.isPushSelected){
        this.setState({isPushSelected: false}, () => {
        this.state.pushSub = 'no';
        OneSignal.setSubscription(false);
        firebase.database().ref(`server/saving-data/fireblog/subs/${this.state.deviceID}/push`).set(this.state.isPushSelected);
        Alert.alert('Push Notifications cancelled successfully');
        });
    } else {
        this.setState({isPushSelected: true}, () => {
        this.state.pushSub = 'yes';
        OneSignal.setSubscription(true);
        firebase.database().ref(`server/saving-data/fireblog/subs/${this.state.deviceID}/push`).set(this.state.isPushSelected);
        Alert.alert('Push Notifications activated successfully');
        });
    }
}
else{//if no internet connection
    alert('No internet connection - push settings could not be saved.');
}

});

}

    delete(){
      OneSignal.deleteTag("Beardmore"); //changed from ejbeardmore
      OneSignal.deleteTag("Bjelke-Petersen");
      OneSignal.deleteTag("Boondooma");
      OneSignal.deleteTag("Burdekin Falls");
      OneSignal.deleteTag("Callide");
      OneSignal.deleteTag("Cania");
      OneSignal.deleteTag("Coolmunda");
      OneSignal.deleteTag("Eungella");
      OneSignal.deleteTag("Fairbairn");
      OneSignal.deleteTag("Fred Haigh");
      OneSignal.deleteTag("Glenlyon");
      OneSignal.deleteTag("Julius");
      OneSignal.deleteTag("Kinchant");
      OneSignal.deleteTag("Kroombit");
      OneSignal.deleteTag("Leslie");
      OneSignal.deleteTag("Paradise");
      OneSignal.deleteTag("Peter Faust");
      OneSignal.deleteTag("Teemburra");
      OneSignal.deleteTag("Tinaroo Falls");
      OneSignal.deleteTag("Wuruma");

      OneSignal.getTags((receivedTags) => {
        console.log(receivedTags);
        var getTags = [];
        for (var tag in receivedTags) {
          getTags.push(tag);
        }
        this.setState({getTags});
      })
      alert("deleted");
    }

    onClear() {
      this.state.subs.forEach((item)=> {
        OneSignal.deleteTag(item);
      });
      this.clearSelected.call(this);
    }

    clearSelected() {
      this.setState({
        selectItems: [],
        selectItems1: [],
        isViewSelect: false,
        isRegionSelect: false,
      }, ()=> {
        this.setState({
          isViewSelect: true,
          isRegionSelect: true,
        });
      });
    }

    prepareSendItem(item) {
      switch (item) {

        case "Bjelke-Petersen Dam":
         // console.log("Bjelke-Petersen was clicked");
          return {
            key: "Bjelke-Petersen",
            value: "Bjelke-Petersen"
          };
        case "Boondooma Dam":
         // console.log("Boondooma was clicked");
          return {
            key: "Boondooma",
            value: "Boondooma",
          };
        case "Burdekin Falls Dam":
          //console.log("Burdekin Falls was clicked");
          return {
            key: "Burdekin Falls",
            value: "Burdekin Falls"
          };
        case "Callide Dam":
          //console.log("Callide was clicked");
          return {
            key: "Callide",
            value: "Callide"
          };
        case "Cania Dam":
          //console.log("Cania was clicked");
          return {
            key: "Cania",
            value: "Cania"
          };
        case "Coolmunda Dam":
         // console.log("Coolmunda was clicked");
          return {
            key: "Coolmunda",
            value: "Coolmunda"
          };
        case "Beardmore Dam": //changed from ej beadmore dam
          //console.log("Beardmore was clicked");
          return {
            key: "Beardmore",
            value: "Beardmore"
          };
        case "Eungella Dam":
          //console.log("Eungella was clicked");
          return {
            key: "Eungella",
            value: "Eungella"
          };
        case "Fairbairn Dam":
          //console.log("Fairbairn was clicked");
          return {
            key: "Fairbairn",
            value: "Fairbairn"
          };
          case "Glenlyon Dam":
          //console.log("Glenlyon was clicked");
          return {
            key: "Glenlyon",
            value: "Glenlyon"
          };
        case "Fred Haigh Dam":
          //console.log("FredHaigh was clicked");
          return {
            key: "Fred Haigh", //changed from fredhaigh
            value: "Fred Haigh"
          };
        case "Julius Dam":
         // console.log("Julius was clicked");
          return {
            key: "Julius",
            value: "Julius"
          };
        case "Kinchant Dam":
          //console.log("Kinchant was clicked");
          return {
            key: "Kinchant",
            value: "Kinchant"
          };
        case "Kroombit Dam":
          //console.log("Kroombit was clicked");
          return {
            key: "Kroombit",
            value: "Kroombit"
          };
        case "Leslie Dam":
          //console.log("Leslie was clicked");
          return {
            key: "Leslie",
            value: "Leslie"
          };
        case "Paradise Dam":
          //console.log("Paradise was clicked");
          return {
            key: "Paradise",
            value: "Paradise"
          };
        case "Peter Faust Dam":
         // console.log("PeterFaust was clicked");
          return {
            key: "Peter Faust",
            value: "Peter Faust"
          };
        case "Teemburra Dam":
          //console.log("Teemburra was clicked");
          return {
            key: "Teemburra",
            value: "Teemburra"
          };
        case "Tinaroo Falls Dam":
          //console.log("Tinaroo Falls was clicked");
          return {
            key: "Tinaroo Falls",
            value: "Tinaroo Falls"
          };
        case "Wuruma Dam":
          //console.log("Wuruma was clicked");
          return {
            key: "Wuruma",
            value: "Wuruma"
          };
          default:
          return null;
      }
      }


         prepareSendDamItem(region) {
      switch (region) {
        case "Far North Queensland":
          //console.log("FNQ was clicked");
          return ["Tinaroo Falls Dam"]
          break
        case "North Queensland":
         // console.log("NQ was clicked");
          return ["Julius Dam", "Burdekin Falls Dam"];
          break
        case "Mackay, Isaac, Whitsunday":
          //console.log("Whitsunday was clicked");
          return ["Peter Faust Dam", "Eungella Dam", "Kinchant Dam", "Teemburra Dam"];
          break
        case "Central Queensland":
         // console.log("CQ was clicked");
          return ["Fairbairn Dam", "Callide Dam", "Kroombit Dam", "Cania Dam"];
          break
        case "Wide Bay Burnett":
          //console.log("WB Burnett was clicked");
          return  ["Wuruma Dam", "Fred Haigh Dam", "Paradise Dam"];
          break
        case "Darling Downs South West":
          //console.log("DD SW was clicked");
          return ["Boondooma Dam", "Bjelke-Petersen Dam", "Leslie Dam", "Coolmunda Dam", "Beardmore Dam","Glenlyon Dam"];
          break
        default:
          return null;
      }
    }



    Submit() {
      const {subs} = this.state
      const {selectItems} = this.state
      const {subbedregions} = this.state

console.log('checking connection');
  NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
  NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
            NetInfo.isConnected.fetch().done(
                (isConnected) => { this.setState({ status: isConnected });

 //if connected
if(this.state.status){

      //this.state.selectItems.forEach((item)=> { //ALEX 8.3.19
      this.state.subs.forEach((item)=> {
      OneSignal.deleteTag(item);

      });
      //console.log('locas array: ' + locs);

      let dams = {};
      let selectedregions = [];

//compare subbedregions to selectItems1 for deleted regions
var deletedregions = [];
//for each in selectItems1
for(var i =0; i < this.state.subbedregions.length;i++){
      //if element doesnt exist in subbedregions (so is new), then:
      if (this.state.selectItems1.indexOf(this.state.subbedregions[i]) < 0){
        //push
        deletedregions.push(this.state.subbedregions[i]);
        console.log('deleted regions');
        console.log(deletedregions);
        //console.log(this.state.selectItems1[i]);
      }
}

//ADDING REGIONS//////////////
  this.state.selectItems1.forEach((item)=>{
    console.log('region item'+item);
    switch (item) {
      case "Far North Queensland":{
          dams = ['Tinaroo Falls Dam'];
          selectedregions["Far North Queensland"] = [];//changed from ej beardmore
          dams.map(function(item, index){
            selectedregions["Far North Queensland"].push(item)
            if (subbedregions.indexOf("Far North Queensland") < 0){ //CHECK IF NEW REGION
              selectItems.push(item);
            }
          })
          if(selectedregions["Far North Queensland"].length == 0) selectedregions["Far North Queensland"] = dams
          break;
          }
          case "North Queensland":{
           //console.log("NQ was clicked");
           dams = ["Julius Dam", "Burdekin Falls Dam"];
           selectedregions["North Queensland"] = [];//changed from ej beardmore
           dams.map(function(item, index){
            selectedregions["North Queensland"].push(item)
            if (subbedregions.indexOf("North Queensland") < 0){
              selectItems.push(item);
            }
           })
           if(selectedregions["North Queensland"].length == 0) selectedregions["North Queensland"] = dams
           break;
          }
          case "Mackay, Isaac, Whitsunday":{
          //console.log("Mackay was clicked");
            dams = ["Peter Faust Dam", "Eungella Dam", "Kinchant Dam", "Teemburra Dam"];
            selectedregions["Mackay, Isaac, Whitsunday"] = [];//changed from ej beardmore
            dams.map(function(item, index){
             selectedregions["Mackay, Isaac, Whitsunday"].push(item)
             if (subbedregions.indexOf("Mackay, Isaac, Whitsunday") < 0){
              selectItems.push(item);
            }
            })
            if(selectedregions["Mackay, Isaac, Whitsunday"].length == 0) selectedregions["Mackay, Isaac, Whitsunday"] = dams
            break;
          }
          case "Central Queensland":{
            //console.log("CQ was clicked");
            dams = ["Fairbairn Dam", "Callide Dam", "Kroombit Dam", "Cania Dam"];
            selectedregions["Central Queensland"] = [];//changed from ej beardmore
            dams.map(function(item, index){
             selectedregions["Central Queensland"].push(item)
             if (subbedregions.indexOf("Central Queensland") < 0){
              selectItems.push(item);
            }
            })
            if(selectedregions["Central Queensland"].length == 0) selectedregions["Central Queensland"] = dams
            break;
          }
        case "Wide Bay Burnett":{
          //console.log("WB Burnett was clicked");
          dams = ["Wuruma Dam", "Fred Haigh Dam", "Paradise Dam"];
          selectedregions["Wide Bay Burnett"] = [];//changed from ej beardmore
          dams.map(function(item, index){
           selectedregions["Wide Bay Burnett"].push(item)
           if (subbedregions.indexOf("Wide Bay Burnett") < 0){
            selectItems.push(item);
          }
          })
          if(selectedregions["Wide Bay Burnett"].length == 0) selectedregions["Wide Bay Burnett"] = dams
          break;
        }
        case "Darling Downs South West":{
          //console.log("DD SW was clicked");
          dams = ["Boondooma Dam", "Bjelke-Petersen Dam", "Leslie Dam", "Coolmunda Dam", "Beardmore Dam","Glenlyon Dam"];
          selectedregions["Darling Downs South West"] = [];//changed from ej beardmore
          dams.map(function(item, index){
           selectedregions["Darling Downs South West"].push(item)
           if (subbedregions.indexOf("Darling Downs South West") < 0){
            selectItems.push(item);
          }
          })
          if(selectedregions["Darling Downs South West"].length == 0) selectedregions["Darling Downs South West"] = dams
          break;
        }

      }
      });

//DELETING REGIONS//////////////
  deletedregions.forEach((item)=>{ //compare existing regions to new regions, then do switch
    console.log('item: '+item);
          switch (item) {
            case "Far North Queensland":{
              dams = ['Tinaroo Falls Dam'];
              dams.map(function(item, index){
                console.log('delete: '+item);
              if (selectItems.indexOf(item)> -1){ //if dam to be deleted is in selected items
                  selectItems.splice(selectItems.indexOf(item), 1); //if item to be removed is at an index, remove 1 item from that index (the item to be removed)
              }
            })
            break;
              //this.state.selectItems.push(dams); //pushing new region dams to selected dams
            }
            case "North Queensland":{
              dams = ["Julius Dam", "Burdekin Falls Dam"];
              dams.map(function(item, index){
                console.log('delete: '+item);
                if (selectItems.indexOf(item)> -1){ //if dam to be deleted is in selected items
                  selectItems.splice(selectItems.indexOf(item), 1); //if item to be removed is at an index, remove 1 item from that index (the item to be removed)
              }
              })
              break;
              //this.state.selectItems.push(dams);
              }
              case "Mackay, Isaac, Whitsunday":{
                dams = ["Peter Faust Dam", "Eungella Dam", "Kinchant Dam", "Teemburra Dam"];
                dams.map(function(item, index){
                  console.log('delete: '+item);
                  if (selectItems.indexOf(item)> -1){ //if dam to be deleted is in selected items
                    selectItems.splice(selectItems.indexOf(item), 1); //if item to be removed is at an index, remove 1 item from that index (the item to be removed)
                }
              })//this.state.selectItems.push(dams);
              break;
              }
              case "Central Queensland":{
                dams = ["Fairbairn Dam", "Callide Dam", "Kroombit Dam", "Cania Dam"];
                dams.map(function(item, index){
                  console.log('delete: '+item);
                  if (selectItems.indexOf(item)> -1){ //if dam to be deleted is in selected items
                    selectItems.splice(selectItems.indexOf(item), 1); //if item to be removed is at an index, remove 1 item from that index (the item to be removed)
                }
              })//this.state.selectItems.push(dams);
              break;
              }
            case "Wide Bay Burnett":{
              dams = ["Wuruma Dam", "Fred Haigh Dam", "Paradise Dam"];
              dams.map(function(item, index){
                console.log('delete: '+item);
                if (selectItems.indexOf(item)> -1){ //if dam to be deleted is in selected items
                  selectItems.splice(selectItems.indexOf(item), 1); //if item to be removed is at an index, remove 1 item from that index (the item to be removed)
              }
            })//this.state.selectItems.push(dams);
            break;
            }
            case "Darling Downs South West":{
              dams = ["Boondooma Dam", "Bjelke-Petersen Dam", "Leslie Dam", "Coolmunda Dam", "Beardmore Dam","Glenlyon Dam"];
              dams.map(function(item, index){
                console.log('delete: '+item);
                if (selectItems.indexOf(item)> -1){ //if dam to be deleted is in selected items
                  selectItems.splice(selectItems.indexOf(item), 1); //if item to be removed is at an index, remove 1 item from that index (the item to be removed)
              }
            })//this.state.selectItems.push(dams);
            break;
            }
          }
    });


//UPDATE DAMS
//var newselectitems = this.state.selectItems;
var newsubs = [];
      var locs = {};
      this.state.selectItems.forEach((item)=> {
        let sendItem = this.prepareSendItem.call(this, item);
        if (sendItem != null) {
          //console.log('locs SendItem: ' + sendItem);
          let {key, value} = sendItem;
          locs[key] = item;
          //console.log('key'+key);
          newsubs.push(key); //UPDATE SUBSCRIPTIONS
        }
      });
      this.setState({ subs: newsubs});

//UPDATE REGIONS
//this.state.selectItems1.push(newregions); //no need, newregions are pulled from selecitems1 - already exist in list
this.setState({ subbedregions: this.state.selectItems1});

//update email
const email = this.state.emailAddress || null;
var updates = {};
console.log('Email to be submitted: ' + email);

//CHECK DATA IS SELECTED
var size = false;
for(var key in locs){
  size =true;
  break;
}
//console.log(locs);
console.log('locs: ' + size);

//IF NO DATA
if(!size){
  firebase.database().ref(`server/saving-data/fireblog/subs/${this.state.deviceID}`).set({
    email: email,
    locs: locs,
    regions: selectedregions,
    emailSub: this.state.isEmailSelected,
    push: this.state.isPushSelected
  });
  this.setState({filters:false});
  alert('No Alert Filters Selected');
  //update subs? from alerts //ALEX TODO
 // close = false;
  this.setState({open1:false});
}
//IF DATA
else if (size){ //email!==null || size
        firebase.database().ref(`server/saving-data/fireblog/subs/${this.state.deviceID}`).set({
          email: email,
          locs: locs,
          regions: selectedregions,
          emailSub: this.state.isEmailSelected,
          push: this.state.isPushSelected
        });
        //this.setState({filters:true});


          console.log('subs'+this.state.subs);
          this.setState({filters:true});
          this.setState({open1:false});


          AsyncStorage.setItem('fetchSubs', JSON.stringify(this.state.subs), (err, result) => { //LOCAL STORAGE
            //alert('locally stored');
            console.log('newly set subs have been saved locally');

          })


        Alert.alert("Your alert settings have been updated")

      }
    //  console.log(this.state.subbedregions);
     // console.log(selectedregions);

      for(var key in locs){
        OneSignal.sendTag(key, key);
        console.log('key: '+key);
      }

      //OneSignal.sendTags(selectedregions);
      var { getTags } = this.state;
      this.setState({ getTags: locs });

      console.log(this.state.selectItems);
      console.log(this.state.selectItems1);
      console.log(deletedregions);
      console.log(selectedregions);

    }
    else{
      Alert.alert("No Internet Connectivity","Changes could not be saved")
      this.setState({open1:false});
    }
      this.setState({isOpened: false});

});

    }

    onPressShowAll(){
        console.log("ShowAll has Clicked");
        //this.setState({filterFlag : false});
    }

    selectAll(){
      console.log('all');
      Alert.alert(
          'Alert',
          'Would you like to subscribe to all alerts?',
          [
                {text: 'OK', onPress: () => this.acceptAll()},
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'),style: 'cancel'}              ]
        )
  }

  acceptAll(){
      this.setState(
          {
              selectItems: ["Beardmore Dam","Bjelke-Petersen Dam","Boondooma Dam","Burdekin Falls Dam","Callide Dam","Cania Dam","Coolmunda Dam","Eungella Dam","Fairbairn Dam","Fred Haigh Dam","Glenlyon Dam","Julius Dam","Kinchant Dam","Kroombit Dam","Leslie Dam","Paradise Dam","Peter Faust Dam","Teemburra Dam","Tinaroo Falls Dam","Wuruma Dam"],
              selectItems1: ["Far North Queensland","North Queensland","Mackay, Isaac, Whitsunday", "Central Queensland","Wide Bay Burnett","Darling Downs South West"],
          }
      );
      this.Submit();
  }

    onPressShowOperational(){
      if (this.state.filterFlag && this.state.filterKey === 'op') {
        this.setState({ filterFlag: false });
      }
      else {
        this.setState({ filterFlag: true, filterKey: 'op' });
        var filterdata = [];
//        console.log(this.state.data);
        this.state.data.map((key) => {
            if (key.aud === "Operational"){
                filterdata.push(key);
            }
        })
//        filterdata = filterdata.reverse();
        this.setState({ filterdata }, () => {
          console.log(this.state.filterdata);
        });
        console.log("Operational has Clicked");
      }
    }
    onPressShowCommunity(){
      if (this.state.filterFlag && this.state.filterKey === 'co') {
        this.setState({ filterFlag: false });
      }
      else {
        this.setState({ filterFlag: true, filterKey: 'co' });
        var filterdata = [];
        this.state.data.map((key) => {
            if (key.aud === "Community"){
                filterdata.push(key);
            }
        })
        //filterdata = filterdata.reverse();
        this.setState({filterdata});
        console.log("Community has Clicked");
      }
    }



    renderNavBar1() {
      const { navigation } = this.props
      return (
        <View style={styles.navBar}>
        {/*BARS*/}
        <Image source = {require('./HeaderBar.png')} style = {{marginTop: height*.075,marginLeft: width*.12}}/>

          <TouchableOpacity style={{position:'absolute',zIndex:10,left:width*.794,alignItems: 'flex-end', height: height*.05, width: width*.2}} onPress={() => this.setState({isOpened: false,open:false, open1:false})}>
      <FAIcon name="times" size={30} color='#fff'style={{ alignSelf:'center', paddingTop:height*.004 }}/>
    </TouchableOpacity>



<Image source={require('./SunWater_Logo_New.png')} style = {{position:'absolute', marginLeft:width*.35,height:height*.4, width:width*.325, resizeMode:'contain'}}/>

                {/*<Text style ={{color:'#004960', fontFamily:'Rubik-Regular', fontSize:24, paddingTop:height*0.035}}>Alerts</Text>*/}



          </View>

      )
  }

    renderNavBar() {
        const { navigation } = this.props
        return (
          <View style={styles.navBar}>
          {/*BARS*/}
          <Image source = {require('./HeaderBar.png')} style = {{marginTop: height*.075,marginLeft: width*.12}}/>

          <TouchableOpacity
                    style={{ flexDirection: 'row', alignItems: 'center',marginTop:height*.05,position:'absolute', height: height*.1, width: width*.3}}
                    //onPress={() => { this.props.navigation.navigate('HubScreen');}} >
                    onPress={() => { this.props.navigation.goBack();}} >
                    <FAIcon name='home' size={25} style={{color: '#fff',marginLeft:width*.05,marginTop: height*.02, paddingBottom:height*.009}} />
                </TouchableOpacity>



<Image source={require('./SunWater_Logo_New.png')} style = {{position:'absolute', marginLeft:width*.35,height:height*.4, width:width*.325, resizeMode:'contain'}}/>

                  {/*<Text style ={{color:'#004960', fontFamily:'Rubik-Regular', fontSize:24, paddingTop:height*0.035}}>Alerts</Text>*/}


 <TouchableOpacity
                    style={{ flexDirection: 'row', alignItems: 'center',marginTop:height*.05,position:'absolute', height: height*.1, width: width*.05}}
                    onPress={() => {
                      //if(this.state.status){
                      const _this = this
                      this.onShowSettings()
                      this.setState({isOpened: true, open: true})
                   }} >
                    <FAIcon name='cog' size={27} style={{ justifyContent:'center',paddingBottom:height*.05,paddingLeft:width*.08,marginLeft:width*.8,marginTop:height*.0575,color: '#ffffff' }} />
                </TouchableOpacity>

            </View>

        )
    }



}

const styles = StyleSheet.create({
    buttonSubmit: {
      marginBottom: 10,
      backgroundColor: '#2196F3',
    },

    testButton:{
        backgroundColor:'white',
        flex:1,
        top: 70,
        height: 'auto',
        width: 400,
        position: 'absolute',
        alignItems:'center'
    },
    testButton2:{
        backgroundColor:'white',
        flex:1,
        top: 190,
        height: 'auto',
        width: 400,
        position: 'absolute',
        alignItems:'center'
    },
    testButton3:{
        backgroundColor:'white',
        flex:1,
        top: 310,
        height: 'auto',
        width: 400,
        position: 'absolute',
        alignItems:'center',

    },
    regionbutton: {
    //  flex: 1,
      justifyContent: 'center',

      alignItems: 'center',
      borderRadius: 15,
      height: height*.078,
      width:width*.4,
      marginTop:height*.02,
      marginBottom: height*.02,
   //   paddingTop:height*.005,
    //  paddingBottom: height*.005,
    },
  dambutton: {
marginBottom: height*.02,
height: height*.078,
width:width*.4,

 // backgroundColor:'#00AFD1',
 //flex: 1,
 justifyContent: 'center',
 alignItems: 'center',
  borderRadius: 15,
  marginTop:height*.03,
  //paddingTop:height*.005,
 // paddingBottom: height*.005,
},


listStyle:{

    width: Dimensions.get('window').width*.9,
//    borderTopColor:'#000',
    //borderBottomColor: '#71D7EB',
    //borderBottomColor: '#3f98ac',
    paddingRight:width*.04,
    //paddingLeft:width*.05,
    //borderTopWidth: 0.5,
//    borderBottomWidth: 1,
    marginLeft: width*.05,
    marginBottom: height*.009,
    //marginTop:2,
    //minHeight: 90,
    backgroundColor:'rgba(255,255,255,.85)',
    //borderRadius:5,
//    borderColor:'black',
    height: 'auto',
   // borderWidth: 1,
    //textAlign: 'center'
},

    container: {
        shadowColor: '#000000',
        shadowOpacity: 0.4,
        shadowOffset: { height: -5, width: -5 },
        shadowRadius: 10,
        backgroundColor: 'transparent'
    },
    containerbg: {
        flex: 1,
        position: 'relative',
        resizeMode: 'cover',
        backgroundColor: 'transparent'
    },
    navBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: height*.1,
        alignItems: 'center',
        position: 'relative',
      //  paddingHorizontal: 25,
        //backgroundColor: '#4086c0'
        backgroundColor:'transparent',
       // marginBottom: height*.04,

     paddingBottom: height*.08,


    },

    button1:{
      backgroundColor:'#00AFD1',
     //backgroundColor:('#003359'),
     shadowOffset:{  width: 0,  height: 2},
     shadowColor: 'rgba(0,0,0,0.16)',
     shadowOpacity: 1.0,
     shadowRadius:6,
     elevation: 5,
      //fontSize: 100,
      width: Dimensions.get('window').width*.8,
      height: Dimensions.get('window').width/6,
      alignSelf:'center',
      justifyContent: 'center',
     // borderRadius: 7,
      position:'relative'

      //flex: 1,
      //resizeMode: Image.resizeMode.contain,
  },
  buttonText:{

      color: 'white',
      fontFamily: 'Rubik-Medium',
      fontSize: 16,
      textAlign:'center',


  },

    buttonModal:{
      backgroundColor:'white',
     //backgroundColor:('#003359'),
     shadowOffset:{  width: 0,  height: 2},
     shadowColor: 'rgba(0,0,0,0.16)',
     shadowOpacity: 1.0,
     shadowRadius:6,
      //fontSize: 100,
      width: Dimensions.get('window').width*.28,
      height: Dimensions.get('window').height*.065,
     // alignSelf:'center',
      justifyContent: 'center',
     // borderRadius: 7,
      position:'relative',
      marginTop: height*0.02,
      marginBottom: height*0.02,

      //paddingRight: width*.0175

      //flex: 1,
      //resizeMode: Image.resizeMode.contain,
  },

    siteName: {
        marginTop: 30,
        width: 250
    },
    logo: {
        color: 'white',
        marginTop: 10
    },
    stretch: {
        flex: 2,
        height: undefined,
        width: undefined,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundColor: 'transparent'
    },
    baseText: {
        fontFamily: 'Snell Roundhand',
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 30,
        color: 'white',
        position: 'relative',
    },
    topButtons: {
      // display: 'flex',
      // flex: 1,
      width: width,
      height: 'auto',
      flexDirection: 'row',
      position: 'relative',

    },
    topbutton1: {
      // flex:1,
      //  width: 180,
      width: width / 2,
      height: 'auto',
      position: 'relative',
      borderColor:'#fff',
      borderWidth:1,

   },

    topButtonImg: {
      width: width / 2,
      //height: 90,
      position: 'relative',
      height: width *0.45/2,
    },

   filterButtons:{
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: Dimensions.get('window').height*.08,
    alignItems:'center',
    justifyContent:'center',
   // borderColor:'black',
   //borderWidth:1,
    backgroundColor: '#00AFD1'
   },

   topbutton2: {
    // flex:1,
    //  width: 180,
    width: width / 2,
     height:'auto',
     borderColor:'#fff',
     borderWidth:1,

    //  left:180,
    //  bottom: 90,
    //  position: 'relative',
 },

 topbutton3: {
  // flex:1,
   width: 60,
   height:60,
   top:10,
   left:100,
   position: 'absolute',
},

modalContainer: {

 backgroundColor:'black'

},

textInput: {
  fontSize: 18,
  fontFamily:'Rubik-Regular',
  borderColor:'#5f5f5f',
  borderWidth:1,
  borderRadius: 15,
  width: width*.7,
  height: height*.07,
  alignSelf:'center',
  color:'#5f5f5f',
 // marginBottom:height*.05

},

modalInnerView: {
    alignItems: 'center',
    backgroundColor:'white',

},

emailConfirmButton: {
  backgroundColor: '#00AFD1',
  height: height*.08,
  width: width*.3,
  borderRadius: 15,
  justifyContent: 'center',
  alignItems: 'center',
  padding: 10,
  marginTop: 20,

},

emailConfirmText:{
  color: 'white',
  fontSize: 16,
  fontFamily:'Rubik-Regular',
  backgroundColor:'transparent'


},

borderline:{
  marginLeft: width*.05,
   marginRight: width*.05,
   borderBottomColor:'#00AFD1',
   borderBottomWidth:1,
  backgroundColor: 'transparent',
  },


modalHeading:{

  color: '#031E2F',
  textAlign:'left',
  fontSize: 16,
  fontFamily: 'Rubik-Regular',
  marginBottom:height*.05,
  marginLeft:width*.1,
  marginRight:width*.1,
  marginTop:height*.05,
  left: width*.02
},
navyheading:{

  color: '#031E2F',
  textAlign:'left',
  fontSize: 18,
  fontFamily: 'Rubik-Medium',
  marginBottom:height*.01, //alex
},
navybody:{

  color: '#004960',
  //textAlign:'left',
  fontSize: 14,
  fontFamily: 'Rubik-Regular',
 // marginBottom:height*.15,

}

})
