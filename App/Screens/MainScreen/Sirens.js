
'use strict'
import React, {Component} from 'react'

import { StyleSheet, Linking, View, Image,TouchableOpacity, TextInput, Dimensions, Text, NativeModules } from 'react-native'
import{FormInput, Row} from 'react-native-elements'
import FAIcon from 'react-native-vector-icons/FontAwesome'
import Container from '../../Component/Container'
import AlertsScreen from '../AlertsScreen'
//import { FormInput } from '../../../../../Library/Caches/typescript/2.6/node_modules/@types/react-native-elements';
const width= Dimensions.get('window').width
const height= Dimensions.get('window').height
export default class Sirens extends Component {
    constructor(props) {
        super();
        this.state = {
            visible: false
          };
        }

/* <TouchableOpacity style = {styles.button1} onPress={() => {NativeModules.arbridge.start3dAR()} }>*/


    render() {
        return (
<Container style={[styles.container]}>

                              {this.renderNavBar()}

<View style = {{backgroundColor:'white'}}>
<View style = {{marginLeft: width*.08, marginRight: width*.08}}>
<Text style = {styles.navyheading}>Sirens</Text>
</View>

<View style = {{marginTop: height*.1}}>
<Text  style={[styles.navybody,{paddingLeft: width*.05,paddingRight: width*.05}]}>
A dam emergency is when there is an unexpected situation developing at a dam. At worst case, this could eventuate to a dam failure.{'\n'}{'\n'}
Dam failures are extremely rare with only one instance in Australia in the past 90 years. SunWater is committed to community safety and alerting residents about emergency situations such as this.{'\n'}{'\n'}
Six warning sirens are being installed throughout regional Queensland to alert residents living immediately downstream of Coolmunda, Fairbairn, Kinchant, Teemburra, Paradise, Kroombit and Callide Dam{'\n'}{'\n'}
in the highly unlikely event of an emergency that might lead to dam failure.{'\n'}{'\n'}
For more information on the project visit call
</Text>
<Text onPress={()=> Linking.openURL('tel:000')} style = {[styles.navybody,{textDecorationLine :'underline',marginLeft: width*.175}]}>131589.{'\n'}</Text>


</View>

<TouchableOpacity style={[styles.button1,{marginTop: height*.1}]}  onPress={() => Linking.openURL('http://www.sunwater.com.au/development/projects/emergency-warning-system-installation')}>
<Text style = {styles.buttonText}> Find out more</Text>
  </TouchableOpacity>

{/*<View>
 <TouchableOpacity style ={[styles.button1,{marginTop:height*0.1}]} onPress={() =>{this.props.navigation.goBack()}} >
<Text style = {styles.buttonText}>Return to Hub
    </Text>
  </TouchableOpacity>
</View>
*/}

</View>


               </Container>

        )
    }


    renderNavBar() {
        const { navigation } = this.props
        return (
          <View style={styles.navBar}>
          {/*BARS*/}

          <Image source = {require('./HeaderBar.png')} style = {{marginTop: height*.075,marginLeft: width*.12}}/>

          <TouchableOpacity
                    style={{ flexDirection: 'row', alignItems: 'center',marginTop:height*.05,position:'absolute', height: height*.1, width: width*.3}}
                    onPress={() => { this.props.navigation('HubScreen');}} >

                    <FAIcon name='home' size={30} style={{color: '#111D28',marginLeft:width*.05,marginTop: height*.02, paddingBottom:height*.009}} />
                </TouchableOpacity>




    <Text style={styles.navyheading}>
  Sirens</Text>


            </View>

        )
    }
}

const styles = StyleSheet.create({
    greybody:{
        color: '#5A5A5A',
        fontFamily: 'Rubik-Regular',
        fontSize: 14,
        textAlign:'center',
        marginLeft:width*.1,
        marginRight:width*.1,
        marginBottom:height*.15

    },

    container: {
        shadowColor: '#000000',
        shadowOpacity: 0.4,
        shadowOffset: { height: -5, width: -5 },
        shadowRadius: 10,
        backgroundColor: 'white',
        width: '100%',
        height: 'auto'
    },

    ARbutton:{
            height:height*.07,
            width:width*.3,
            borderRadius: 15,
            position:'absolute',
            marginTop:height*.75,
            marginLeft:width*.175,
            backgroundColor:'#00AFD1',
            paddingTop:height*.01,

    },
    ARbutton3d:{
            height:height*.07,
            width:width*.3,
            borderRadius: 15,
            position:'absolute',
            marginTop:height*.75,
            marginLeft:width*.480,
            backgroundColor:'#00AFD1',
            paddingTop:height*.01
    },

    navBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: height*.1,
        alignItems: 'center',
        position: 'relative',
      //  paddingHorizontal: 25,
        //backgroundColor: '#4086c0'
        backgroundColor:'transparent',
       // marginBottom: height*.04,

     paddingBottom: height*.08,


    },
    headerHolder: {
        padding: width*.1,
        flex: 1,
        backgroundColor:'white',
       // justifyContent:'center'
    },
    siteName: {
        marginTop: 30,
        width: 250
    },
    logo: {
        color: 'white',
        marginTop: 10
    },
    stretch: {
        flex:2,
        height: undefined,
        width: undefined,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    baseText: {
        fontFamily: 'Snell Roundhand',
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 30,
        color:'white',
        position: 'absolute',
        left: 100,
        paddingBottom:20,
        },

        borderline:{
            marginLeft: width*.05,
             marginRight: width*.05,
             borderBottomColor:'#00AFD1',
             borderBottomWidth:1},

        navyheading:{
            position:'absolute',
            color: 'white',
            textAlign:'left',
            fontSize: 18,
            fontFamily: 'Rubik-Regular',
            marginBottom:height*.01,
            marginLeft: width*.4,
            marginTop:height*.15,
          },
          navybody:{

            color: '#5a5a5a',
            marginLeft:width*.1,
            //textAlign:'left',
            fontSize: 14,
            fontFamily: 'Rubik-Regular',
           // marginBottom:height*.15,

          },
          button1:{
            backgroundColor:'#00AFD1',
           //backgroundColor:('#003359'),
           shadowOffset:{  width: 0,  height: 2},
           shadowColor: 'rgba(0,0,0,0.16)',
           shadowOpacity: 1.0,
           shadowRadius:6,
           elevation: 5,
            //fontSize: 100,
            width: Dimensions.get('window').width*.8,
            height: Dimensions.get('window').width/6,
            alignSelf:'center',
            justifyContent: 'center',
           // borderRadius: 7,
            position:'relative'

            //flex: 1,
            //resizeMode: Image.resizeMode.contain,
        },
        buttonText:{

            color: 'white',
            fontFamily: 'Rubik-Medium',
            fontSize: 16,
            textAlign:'center',


        },

})

