'use strict'
import React, {Component} from 'react'

import { StyleSheet, View, Dimensions,TouchableOpacity, Share,Image,Text } from 'react-native'

import FAIcon from 'react-native-vector-icons/FontAwesome'
import Container from '../../Component/Container'
const width = Dimensions.get("window").width
const height = Dimensions.get("window").height
export default class ShareAppScreen extends Component {

    render() {
        return (
            <Container sytle = {[styles.container, this.props.style || {}]}>
                { this.renderNavBar() }
                { this.renderHeader() }


            </Container>
        )
    }

    renderNavBar() {
        return (
            <View style = {styles.navBar}>
                <TouchableOpacity onPress={ this.props.onMenuPress} >
                <FAIcon name = 'bars' size={22} style = {{color: '#fff'}} />
                </TouchableOpacity>

            </View>
        )
    }

    share(){

        Share.share({
            subject: 'SunWater App',
            message: '#DamStraight: Download SunWater\'s new Dam app',
            url: 'http://www.sunwater.com.au?subject=abcdef',
            title: '#DamStraight'
          })
    }

    renderHeader() {
        return (

            <View style = {{flex: 1}}>
            {/*<Image source = {require('./frontscreen.png')} style ={styles.background}>*/}
            <View style = {{alignItems:'center', marginTop:height*.2}}>
    <TouchableOpacity style = {{backgroundColor:'white', marginBottom: height*.05, borderRadius:25, minHeight:50, alignContent:'center'}} onPress = {()=>this.share()}>
            <Text style = {{position:'relative'}}>Share via Facebook
            </Text>
    </TouchableOpacity>



      </View>
{/*</Image>*/}
      </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        shadowColor: '#000000',
        shadowOpacity: 0.4,
        shadowOffset: { height: -5, width:-5},
        shadowRadius: 10,
        backgroundColor: 'black',
    },
    navBar: {
        height: 50,
        justifyContent: 'center',
        paddingHorizontal: 25,
        backgroundColor: 'transparent'
    },
    headerHolder: {
        padding: 25,
        flex: 1
    },
    siteName: {
        marginTop: 30,
        width: 250
    },
    logo: {
        color: 'white',
        marginTop: 10
    },
    stretch: {
        flex:2,
        height: undefined,
        width: undefined,
        position:'absolute',left:0,right:0,top:0,bottom:0
    },
    background:{
        flex:1,
        alignItems:'stretch',
        height:Dimensions.get('window').height,
        width: Dimensions.get('window').width,

},
})
