'use strict'
import React, {Component} from 'react'

import{FormInput, Row} from 'react-native-elements'


import {Alert, StyleSheet,Dimensions, ActivityIndicator, Switch, NetInfo, TextInput, View, ListView,AsyncStorage, ScrollView, TouchableOpacity, Image, Text, TouchableHighlight, Button, Linking } from 'react-native'

import FAIcon from 'react-native-vector-icons/FontAwesome'
import Container from '../../Component/Container'
//import PercentageCircle from 'react-native-percentage-circle'
//import ProgressCircle from 'react-native-progress-circle'
import Modal from 'react-native-simple-modal'
import * as firebase from 'firebase';
import {NavigationActions} from 'react-navigation';
var DeviceInfo = require('react-native-device-info');
import AlertsScreen from '../AlertsScreen'
import Alerts from './Alerts'
import Drawer from 'react-native-drawer-menu';
import CustomMultiPicker from "react-native-multiple-select-list";
import Expand from 'react-native-simple-expand';
import OneSignal from 'react-native-onesignal';
import Carousel from 'react-native-snap-carousel';
//import { FormInput } from '../../../../../Library/Caches/typescript/2.6/node_modules/@types/react-native-elements';
const width= Dimensions.get('window').width
const height= Dimensions.get('window').height
export default class ARScreen extends Component {


        constructor(props){
            super(props);
            this.initialState = {
                sitevisited : true,
                gotoNext: false,
                dbConnectionFailed: false,
                status: false, //internet connectivitt
               //SPINNER
               visible: false,
               animating: true,
                valID : '',
                deviceID : '',
                open: true,
                open1:false,
                isOpened:false,
                screen:'one',
                newMail:'',
                emailAddress:'',
                isEmailSelected:false, //email wizard toggle
                showPopUp:false, //show email popup
                isPushSelected:false, //push wizard toggle
                getTags:'',
                selectItems: [],
                selectItems1: [],
                newselectitems: [],
                key : 0,

            }
            this.state = this.initialState;

        }


/* <TouchableOpacity style = {styles.button1} onPress={() => {NativeModules.arbridge.start3dAR()} }>*/


    render() {
        return (
<Container style={[styles.container]}>

                              {this.renderNavBar()}

<View style = {{backgroundColor:'white'}}>

<View style ={{marginTop: height*.035}}>
<View style={styles.emerView}>
<Text style = {styles.emergencyNames}>Emergency       </Text><Text onPress={()=> Linking.openURL('tel:000')} style = {styles.phoneNumbers}>000{'\n'}</Text>
</View>

<View style={styles.emerView}>
<Text style = {styles.emergencyNames}>QLD SES       </Text><Text onPress={()=> Linking.openURL('tel:132500')} style = {styles.phoneNumbers}>13 25 00{'\n'}</Text>
</View>

<View style={styles.emerView}>
<Text style = {styles.emergencyNames}>Policelink      </Text><Text onPress={()=> Linking.openURL('tel:131444')} style = {styles.phoneNumbers}>13 14 44{'\n'}</Text>
</View>

<View style={styles.emerView}>
<Text style = {styles.emergencyNames}>Crime Stoppers   </Text><Text onPress={()=> Linking.openURL('tel:1800333000')} style = {styles.phoneNumbers}>1800 333 000{'\n'}</Text>
</View>

<View style={styles.emerView}>
<Text style = {styles.emergencyNames}>SunWater   </Text><Text onPress={()=> Linking.openURL('tel:131589')} style = {styles.phoneNumbers}>13 15 89{'\n'}</Text>
</View>
</View>

<View>
<TouchableOpacity style={[styles.button1,{marginTop: height*.25}]} onPress={ () => this.props.navigation.navigate('AlertsScreen')}>



<Text style = {styles.buttonText}>Return to alerts
                </Text>
</TouchableOpacity>
</View>


</View>


               </Container>

        )
    }
    alerts(){ //on push/email wizard submit


        const {navigation} = this.props;
   navigation('AlertsScreen');
    }

   renderNavBar() {
        const { navigation } = this.props
        return (
          <View style={styles.navBar}>
          {/*BARS*/}

          <Image source = {require('./HeaderBar.png')} style = {{marginTop: height*.075,marginLeft: width*.12}}/>

          <TouchableOpacity
                    style={{ flexDirection: 'row', alignItems: 'center',marginTop:height*.05,position:'absolute', height: height*.1, width: width*.3}}
                    onPress={() => { this.props.navigation.goBack();}} >

                    <FAIcon name='home' size={30} style={{color: '#111D28',marginLeft:width*.05,marginTop: height*.02, paddingBottom:height*.009}} />
                </TouchableOpacity>




    <Text style={styles.navyheading}>
  Contact numbers</Text>


            </View>

        )
    }


}

const styles = StyleSheet.create({
    greybody:{
        color: '#5A5A5A',
        fontFamily: 'Rubik-Regular',
        fontSize: 14,
        textAlign:'center',
        marginLeft:width*.1,
        marginRight:width*.1,
        marginBottom:height*.15

    },
    container: {
        shadowColor: '#000000',
        shadowOpacity: 0.4,
        shadowOffset: { height: -5, width: -5 },
        shadowRadius: 10,
        backgroundColor: 'white',
        width: '100%',
        height: 'auto'
    },

    ARbutton:{
            height:height*.07,
            width:width*.3,
            borderRadius: 15,
            position:'absolute',
            marginTop:height*.75,
            marginLeft:width*.175,
            backgroundColor:'#00AFD1',
            paddingTop:height*.01,

    },
    ARbutton3d:{
            height:height*.07,
            width:width*.3,
            borderRadius: 15,
            position:'absolute',
            marginTop:height*.75,
            marginLeft:width*.480,
            backgroundColor:'#00AFD1',
            paddingTop:height*.01
    },

    navBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: height*.1,
        alignItems: 'center',
        position: 'relative',
      //  paddingHorizontal: 25,
        //backgroundColor: '#4086c0'
        backgroundColor:'transparent',
       // marginBottom: height*.04,

     paddingBottom: height*.08,


    },
    headerHolder: {
        padding: width*.1,
        flex: 1,
        backgroundColor:'white',
       // justifyContent:'center'
    },
    siteName: {
        marginTop: 30,
        width: 250
    },
    logo: {
        color: 'white',
        marginTop: 10
    },
    stretch: {
        flex:2,
        height: undefined,
        width: undefined,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    baseText: {
        fontFamily: 'Snell Roundhand',
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 30,
        color:'white',
        position: 'absolute',
        left: 100,
        paddingBottom:20,
        },

        borderline:{
            marginLeft: width*.05,
             marginRight: width*.05,
             borderBottomColor:'#00AFD1',
             borderBottomWidth:1},

             navyheading:{
                position:'absolute',
                color: 'white',
                textAlign:'left',
                fontSize: 18,
                fontFamily: 'Rubik-Regular',
                marginBottom:height*.01,
                marginLeft: width*.4,
                marginTop:height*.15,
              },
          navybody:{
//TOM
           // color: '#004960',
            //textAlign:'left',
           // fontSize: 14,
            //fontFamily: 'Rubik-Regular',
           // marginBottom:height*.15,

          },
          emerView:{
              marginTop: height*.03,
              marginRight: width*.1,
              marginLeft:width*0.1,
              flexDirection: 'row',
              borderBottomColor:'#C9C9C9',
              borderBottomWidth:1
          },
          emergencyNames:{
            color:'#031E2F',
            fontSize:16,
            fontFamily:'Rubik-Medium',


          },

          phoneNumbers:{
           // textDecorationLine :'underline',

            textAlign:'right',
            fontSize: 16,
            flex: 1,
            color:'#00B0CA',
            fontFamily:'Rubik-Medium'

                      },


          button1:{
            backgroundColor:'#00AFD1',
           //backgroundColor:('#003359'),
           shadowOffset:{  width: 0,  height: 2},
           shadowColor: 'rgba(0,0,0,0.16)',
           shadowOpacity: 1.0,
           shadowRadius:6,
           elevation: 5,
            //fontSize: 100,
            width: Dimensions.get('window').width*.8,
            height: Dimensions.get('window').width/6,
            alignSelf:'center',
            justifyContent: 'center',
           // borderRadius: 7,
            position:'relative'

            //flex: 1,
            //resizeMode: Image.resizeMode.contain,
        },
        buttonText:{

            color: 'white',
            fontFamily: 'Rubik-Medium',
            fontSize: 16,
            textAlign:'center',


        },

})
