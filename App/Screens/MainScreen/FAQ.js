'use strict'
import React, {Component} from 'react'

import { StyleSheet, View, TouchableOpacity, Image, Text } from 'react-native'

import FAIcon from 'react-native-vector-icons/FontAwesome'
import Container from '../../Component/Container'

export default class FAQScreen extends Component {

    render() {
        return (
            <Container sytle = {[styles.container, this.props.style || {}]}>
                { this.renderNavBar() }
                { this.renderHeader() }
            </Container>
        )
    }

    renderNavBar() {
        return (
            <View style = {styles.navBar}>
                <TouchableOpacity onPress={ this.props.onMenuPress} >
                <FAIcon name = 'bars' size={22} style = {{color: '#fff'}} />
                </TouchableOpacity>
            </View>
        )
    }
    renderHeader() {
        return (
            <View style={ styles.headerHolder }>
               {/*} <Image
                    style={styles.stretch}
                    source = {require('./Sunwater_app_FAQ.jpg')}>
        </Image>*/}default
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        shadowColor: '#000000',
        shadowOpacity: 0.4,
        shadowOffset: { height: -5, width:-5},
        shadowRadius: 10,

    },
    navBar: {
        height: 50,
        justifyContent: 'center',
        paddingHorizontal: 25,
        backgroundColor: 'transparent'
    },
    headerHolder: {
        padding: 25,
        flex: 1,

    },
    siteName: {
        marginTop: 30,
        width: 250
    },
    logo: {
        color: 'white',
        marginTop: 10
    },
    stretch: {
        flex:2,
        height: undefined,
        width: undefined,
        position:'absolute',left:0,right:0,top:0,bottom:0
    },
})
