'use strict'
import React, {Component} from 'react'

import { StyleSheet, Linking, View, Image,TouchableOpacity, TextInput, Dimensions, Text, NativeModules } from 'react-native'
import{FormInput, Row} from 'react-native-elements'
import FAIcon from 'react-native-vector-icons/FontAwesome'
import Container from '../../Component/Container'
import AlertsScreen from '../AlertsScreen'
//import { FormInput } from '../../../../../Library/Caches/typescript/2.6/node_modules/@types/react-native-elements';
const width= Dimensions.get('window').width
const height= Dimensions.get('window').height
export default class HubScreen extends Component {
    static navigationOptions = {
        title: 'Alerts',

      gesturesEnabled : true

    };

    constructor(props) {
        super();
        this.state = {
            visible: false
          };
        }

/* <TouchableOpacity style = {styles.button1} onPress={() => {NativeModules.arbridge.start3dAR()} }>*/


    render() {
        return (

<Container style={[styles.container]}>

<Image source = {require('../Images/MenuNavigationBackground.png')} style ={{width: Dimensions.get('window').width,flex: 1}}>

                              {this.renderNavBar()}

<View>
{/*<FAIcon name='home' size={50} style={{color: '#fff',backgroundColor:'transparent',marginLeft:width*.425,marginTop:height*.04, marginBottom: height*.015}} />*/}

{/*<Text style = {{backgroundColor:'transparent',marginTop: height*.1,marginBottom: height*.13,fontSize: 27.5, width:width,textAlign:'center', fontFamily: 'Rubik-Regular',color:'white'}}>Welcome to the{'\n'}SunWater app.</Text>*/}
<View style= {{height:height*.65, marginTop:height*.215}}>
<View style={[{width:width,flexDirection: 'row',backgroundColor:'transparent'}]}>


<View style={{flex:1,backgroundColor:'transparent'}}>
    <TouchableOpacity onPress={() => {this.props.navigation.navigate('AlertsScreen');}}
        style={{

           // borderRightColor:'white',
          //  borderRightWidth:1,
            marginLeft:width*.05,
            alignItems:'center',
            justifyContent:'center',
            width:'100%',
            height:height*.175,
           // borderRadius:80,

           //backgroundColor: 'rgba(0,0,0,0.3)',
           shadowColor: '#004960',
           shadowOffset: {width: 0, height: 2},
           shadowOpacity: 0.8,
          shadowRadius: 2
        }}>


        <Image source = {require('../Icons/MenuIcon-Alerts.png')}/>

    {/*<FAIcon name='bell-o' size={30} style={{color: '#fff'}} />*/}
    <Text style={[styles.hubText,{color:'white',marginTop:width*0.05}]}>Alerts</Text>
    </TouchableOpacity>

</View>
<View style={{flex:1,backgroundColor:'transparent'}}>
    <TouchableOpacity onPress={() => {this.props.navigation.navigate('ARScreen');}}
        style={{

            alignItems:'center',
            justifyContent:'center',
            width:'100%',
            height:height*.175,
            right:width*.05,
            //borderRadius:80,
         //   borderColor:'transparent',
          //  backgroundColor: 'rgba(0,0,0,0.3)',
            shadowColor: '#004960',
            shadowOffset: {width: 0, height: 2},
            shadowOpacity: 0.8,
            shadowRadius: 2
            }}>
                <Image source = {require('../Icons/MenuIcon-Directory.png')}/>
    {/*<FAIcon name='phone' size={30} style={{color: '#fff'}} />*/}
    <Text style={[styles.hubText,{color:'white',marginTop:width*0.05}]}>Directory</Text>
    </TouchableOpacity>

</View>
</View>





{/*TOM*/}


<View style={[{width:width,flexDirection: 'row',backgroundColor:'transparent'}]}>
<View style={{flex:1,backgroundColor:'transparent'}}>
    <TouchableOpacity onPress={() => {this.props.navigation.navigate('AR_annotationsScreen');}}
        style={{
          //  borderRightColor:'white',
          //  borderRightWidth:1,
          //  borderTopColor:'white',
          //  borderTopWidth:1,
            alignItems:'center',
            justifyContent:'center',
            width:'100%',
            height:height*.175,
            marginLeft:width*.05,
           // borderRadius:80,
           borderColor:'transparent',
         //  backgroundColor: 'rgba(0,0,0,0.3)',
            shadowColor: '#004960',
            shadowOffset: {width: 0, height: 2},
            shadowOpacity: 0.8,
            shadowRadius: 2
            }}>
             <Image source = {require('../Icons/MenuIcon_DamLevels.png')}/>
   {/*} <FAIcon name='line-chart' size={30} style={{color: '#fff'}} />*/}
    <Text style={[styles.hubText,{color:'white',marginTop:width*0.05}]}>Dam levels</Text>
    </TouchableOpacity>

</View>
<View style={{flex:1,backgroundColor:'transparent'}}>
    <TouchableOpacity onPress={() => {this.props.navigation.navigate('FeedbackScreen');}}
        style={{
            //borderTopColor:'white',
            //borderTopWidth:1,
            shadowColor: '#004960',
            shadowOffset: {width: 0, height: 2},
            shadowOpacity: 0.8,
            shadowRadius: 2,
            alignItems:'center',
            justifyContent:'center',
            width:'100%',
            height:height*.175,
            right:width*.05,
            //borderRadius:80,
            //backgroundColor: 'rgba(0,0,0,0.3)',
            }}>
             <Image source = {require('../Icons/MenuIcon-HaveYourSay.png')}/>
    {/*<FAIcon name='comments' size={30} style={{color: '#fff',marginTop:height*.03}} />*/}
    <Text style={[styles.hubText,{color:'white',marginTop:width*0.05, textAlign:'center'}]}> Have your say</Text>
    </TouchableOpacity>

</View>
</View>

<View style={[{width:width,flexDirection: 'row',backgroundColor:'transparent'}]}>
<View style={{flex:1,backgroundColor:'transparent'}}>
    <TouchableOpacity onPress={() => {this.props.navigation.navigate('AboutUsScreen');}}
        style={{
          //  borderRightColor:'white',
            //borderRightWidth:1,
            //borderTopColor:'white',
            //borderTopWidth:1,
            alignItems:'center',
            justifyContent:'center',
            width:'100%',
            height:height*.25,
           // borderRadius:80,
           marginLeft:width*.05,
           //backgroundColor: 'rgba(0,0,0,0.3)',
           shadowColor: '#004960',
           shadowOffset: {width: 0, height: 2},
           shadowOpacity: 0.8,
           shadowRadius: 2}}>
     <View style = {{marginBottom: height*.05}}>
     <Image source = {require('../Icons/MenuIcon-About.png')}/>
    {/*<FAIcon name='users' size={30} style={{color: '#fff', marginLeft: width*.025}} />*/}
    <Text style={[styles.hubText,{color:'white',marginLeft: width*.025,marginTop:height*0.02}]}>About</Text>
    </View>
    </TouchableOpacity>

</View>
<View style={{flex:1,backgroundColor:'transparent'}}>
    <TouchableOpacity onPress={() => {this.props.onItemSelected('AlertsScreen',{key:false});}}

        style={{
            //borderTopColor:'white',
            //borderTopWidth:1,
            alignItems:'center',
            justifyContent:'center',
            width:'100%',
            height:height*.25,
            //borderRadius:80,
            borderColor:'transparent',
            //backgroundColor: 'rgba(0,0,0,0.3)',
            shadowColor: '#004960',
            shadowOffset: {width: 0, height: 2},
            shadowOpacity: 0.8,
            shadowRadius: 2,
            right:width*.05

            }}>
                 <View style = {{marginBottom: height*.05}}>
                 <Image source = {require('../Icons/MenuIcon-Settings.png')}/>
    {/*<FAIcon name='cog' size={30} style={{color: '#fff', marginLeft: width*.045}} />*/}
    <Text style={[styles.hubText,{color:'white',marginTop:height*0.02}]}>Settings</Text>
    </View>
    </TouchableOpacity>

</View>
</View>
</View>



</View>
</Image>

               </Container>

        )
    }


    renderNavBar() {

        return (
            <View style={styles.navBar}>
 {/*BARS*/}
 <Image source = {require('./HeaderBar.png')} style = {{marginLeft: width*.12}}/>

             <TouchableOpacity
                    style={{ flexDirection: 'row', alignItems: 'center',marginTop:height*.05,position:'absolute', height: height*.1, width: width*.3}}
                    onPress={() => { this.props.onMenuPress();}} >
                    <FAIcon name='bars' size={25} style={{color: '#fff',marginLeft:width*.05,marginTop: height*.02, paddingBottom:height*.009}} />
                </TouchableOpacity>
                <Image source={require('./SunWater_Logo_New.png')} style = {{position:'absolute', marginLeft:width*.35,marginTop: height*.05,height:height*.325, width:width*.325, resizeMode:'contain'}}/>


</View>


        )
    }


}

const styles = StyleSheet.create({
    back: {
        position: 'absolute',
        top:0,
        right: 0,
        bottom: 0,
        left: 0,
        width: width,
        backgroundColor: '#004960'
    },



    greybody:{
        color: '#5A5A5A',
        fontFamily: 'Rubik-Regular',
        fontSize: 14,
        textAlign:'center',
        marginLeft:width*.1,
        marginRight:width*.1,
        marginBottom:height*.15

    },
    container: {
        shadowColor: '#000000',
        shadowOpacity: 0.4,
        shadowOffset: { height: -5, width: -5 },
        shadowRadius: 10,
        backgroundColor: 'white',
        width: '100%',
        height: 'auto'
    },

    ARbutton:{
            height:height*.07,
            width:width*.3,
            borderRadius: 15,
            position:'absolute',
            marginTop:height*.75,
            marginLeft:width*.175,
            backgroundColor:'#00AFD1',
            paddingTop:height*.01,

    },
    ARbutton3d:{
            height:height*.07,
            width:width*.3,
            borderRadius: 15,
            position:'absolute',
            marginTop:height*.75,
            marginLeft:width*.480,
            backgroundColor:'#00AFD1',
            paddingTop:height*.01
    },

    navBar: {
        flexDirection: 'row',
        //justifyContent: 'space-between',
        height: height*.1,
        alignItems: 'center',
      //  paddingHorizontal: 25,
        backgroundColor: 'transparent',
       // backgroundColor:'white',
        marginBottom: height*.04,
    },
    headerHolder: {
        padding: width*.1,
        flex: 1,
        backgroundColor:'white',
       // justifyContent:'center'
    },
    siteName: {
        marginTop: 30,
        width: 250
    },
    logo: {
        color: 'white',
        marginTop: 10
    },
    stretch: {
        flex:2,
        height: undefined,
        width: undefined,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    baseText: {
        fontFamily: 'Snell Roundhand',
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 30,
        color:'white',
        position: 'absolute',
        left: 100,
        paddingBottom:20,
        },

        borderline:{
            marginLeft: width*.05,
             marginRight: width*.05,
             borderBottomColor:'#EEAF00',
             borderBottomWidth:1},

             hubText:{

                color: '#004960',
                textAlign:'left',
                fontSize: 14,
                fontFamily: 'Rubik-Regular',
                marginBottom:height*.01,
               // marginTop:height*.15,
              },

        navyheading:{

            color: '#004960',
            textAlign:'left',
            fontSize: 18,
            fontFamily: 'Rubik-Regular',
            marginBottom:height*.01,
           // marginTop:height*.15,
          },
          navybody:{

            color: '#004960',
            //textAlign:'left',
            fontSize: 14,
            fontFamily: 'Rubik-Regular',
           // marginBottom:height*.15,

          },

        button1:{
            backgroundColor:'white',

           shadowOffset:{  width: 0,  height: 2},
           shadowColor: 'rgba(0,0,0,0.16)',
           shadowOpacity: 1.0,
           shadowRadius:6,
            //fontSize: 100,
            width: Dimensions.get('window').width/2,
            height: Dimensions.get('window').width/6,
            alignSelf:'center',
            justifyContent: 'center',
            borderRadius: 7,
            position:'relative'

            //flex: 1,
            //resizeMode: Image.resizeMode.contain
        },
        buttonText:{

            color: '#5A5A5A',
            fontFamily: 'Rubik-Regular',
            fontSize: 16,
            textAlign:'center',


        },

})
