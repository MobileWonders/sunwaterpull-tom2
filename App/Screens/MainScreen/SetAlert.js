'use strict'
import React, {Component} from 'react'

import { StyleSheet, View, TouchableOpacity, WebView,Image,Text, Dimensions,TouchableHighlight } from 'react-native'

import FAIcon from 'react-native-vector-icons/FontAwesome'
import Container from '../../Component/Container'
import SetAlert from '../../Screens/MainScreen/SetAlert';

export default class AlertSettings extends Component {

    render() {
        return (
            <Container style = {[styles.container, this.props.style || {}]}>
                { this.renderNavBar() }
               <View style ={{ position:'absolute', width: Dimensions.get('window').width, height: Dimensions.get('window').height  }}>
                <WebView
          //ref={WEBVIEW_REF}
          automaticallyAdjustContentInsets={false}
         // style={styles.webView}
         // source={{uri: this.state.url}}
         // source ={{uri:'https://www.google.com'}}
          //source ={{uri:'http://sunwateraware.community/'}}
          source ={{uri:'http://www.sunwater.com.au/__data/win/reports/win_storages.htm'}}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          decelerationRate="normal"
          scrollEnabled ={false}
          onNavigationStateChange={this.onNavigationStateChange}
          onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest}
          startInLoadingState={true}
          scalesPageToFit={true}


        //  scalesPageToFit={this.state.scalesPageToFit}
        />
</View>
            </Container>
        )
    }

    renderNavBar() {
        return (
            <View style = {styles.navBar}>

                <TouchableOpacity onPress={ this.props.onMenuPress} >
                    <FAIcon name = 'bars' size={22} style = {{color: '#fff'}} />
                </TouchableOpacity>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        shadowColor: '#000000',
        shadowOpacity: 0.4,
        shadowOffset: { height: -5, width:-5},
        shadowRadius: 10,
    },
    navBar: {
        height: 50,
        justifyContent: 'center',
        paddingHorizontal: 25,
           backgroundColor: '#00AFD1',
           zIndex:1
    },
    headerHolder: {
        padding: 25,
        flex: 1
    },
    siteName: {
        marginTop: 30,
        width: 250
    },
    logo: {
        color: 'white',
        marginTop: 10
    },
    stretch: {
        flex:2,
        height: undefined,
        width: undefined,
        position:'absolute',left:0,right:0,top:0,bottom:0
    },
    buttonclick:{
        marginLeft: 90,
        marginTop: 50,
        width: 190,
        height: 190,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    webView:{
        position:'relative',
        flex:1


    }

})
