'use strict'
import React, {Component} from 'react'

import { StyleSheet, View, Dimensions, Linking,TouchableOpacity, ActivityIndicator, WebView, Image,Text, ScrollView } from 'react-native'

import FAIcon from 'react-native-vector-icons/FontAwesome'
import Container from '../../Component/Container'

const width = Dimensions.get("window").width
const height = Dimensions.get("window").height
const buttonname ='Return to Hub'

export default class TNC extends Component {
    constructor(props) {
        super();
        this.state = {
          visible: false
        };
      }

      componentDidMount() {

      }

            /*
            <Image source = {require('./backgrounddark.png')} style ={styles.background}/>



<Image style ={styles.navBar}>
{ this.renderNavBar() }
</Image>

<View style = {{position:'relative',width:width, height:height}}>

<Image source = {require('./logo.png')}style = {{height: height/15, marginTop: height*.05,marginBottom: height*.05,alignSelf: 'center', width: width /2,position: 'relative',resizeMode:'contain'}}/>


{
<Text style={{backgroundColor:'transparent',color: 'white', textAlign:'center', fontSize: 18, fontFamily: 'Rubik-Regular', marginBottom:height*.03}}>
   Terms of Use</Text>
   <Text style ={{backgroundColor:'transparent',fontSize:12, color:'white',paddingLeft:width*.05,justifyContent:'center', marginBottom: height*.05,paddingRight:width*.05}}>
   Please read the SunWater Terms of Use carefully before using this notification service application. By accessing services through this notification service application, you agree to observe these Terms and Conditions. If you do not accept all of these Terms and Conditions then please stop using this notification service application immediately</Text>

 <TouchableOpacity style={{backgroundColor:'transparent'}}onPress={() => Linking.openURL('http://www.sunwater.com.au/home/terms-of-use')}>

<Text  style = {{backgroundColor:'transparent',color: '#7BDAED', textAlign:'center',marginBottom: height*.05}} >Click here to see a more detailed Terms of Use</Text>
</TouchableOpacity>
 }

<Text style={{backgroundColor:'transparent',color: 'white', textAlign:'center', fontSize: 18, fontFamily: 'Rubik-Regular', marginBottom:height*.03}}>
   Terms of Use</Text>
   <Text style ={{backgroundColor:'transparent',fontSize:12, color:'white',paddingLeft:width*.05,justifyContent:'center', marginBottom: height*.05,paddingRight:width*.05}}>
   Please read the SunWater Terms of Use carefully before using this application. By accessing services through this application, you agree to observe these Terms of Use. If you do not accept all of these Terms of Use then please stop using this application immediately.
</Text>

<TouchableOpacity onPress={() => Linking.openURL('http://www.sunwater.com.au/home/terms-of-use')}>
<Text  style = {{backgroundColor:'transparent',color: '#7BDAED', textAlign:'center',marginBottom: height*.05}} >   SunWater Terms of Use</Text>
</TouchableOpacity>



                    <TouchableOpacity style ={{alignSelf:'center', justifyContent:'center',borderRadius:10,width:width*.6, height:height*.08, backgroundColor:'#004960'}}onPress={() => this.props.navigation('AlertsScreen')} >

                <Text style = {{backgroundColor:'transparent',fontSize: 18,textAlign:'center', fontFamily: 'Rubik-Regular',color:"#00AFD1"}}>Return to Alerts
                    </Text>

                    </TouchableOpacity>

        </View>
        */
    render() {


        return (



            <Container style = {[styles.container, this.props.style || {}]}>
            <View style = {{flex: 1, backgroundColor:'white'}}>
            {this.renderNavBar()}

            <View style = {{alignContent:'center',}}>

   <Text style ={styles.navybody}>
   Please read the SunWater Terms of Use carefully before using this application.{'\n'}{'\n'}
   By accessing services through this application, you agree to observe these Terms of Use.{'\n'}{'\n'}
   If you do not accept all of these Terms of Use then please stop using this application immediately.

</Text>

<View style = {{marginTop: height*.35}}>

 <TouchableOpacity style = {styles.button1} onPress={() => Linking.openURL('http://www.sunwater.com.au/home/terms-of-use')}>

                 <Text  style = {styles.buttonText} >Read more</Text>
                    </TouchableOpacity>

</View>



{/*
    <TouchableOpacity style ={[styles.button1,{marginTop:height*0.03}]} onPress={() => this.props.navigation('HubScreen')} >


                <Text style = {styles.buttonText}>{buttonname}
                    </Text>

                    </TouchableOpacity>

        */}
                </View>
                </View>
    </Container>


        )
    }
    renderNavBar() {
        const { navigation } = this.props
        return (
          <View style={styles.navBar}>
          {/*BARS*/}

          <Image source = {require('./HeaderBar.png')} style = {{marginTop: height*.075,marginLeft: width*.12}}/>

          <TouchableOpacity
                    style={{ flexDirection: 'row', alignItems: 'center',marginTop:height*.05,position:'absolute', height: height*.1, width: width*.3}}
                    onPress={() => { this.props.navigation.navigate('HubScreen');}} >
                    <FAIcon name='home' size={30} style={{color: '#111D28',marginLeft:width*.05,marginTop: height*.02, paddingBottom:height*.009}} />
                </TouchableOpacity>




    <Text style={styles.navyheading}>
   Terms of use</Text>


            </View>

        )
    }

}

const styles = StyleSheet.create({
      background:{
        flex:1,
       position:'absolute',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,

         },
header:{
    backgroundColor:'#003F54',
    position: 'relative',
    zIndex:1,
},

container: {
    shadowColor: '#000000',
    shadowOpacity: 0.4,
    shadowOffset: { height: -5, width: -5 },
    shadowRadius: 10,

},

    scroll: {
        backgroundColor:'transparent',
        position: 'relative',
        zIndex:1,
       //marginLeft: width*.1,
         height:height*.6,
         //marginRight: width*.1,
         width: width,
         flex:1,

         // marginBottom:height/15,
         // borderRadius:10
    },

    stretch: {
        flex:2,
        height: undefined,
        width: undefined,
        position:'absolute',left:0,right:0,top:0,bottom:0
    },

    heading:{
        textAlign:'center',
        fontSize:22,
        color:'#7BDAED',
        fontFamily: 'Rubik-Regular',

        backgroundColor:'transparent'

    },
    heading2:{ //please read terms etc
        textAlign:'left',
        fontSize:14,
        lineHeight: 21,
        color: '#7BDAED',

        fontFamily: 'Rubik-Regular'
    },
    heading3:{
        textAlign:'left',
        fontSize:18,
        color: '#7BDAED',
        fontFamily: 'Rubik-Regular',
        paddingBottom:height*.02,

    },
    heading4:{
        textAlign:'left',
        fontSize:16,
        color: '#7BDAED',
        fontFamily: 'Rubik-Regular',
        paddingBottom:height*.02,
        fontStyle: 'italic'

    },
    body:{
        textAlign:'left',
        fontSize:14,
        color:'#7BDAED',
        marginBottom:20,
        fontFamily: 'Rubik-Regular'
    },
    disclaimer:{
        textAlign:'left',
        fontSize: 14,
        color:'#7BDAED',
        fontFamily: 'Rubik-Regular'
    },
    navBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: height*.1,
        alignItems: 'center',
        position: 'relative',
      //  paddingHorizontal: 25,
        //backgroundColor: '#4086c0'
        backgroundColor:'transparent',
       // marginBottom: height*.04,

     paddingBottom: height*.08,


    },
    navyheading:{
        position:'absolute',
        color: 'white',
        textAlign:'left',
        fontSize: 18,
        fontFamily: 'Rubik-Regular',
        marginLeft:width*.375,

    },

    navybody:{

        color: '#5F5F5F',
        textAlign:'left',
        fontSize: 16,
        fontFamily: 'Rubik-Regular',
       // marginBottom:height*.15,
        marginLeft:width*.1,
        marginRight:width*.1,
        marginTop:height*.1
    },

    modalHeading:{

        color: '#004960',
        textAlign:'left',
        fontSize: 16,
        fontFamily: 'Rubik-Regular',
        marginBottom:height*.05,
        marginLeft:width*.1,
        marginRight:width*.1,
        marginTop:height*.05,
        left: width*.02
    },
    greybody:{
        color: '#5A5A5A',
        fontFamily: 'Rubik-Regular',
        fontSize: 14,
        textAlign:'center',
        marginLeft:width*.1,
        marginRight:width*.1,
        marginBottom:height*.15

    },


    greybody1:{
        color: '#5A5A5A',
        fontFamily: 'Rubik-Regular',
        fontSize: 14,
        right: width*.1,
        //textAlign:'center',
        //marginLeft:width*.1,
        //marginRight:width*.1,
        marginBottom:height*.05,
        marginTop:height*.1

    },

    borderline:{
        marginLeft: width*.1,
         marginRight: width*.1,
         borderBottomColor:'#00AFD1',
         borderBottomWidth:1
    },


    logo:{

        width: Dimensions.get('window').width/2,
        height: Dimensions.get('window').width/6,
        marginTop: Dimensions.get('window').height/4.5,
        marginLeft: Dimensions.get('window').width/4,
        alignItems:'center',
        justifyContent: 'center',
        position:'absolute',

            color: 'white',


    },


    buttonModal:{
        backgroundColor:'white',
       //backgroundColor:('#003359'),
       shadowOffset:{  width: 0,  height: 2},
       shadowColor: 'rgba(0,0,0,0.16)',
       shadowOpacity: 1.0,
       shadowRadius:6,
        //fontSize: 100,
        width: Dimensions.get('window').width*.28,
        height: Dimensions.get('window').height*.075,
       // alignSelf:'center',
        justifyContent: 'center',
        borderRadius: 7,
        position:'relative',
        marginTop: height*0.02,
        marginBottom: height*0.02,

        //flex: 1,
        //resizeMode: Image.resizeMode.contain,
    },
wave:{
        position:'absolute',
        marginTop: height*.78,
        alignSelf:'center',
        width: width*1.1,


},
button1:{
    backgroundColor:'#00AFD1',
   //backgroundColor:('#003359'),
   shadowOffset:{  width: 0,  height: 2},
   shadowColor: 'rgba(0,0,0,0.16)',
   shadowOpacity: 1.0,
   shadowRadius:6,
   elevation: 5,
    //fontSize: 100,
    width: Dimensions.get('window').width*.8,
    height: Dimensions.get('window').width/6,
    alignSelf:'center',
    justifyContent: 'center',
   // borderRadius: 7,
    position:'relative'

    //flex: 1,
    //resizeMode: Image.resizeMode.contain,
},
buttonText:{

    color: 'white',
    fontFamily: 'Rubik-Medium',
    fontSize: 16,
    textAlign:'center',


},

})
/*
    background:{
            flex:1,
            alignItems:'stretch',
            height:'auto',
            width: Dimensions.get('window').width,

    },

*/




