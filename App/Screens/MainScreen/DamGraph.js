'use strict'
import React, {Component} from 'react'

import { StyleSheet, Linking, View, Image,TouchableOpacity, TextInput, FlatList, RefreshControl, Dimensions, Text, NativeModules, ScrollView } from 'react-native'
import{FormInput, Row} from 'react-native-elements'
import FAIcon from 'react-native-vector-icons/FontAwesome'
import Container from '../../Component/Container'
import AlertsScreen from '../../Screens/AlertsScreen';
//import PercentageCircle from 'react-native-percentage-circle';
import { ArtTest } from "react-native-art-wave"

//import { FormInput } from '../../../../../Library/Caches/typescript/2.6/node_modules/@types/react-native-elements';
const width= Dimensions.get('window').width
const height= Dimensions.get('window').height
const _styles = StyleSheet.create({
    container: {
        flex: 1,
        marginVertical: 10,
        marginHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: StyleSheet.hairlineWidth,
    },
    wave: {
        width: 100,
        aspectRatio: 1,
        overflow: 'hidden',
        backgroundColor: 'white',
    },
    waveBall: {
        width: 100,
        aspectRatio: 1,
        borderRadius: 50,
        overflow: 'hidden',
    }
});
export default class DamGraphScreen extends Component {
    constructor(props) {
        super(props);
        this.initialState = {
            visible: false,
            level: 0,
            name: "unavailable",
            date: "unavailable",
            bga: "unavailable",
            bgaDate: "unavailable",

          };

          console.log(this.props.navigation.state.params);

          if ( this.props.navigation.state.params !== undefined) {

            //this.setState({ level: parseFloat(this.props.navigation.state.params.value.key)});
            let level = this.props.navigation.state.params.value.key;
            this.initialState.level = parseFloat(level);
            console.log(level)
            //this.setState({ name :this.props.navigation.state.params.value.name});
            let name = this.props.navigation.state.params.value.name;
            this.initialState.name = name;

            //this.setState({ date :this.props.navigation.state.params.value.date});
            let date = this.props.navigation.state.params.value.date;
            this.initialState.date = date;


            let time = this.props.navigation.state.params.value.date;
            let times = 0;

            const words = time.split(' ');
            let test  = words[1];
            let test2 = test.split(':');
            const hours = test2[0];
            const minutes = test2[1];
            const seconds = test2[2];
            let ampm = "";
            console.log(hours)

            if(hours == 12){
                times = hours
                ampm = "pm"

            }

            else if (hours > 12){
                times = hours - 12
                ampm = "pm"

            }else{
                times = hours
                ampm = "am"
            }

            this.initialState.hours = times;
            this.initialState.minutes = minutes;
            this.initialState.seconds = seconds;
            this.initialState.ampm = ampm;
            //this.setState({ bga:this.props.navigation.state.params.value.bga});
            let bga = this.props.navigation.state.params.value.bga;
            this.initialState.bga = bga;

            //this.setState({ bgaDate :this.props.navigation.state.params.value.bgaDate});
            let bgaDate = this.props.navigation.state.params.value.bgaDate;
            this.initialState.bgaDate = bgaDate;



          }
          else{
            this.initialState.level= 0;

          }

          this.state = this.initialState;

        }



  componentWillMount() {

    /*
            axios.get('https://sunwaterapp.com.au/sunwaterjson/index.php?sunwatercipher=sunwaterAR')
            .then(response =>this.setState({data:response.data}));*/
}

/* <TouchableOpacity style = {styles.button1} onPress={() => {NativeModules.arbridge.start3dAR()} }>

top: height*(this.initialState.level/100)*/



render() {

          let marginNumber = height - (height*(this.initialState.level/100))
        let waterlevel = this.initialState.level/100;
        console.log(waterlevel);
    return (
<Container style={[styles.container]}>

<ArtTest
       type={"rectangular"}
       proportion={waterlevel}
       surfaceWidth = {width}
       surfaceHeigth ={height}
       superViewBackgroundColor={'transparent'}
       style = {{backgroundColor:'rgba(116,204,244,0.5)'}}/>



  {/*}      <View style = {{backgroundColor:'rgba(51,255,255,0.3)',  height: height, marginTop:marginNumber}}/>*/}

    <View style = {{backgroundColor:'transparent', position:'absolute'}}>


                          <View style = {{marginLeft: width*.08, marginRight: width*.08 }}>
<Text style = {styles.navyheading}>{this.state.name}</Text>
{this.renderNavBar()}
</View>

 <View style={{alignContent:'center',justifyContent:'center',width:width,marginTop: height*.175}}>
    {/*<PercentageCircle borderWidth = {30} radius={100} percent={this.state.level} color={"#004960"}></PercentageCircle>*/}
<TouchableOpacity onPress ={() => Linking.openURL('http://www.sunwater.com.au/__data/win/reports/win_storages.htm')}><Text style = {{textAlign: 'center',fontSize:60}}>{this.state.level}%</Text></TouchableOpacity>
</View>

<View style = {{bottom: height*.035, right: width*.0075}}>
      <Text style = {[styles.buttonText,{fontWeight:'bold',textAlign:'center', fontSize:20}]}>{'\n'} Capacity</Text>
      <Text style = {{ marginLeft:width*.3}}> {this.state.hours}:{this.state.minutes}{/*:{this.state.seconds}*/} {this.state.ampm}, {this.state.bgaDate}
      </Text>
</View>

  <Text style={{alignContent:'center',justifyContent:'center',textAlign:'center', fontSize: 20, fontFamily: 'Rubik-Regular', width:width, marginTop: height*.25, color: '#111028'}}>Algae Status:</Text>


  <TouchableOpacity  onPress={() => Linking.openURL('http://www.sunwater.com.au/home/blue-green-algae-level')}>
  <Text style = {{fontSize:40,color:"#111028", textAlign:'center'}}> {this.state.bga}</Text>
  </TouchableOpacity>
</View>



           </Container>

    )
}


    renderNavBar() {
        const { navigation } = this.props
        return (
            <View style={styles.navBar}>
 {/*BARS*/}
             <TouchableOpacity
                    style={{position:'absolute', flexDirection: 'row', alignItems: 'center', height: height*.1, bottom: height*.05}}
                    onPress={() => {this.props.navigation.goBack()}}>
                    <FAIcon name='arrow-left' size={26} style={{color: '#111028'}} />
                </TouchableOpacity>

              {/*} <Text style ={{color:'white', fontFamily:'Rubik-Regular', fontSize:24, marginRight:width*.175,paddingTop:height*0.035}}>Augmented Reality</Text>*/}



</View>


        )
    }


}


const styles = StyleSheet.create({
    greybody:{
        color: '#5A5A5A',
        fontFamily: 'Rubik-Regular',
        fontSize: 14,
        textAlign:'center',
        marginLeft:width*.1,
        marginRight:width*.1,
        marginBottom:height*.15

    },
    container: {
        shadowColor: '#000000',
        shadowOpacity: 0.4,
        shadowOffset: { height: -5, width: -5 },
        shadowRadius: 10,
        backgroundColor: 'white',
        width: '100%',
        height: '100%'
    },

    ARbutton:{
            height:height*.07,
            width:width*.3,
            borderRadius: 15,
            position:'absolute',
            marginTop:height*.75,
            marginLeft:width*.175,
            backgroundColor:'#00AFD1',
            paddingTop:height*.01,

    },
    ARbutton3d:{
            height:height*.07,
            width:width*.3,
            borderRadius: 15,
            position:'absolute',
            marginTop:height*.75,
            marginLeft:width*.480,
            backgroundColor:'#00AFD1',
            paddingTop:height*.01
    },

    navBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 50,
        alignItems: 'center',
      //  paddingHorizontal: 25,
        //backgroundColor: '#4086c0'
        backgroundColor:'transparent',
        marginBottom: height*.04,
    },
    headerHolder: {
        padding: width*.1,
        flex: 1,
        backgroundColor:'white',
       // justifyContent:'center'
    },
    siteName: {
        marginTop: 30,
        width: 250
    },
    logo: {
        color: 'white',
        marginTop: 10
    },
    stretch: {
        flex:2,
        height: undefined,
        width: undefined,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    baseText: {
        fontFamily: 'Snell Roundhand',
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 30,
        color:'white',
        position: 'absolute',
        left: 100,
        paddingBottom:20,
        },


        borderline:{
            marginLeft: width*.05,
             marginRight: width*.05,
             borderBottomColor:'#00AFD1',
             borderBottomWidth:1,


            },

        navyheading:{

            color: '#111028',
            textAlign:'center',
            fontSize: 25,
            fontFamily: 'Rubik-Regular',
           // marginBottom:height*.01,
           marginTop:height*.05,
          },
          navybody:{

            color: '#111028',
            //textAlign:'left',
            fontSize: 14,
            fontFamily: 'Rubik-Regular',
           // marginBottom:height*.15,

          },

        button1:{
            backgroundColor:'white',

           shadowOffset:{  width: 0,  height: 2},
           shadowColor: 'rgba(0,0,0,0.16)',
           shadowOpacity: 1.0,
           shadowRadius:6,
            //fontSize: 100,
            width: Dimensions.get('window').width/2,
            height: Dimensions.get('window').width/6,
            alignSelf:'center',
            justifyContent: 'center',
            borderRadius: 7,
            position:'relative'

            //flex: 1,
            //resizeMode: Image.resizeMode.contain
        },
        buttonText:{

            color: '#111028',
            fontFamily: 'Rubik-Regular',
            fontSize: 13,
            textAlign:'center',


        },

})
