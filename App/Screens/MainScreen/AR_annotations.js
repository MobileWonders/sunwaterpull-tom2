'use strict'
import React, {Component} from 'react'

import { StyleSheet, Linking, View, Image,TouchableOpacity, TextInput, FlatList, RefreshControl, Dimensions, Text, NativeModules, ScrollView } from 'react-native'
import{FormInput, Row} from 'react-native-elements'
import FAIcon from 'react-native-vector-icons/FontAwesome'
import Container from '../../Component/Container'
import AlertsScreen from '../../Screens/AlertsScreen';


//import { FormInput } from '../../../../../Library/Caches/typescript/2.6/node_modules/@types/react-native-elements';
const width= Dimensions.get('window').width
const height= Dimensions.get('window').height

export default class AR_annotationsScreen extends Component {
    constructor(props) {
        super();
        this.state = {
            visible: false,
            data: null
          };
        }

        fetchJSON(){
            this.setState({data:null});
            fetch("https://sunwaterapp.com.au/sunwaterjson/index.php?sunwatercipher=sunwaterAR")
            .then((response) =>
                response.json()
             // console.log(response);
        //      console.log(JSON.parse(response));

            )
            .then((responseJson) => {
                console.log(responseJson)
                //return  <Text>{responseJson}</Text>;
                if(responseJson.hasOwnProperty('results')){
                    var test = responseJson.results;//.results[0].name;
                    this.setState({data:test});
                }
                else{
                    this.setState({data:"Could not retrieve data, check your internet connection."});
                }
            })
            .catch(error => {
                this.setState({data:"Could not retrieve data, check your internet connection."});
                console.error(error);
                //return <Text>{error}</Text>;
            });
        }

  componentDidMount() {
      this.fetchJSON();
    /*
            axios.get('https://sunwaterapp.com.au/sunwaterjson/index.php?sunwatercipher=sunwaterAR')
            .then(response =>this.setState({data:response.data}));*/
}

/* <TouchableOpacity style = {styles.button1} onPress={() => {NativeModules.arbridge.start3dAR()} }>*/



render() {

console.log('test')

const { onItemSelected } = this.props;

    return (
<Container style={[styles.container]}>

                          {this.renderNavBar()}





<View style = {{backgroundColor:'white'}}>
<View style = {{marginLeft: width*.08, marginRight: width*.08}}>
<Text style = {styles.navyheading}>Dam levels</Text>
</View>

{(this.state.data == null) ?
 <FlatList
 refreshControl={
   <RefreshControl
     refreshing={true}
     tintColor={"#00AFD1"}/>
 }
 style={{height:height*0.45,zIndex:10}}
 />
 : (this.state.data === "Could not retrieve data, check your internet connection.") ?
 <View>
 <View style={[{width:width*0.8,marginLeft:width*0.1,marginTop:height*0.3,flexDirection: 'row',textAlign:'center'}]}>
 <Text style={{textAlign:'center'}}>{this.state.data}</Text>
 </View>
  <TouchableOpacity onPress={() => this.fetchJSON()} style = {[styles.button1,{marginTop:height*0.03}]} title="Submit">
  <Text style ={styles.buttonText} >Refresh</Text>
</TouchableOpacity>
</View>
 :
  <View>
      <View style = {{flexDirection : "row",marginTop:width*0.1,marginBottom:height*0.02}}>
<Text style={[styles.navybody,{flexGrow:1,width:width*0.475,textAlign:'left',paddingLeft:width*0.05}]}>Dam Name</Text><Text style = {[styles.navybody,{textAlign:'center',flexGrow: 1,width:width*0.175}]}>Capacity</Text><Text style = {[styles.navybody,{textAlign:'center',flexGrow: 1,width:width*0.325}]}>Last Updated</Text>
</View>
 <View style={{height:height*.675,borderWidth: 1, borderColor: '#a5a5a5'}}>

<ScrollView style = {{width: width,marginTop:height*0.015}}>

    {this.state.data.map((item) => {
        if (item.name.trim() != "Glenlyon Dam") {
            var d = item.date.replace(/\//g, "-");
            console.log(d);
            var d_trim = d.split(" ")[0];
            var t_trim = d.split(" ")[1];
            var day = d_trim.split("-")[2];
            var month = d_trim.split("-")[1];
            var month_ver = '';
            var day_ver = '';

            switch (month){
                case '01' : month_ver = "Jan"; break;
                case '02' : month_ver = "Feb"; break;
                case '03' : month_ver = "Mar"; break;
                case '04' : month_ver = "Apr"; break;
                case '05' : month_ver = "May"; break;
                case '06' : month_ver = "Jun"; break;
                case '07' : month_ver = "Jul"; break;
                case '08' : month_ver = "Aug"; break;
                case '09' : month_ver = "Sep"; break;
                case '10' : month_ver = "Oct"; break;
                case '11' : month_ver = "Nov"; break;
                case '12' : month_ver = "Dec"; break;
                default: month_ver = ""; break;
            }
            switch (day.charAt(1)){
                case '1' : day_ver = "st"; break;
                case '2' : day_ver = "nd"; break;
                case '3' : day_ver = "rd"; break;
                default: day_ver = "th"; break;
            }
            if(day.charAt(0) === "0"){
                day = day.charAt(1);
            }
            return (
            <View style={{
                shadowColor: '#9a9a9a',
           shadowOffset: {width: 0, height: 1},
           shadowOpacity: 0.8,
           shadowRadius: 1,
            marginBottom:4,marginLeft:width*0.025,width:width*0.95}}>
                <TouchableOpacity  onPress={() => onItemSelected('DamGraphScreen',{key:item.vicinity.split("%")[0],name:item.name,date:item.date,bga:item.algaeStatus,bgaDate:item.algaeDate})} style = {{flexDirection: 'row', width:'100%',/*borderColor:'#004960',borderWidth:1*/}}>
                <View style={{paddingTop:height*0.02,paddingBottom:height*0.02,marginLeft:width*0.02,flexDirection: 'row',width:width*0.92}}>
   <Text style={{flexGrow:1,width:width*0.4,backgroundColor:'transparent'}}>{item.name} :       </Text><Text style = {[styles.navybody,{textAlign:'left',flexGrow: 1,width:width*0.15,backgroundColor:'transparent'}]}>{item.vicinity}</Text><Text style = {[{textAlign:'center',flexGrow: 1,width:width*0.15,backgroundColor:'transparent'}]}>{day+day_ver} {month_ver}</Text><FAIcon name='arrow-circle-o-right' size={18} style = {[{textAlign:'left',flexGrow: 1,width:width*0.015,color: '#004960',backgroundColor:'transparent'}]}/>
    </View>
    </TouchableOpacity>
     </View>
            );

        }

    })}

</ScrollView>
</View>
<View>
<Text style={[styles.navybody,{width:width,textAlign:'right',paddingRight:width*0.05, marginTop: height*.006}]}>(Scroll for more)</Text>
</View>
</View>
}



{/*<View>
<TouchableOpacity style ={[styles.button1]} onPress={() => {this.props.navigation.goBack()}} >
<Text style = {styles.buttonText}>Return to Hub
</Text>
</TouchableOpacity>
</View>
*/}

</View>


           </Container>

    )
}


renderNavBar() {
    const { navigation } = this.props
    return (
      <View style={styles.navBar}>
      {/*BARS*/}

      <Image source = {require('./HeaderBar.png')} style = {{marginTop: height*.075,marginLeft: width*.12}}/>

      <TouchableOpacity
                style={{ flexDirection: 'row', alignItems: 'center',marginTop:height*.05,position:'absolute', height: height*.1, width: width*.3}}
                onPress={() => { this.props.navigation.goBack();}} >

                <FAIcon name='home' size={30} style={{color: '#111D28',marginLeft:width*.05,marginTop: height*.02, paddingBottom:height*.009}} />
            </TouchableOpacity>




<Text style={styles.navyheading}>
Dam levels</Text>


        </View>

    )
}


}


const styles = StyleSheet.create({
    greybody:{
        color: '#5A5A5A',
        fontFamily: 'Rubik-Regular',
        fontSize: 14,
        textAlign:'center',
        marginLeft:width*.1,
        marginRight:width*.1,
        marginBottom:height*.15

    },
    container: {
        shadowColor: '#000000',
        shadowOpacity: 0.4,
        shadowOffset: { height: -5, width: -5 },
        shadowRadius: 10,
        backgroundColor: 'white',
        width: '100%',
        height: 'auto'
    },

    ARbutton:{
            height:height*.07,
            width:width*.3,
            borderRadius: 15,
            position:'absolute',
            marginTop:height*.75,
            marginLeft:width*.175,
            backgroundColor:'#00AFD1',
            paddingTop:height*.01,

    },
    ARbutton3d:{
            height:height*.07,
            width:width*.3,
            borderRadius: 15,
            position:'absolute',
            marginTop:height*.75,
            marginLeft:width*.480,
            backgroundColor:'#00AFD1',
            paddingTop:height*.01
    },

    navBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: height*.1,
        alignItems: 'center',
        position: 'relative',
      //  paddingHorizontal: 25,
        //backgroundColor: '#4086c0'
        backgroundColor:'transparent',
       // marginBottom: height*.04,

     paddingBottom: height*.08,


    },
    headerHolder: {
        padding: width*.1,
        flex: 1,
        backgroundColor:'white',
       // justifyContent:'center'
    },
    siteName: {
        marginTop: 30,
        width: 250
    },
    logo: {
        color: 'white',
        marginTop: 10
    },
    stretch: {
        flex:2,
        height: undefined,
        width: undefined,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    baseText: {
        fontFamily: 'Snell Roundhand',
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 30,
        color:'white',
        position: 'absolute',
        left: 100,
        paddingBottom:20,
        },

        borderline:{
            marginLeft: width*.05,
             marginRight: width*.05,
             borderBottomColor:'#00AFD1',
             borderBottomWidth:1},

             navyheading:{
                position:'absolute',
                color: 'white',
                textAlign:'left',
                fontSize: 18,
                fontFamily: 'Rubik-Regular',
                marginBottom:height*.01,
                marginLeft: width*.4,
                marginTop:height*.15,
              },
          navybody:{

            color: '#004960',
            //textAlign:'left',
            fontSize: 14,
            fontFamily: 'Rubik-Regular',
           // marginBottom:height*.15,

          },

        button1:{
            backgroundColor:'white',

           shadowOffset:{  width: 0,  height: 2},
           shadowColor: 'rgba(0,0,0,0.16)',
           shadowOpacity: 1.0,
           shadowRadius:6,
            //fontSize: 100,
            width: Dimensions.get('window').width/2,
            height: Dimensions.get('window').width/6,
            alignSelf:'center',
            justifyContent: 'center',
            borderRadius: 7,
            position:'relative'

            //flex: 1,
            //resizeMode: Image.resizeMode.contain
        },
        buttonText:{

            color: '#5A5A5A',
            fontFamily: 'Rubik-Regular',
            fontSize: 16,
            textAlign:'center',


        },

})
