import React, {Component} from 'react'
//import AlertsScreen from '../Screens/AlertsScreen'
import {
    StyleSheet, View, TouchableOpacity, Share, Dimensions,WebView, Text, Image, Linking,AsyncStorage
} from 'react-native'

import FAIcon from 'react-native-vector-icons/FontAwesome'


import DeviceInfo from 'react-native-device-info'

//import Alerts from '../Screens/MainScreen/Alerts'
import { StackNavigator } from "react-navigation"
import PropTypes from 'prop-types';


let SideMenuWidth = 300




export default class Menu extends Component {

/*
    setSettings(bool){
//        alert('clicked');
        const { onItemSelected } = this.props;
        AsyncStorage.setItem('MENU_KEY' ,bool.toString());
        onItemSelected('AlertsScreen');
    }
*/

    render() {

        const model = DeviceInfo.getModel();


       var iPhone6s = model.indexOf("iPhone 6s");
       var iPhone6splus = model.indexOf("iPhone 6s Plus");
       var iPhone7 = model.indexOf("iPhone 7");
       var iPhone7plus = model.indexOf("iPhone 7 Plus");
       var iPhone8 = model.indexOf("iPhone 8");
       var iPhone8plus = model.indexOf("iPhone 8 Plus");
       var iPhoneX = model.indexOf("iPhone X");
       var iPhoneSE = model.indexOf("iPhone SE");

  var hide =true;
        if((iPhone6s == 0)||(iPhone6splus == 0)||(iPhone7 == 0)||(iPhone7plus == 0)||(iPhone8 == 0)||(iPhone8plus == 0)||(iPhoneSE == 0)||(iPhoneX == 0))
        {
            hide = false;
        }
        else{
            hide = true;
        }

        const { onItemSelected } = this.props
        const width = Dimensions.get("window").width
const height = Dimensions.get("window").height
        return (
            <View style = {[styles.sideMenu, this.props.style || {}]}>
                <View style={{ paddingHorizontal: 25 }}>
                    { this._renderHeader() }

                    {/*<TouchableOpacity style = {styles.menu} onPress={() => onItemSelected('HubScreen',{key:true})}>
                        <FAIcon name = 'home' color = 'white' size = {24} />
                        <Text style = {{fontFamily:'Rubik-Regular',color: '#fff', fontSize: 18,marginLeft:16}}>Hub home</Text>
                    </TouchableOpacity>*/}


                  {/*this.setSettings(false)*/}
                 {/*}   <TouchableOpacity style = {styles.menu} onPress={() => onItemSelected('AlertsScreen',{key:true})}>
                        <FAIcon name = 'bell-o' color = 'white' size = {24} />
                        <Text style = {{fontFamily:'Rubik-Regular',color: '#fff', fontSize: 18,marginLeft:16}}>Alerts home</Text>
                    </TouchableOpacity>
                */}

                    {/*<TouchableOpacity style = {styles.menu} onPress={() => onItemSelected('AlertsScreen',{key:false})}>
                        <FAIcon name = 'cog' color = 'white' size = {24} />
                        <Text style = {{fontFamily:'Rubik-Regular',color: '#fff', fontSize: 18,marginLeft: 18}}>Alert settings</Text>
                    </TouchableOpacity>
            */ }

                      {/*<TouchableOpacity style = {styles.menu} onPress={() => onItemSelected('AR_annotationsScreen')}>
                <FAIcon name = 'area-chart' color = 'white' size = {22} />
                <Text style = {{fontFamily:'Rubik-Regular',color: '#fff', fontSize: 18,marginLeft: 16}}>Dam Levels</Text>
                </TouchableOpacity>*/}

                    <TouchableOpacity style = {styles.menu} onPress={() => this.share()}>
                        <FAIcon name = 'share' color = 'white' size = {24} />
                        <Text style = {{fontFamily:'Rubik-Regular',color: '#fff', fontSize: 18,marginLeft: 16}}>Share this app</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style = {styles.menu} onPress={() => onItemSelected('SirensScreen')}>
                        <FAIcon name = 'comments-o' color = 'white' size = {24} />
                        <Text style = {{fontFamily:'Rubik-Regular',color: '#fff', fontSize: 18,marginLeft: 16}}>Sirens</Text>
                    </TouchableOpacity>


            {/*} <TouchableOpacity style = {styles.menu} onPress={() => onItemSelected('SirensScreen')}>
                        <FAIcon name = 'book' color = 'white' size = {24} />
                        <Text style = {{fontFamily:'Rubik-Regular',color: '#fff', fontSize: 18,marginLeft: 17}}>Sirens</Text>
            </TouchableOpacity>*/}


                {/*<TouchableOpacity style = {styles.menu} onPress={() => onItemSelected('ARScreen')}>
                <FAIcon name = 'address-book-o' color = 'white' size = {24} />
                <Text style = {{fontFamily:'Rubik-Regular',color: '#fff', fontSize: 18,marginLeft: 18}}>Contact Numbers</Text>
                </TouchableOpacity>*/}


 {/*<TouchableOpacity style = {styles.menu} onPress={() => onItemSelected('AboutUsScreen')}>
                <FAIcon name = 'users' color = 'white' size = {24} />
                <Text style = {{fontFamily:'Rubik-Regular',color: '#fff', fontSize: 18,marginLeft: 18}}>About Us</Text>
            </TouchableOpacity>*/}

                                 {/*      <TouchableOpacity style = {styles.menu} onPress={() => Linking.openURL('mailto:thom.murphy1981@gmail.com?subject=Feedback&body=Template message here')}>
                        <FAIcon name = 'home' color = 'white' size = {24} />
                        <Text style = {{fontFamily:'Rubik-Regular',color: '#fff', fontSize: 18,marginLeft: 16}}>Feedback</Text>
                    </TouchableOpacity>
 <TouchableOpacity style = {styles.menu} onPress={() => this.Alerts.onShowSettings()} >*/}
                   { /*onItemSelected('AlertsScreen'),
                    this.onShowSettings()
                     this.setState({isOpened: true})


                        <FAIcon name = 'info' color = 'white' size = {24} />
                        <Text style = {{color: '#fff', fontSize: 16,marginLeft: 20}}>Set your Alerts</Text>
                    </TouchableOpacity>
                    {/*<TouchableOpacity style = {styles.menu} onPress={() => Linking.openURL('https://online.sunwater.com.au/Login')}>
                    <TouchableOpacity style = {styles.menu} onPress={() => onItemSelected('OnlineScreen')}>
                        <FAIcon name = 'comment-o' color = 'white' size = {24} />
                        <Text style = {{color: '#fff', fontSize: 16,marginLeft: 20}}>Customer Orders</Text>
                    </TouchableOpacity>

                {/*   <TouchableOpacity style = {styles.menu} onPress={() => onItemSelected('AboutScreen')}>
                        <FAIcon name = 'share' color = 'white' size = {24} />
                        <Text style = {{color: '#fff', fontSize: 16,marginLeft: 20}}>About SunWater</Text>
        </TouchableOpacity>*/}

               {/*
                  <TouchableOpacity style = {styles.menu} onPress={() => Linking.openURL('http://sunwateraware.community/#!Map/Search/TopLevel/Barrage%7CDam%7CRainfall%7CRiverLevel%7CWeir/')}>
                        <FAIcon name = 'user-o' color = 'white' size = {24} />
                        <Text style = {{color: '#fff', fontSize: 16,marginLeft: 20}}>SunWater Aware</Text>
                    </TouchableOpacity>





                    <TouchableOpacity style = {styles.menu} onPress={() => onItemSelected('LoginScreen')}>
                        <FAIcon name = 'home' color = 'white' size = {24} />
                        <Text style = {{color: '#fff', fontSize: 18,marginLeft: 20}}>Login</Text>
                    </TouchableOpacity>*/}
                   {/*} <TouchableOpacity style = {styles.menu} onPress={() => onItemSelected('FAQScreen')}>*/}
                    {/*<TouchableOpacity style = {styles.menu}  onPress={() => Linking.openURL('http://www.sunwater.com.au/home/frequently-asked-questions')}>

                        <FAIcon name = 'info-circle' color = 'white' size = {24} style = {{marginLeft:2}} />
                        <Text style = {{fontFamily:'Rubik-Regular',color: '#fff', fontSize: 18, marginLeft:17}}>SunWater FAQs</Text>
                    </TouchableOpacity>*/}
                    <TouchableOpacity style = {styles.menu} onPress={() => onItemSelected('TNCScreen')}>
                        <FAIcon name = 'book' color = 'white' size = {24} />
                        <Text style = {{fontFamily:'Rubik-Regular',color: '#fff', fontSize: 18,marginLeft: 17}}>Terms of use</Text>
                    </TouchableOpacity>

                                        <TouchableOpacity style = {styles.menu} onPress={() => onItemSelected('TutorialScreen')}>
                        <FAIcon name = 'leanpub' color = 'white' size = {24} />
                        <Text style = {{fontFamily:'Rubik-Regular',color: '#fff', fontSize: 18,marginLeft: 17}}>Tutorial</Text>
                    </TouchableOpacity>




                </View>

{/*
For SunWater related questions{'\n'}contact us on:{'\n'}{'\n'}
  13 15 89{'\n'}
{'\n'}
Important emergency numbers:{'\n'}
{'\n'}
Flood Storm Emergency{'\n'}{'\n'}
Queensland SES : {'\n'}132 500{'\n'}
{'\n'}

For life threatening Emergency:{'\n'}   000
*/}
            </View>

        )
    }

    share(){

                Share.share({
                    message: 'I found this app and thought you might be interested! SunWater alerts you about dam water levels, when a dam in your area is spilling and contact information for when you’re in an emergency.',
                    url: 'http://www.sunwater.com.au/',
                    title: 'Look at SunWater\'s new app!',
                    subject: 'Look at SunWater\'s new app!'
                  })
            }
    _renderHeader() {
        const { onItemSelected } = this.props
        return (

    <View style={ styles.header }>

                <View style={ styles.userInfosHolder }>


<TouchableOpacity style = {styles.menu} onPress={() => onItemSelected('TestScreen',{key:true})}>
                    <Image style={ styles.avatar }
                    resizeMode='contain'
                    source={require('../Screens/MainScreen/SunWater_Logo_New.png')}></Image>
                    </TouchableOpacity>
                    <View style={ styles.userInfos }>

                    </View>
                </View>
            </View>

        )
    }

}

const styles = StyleSheet.create({
    sideMenu: {
        position: 'absolute',
        top:0,
        right: 0,
        bottom: 0,
        left: 0,
        width: SideMenuWidth,
        backgroundColor: '#031E2F'
    },
    header: {
        marginTop: 20,
        marginBottom: 20,

    },
    userInfosHolder: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    menu: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    avatar: {
        position: 'relative',
        width: '80%',
        height: 50,

        //borderRadius: 30
    }
})
