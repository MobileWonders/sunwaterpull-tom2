import React, { Component } from 'react';
import { StackNavigator, addNavigationHelpers } from 'react-navigation';


import AlertsScreen from '../Screens/AlertsScreen';
import SunWaterAware from '../Screens/SunWaterAware';
import OnlineScreen from '../Screens/OnlineScreen';
import ShareAppScreen from '../Screens/ShareAppScreen';
import AboutScreen from '../Screens/AboutScreen';
import FAQScreen from '../Screens/FAQScreen';
import TestScreen from '../Screens/TestScreen';
import AlertSettings from '../Screens/AlertSettings';
import ARScreen from '../Screens/ARScreen';
import AboutUsScreen from '../Screens/AboutUsScreen';
import HubScreen from '../Screens/HubScreen';
import AR_annotationsScreen from '../Screens/AR_annotationsScreen';
import Details from '../Screens/Details';
import TNCScreen from '../Screens/TNCScreen';
import TutorialScreen from '../Screens/TutorialScreen';
import SirensScreen from '../Screens/SirensScreen';
import FeedbackScreen from '../Screens/FeedbackScreen';
import DamGraphScreen from '../Screens/DamGraphScreen';

const AppNavigator = StackNavigator({
    TestScreen: { screen: TestScreen, navigationOptions: {gesturesEnabled: false} },
    AlertsScreen: { screen: AlertsScreen},
    SunWaterAware: { screen: SunWaterAware },
    OnlineScreen: { screen: OnlineScreen},
    ShareAppScreen: { screen: ShareAppScreen },
    AboutScreen: { screen: AboutScreen },
    FAQScreen: { screen: FAQScreen },
    FeedbackScreen: { screen: FeedbackScreen },
    TutorialScreen: { screen: TutorialScreen },
    /*
    AlertSettings: {
        screen: AlertsScreen, //opened:true

        navigationOptions: ({navigation}) => ({
            isOpened: false,
      }),},*/
      DamGraphScreen: {screen : DamGraphScreen},
      AboutUsScreen: {screen : AboutUsScreen},
      HubScreen: {screen : HubScreen,navigationOptions: {gesturesEnabled: false} },
    ARScreen: {screen : ARScreen},
    AR_annotationsScreen: {screen : AR_annotationsScreen},
    Details: {screen: Details},
    TNCScreen: {screen: TNCScreen},
    SirensScreen: {screen: SirensScreen},
});

export default class AppWithNavigationState extends Component {
    render() {
        return (
            <AppNavigator />
        );
    }



}
