import React, {Component} from 'react'

import {
    StyleSheet, View, TouchableOpacity, Text, Image
} from 'react-native'

import FAIcon from 'react-native-vector-icons/FontAwesome'

let PushMenuWidth = 300

export default class simpleToggleButton extends Component {

    render() {

        return (

           <View style = {[styles.sideMenu, this.props.style || {}]}>
                <View style={{ paddingHorizontal: 30 }}>
                    { this._renderHeader() }
                    <Button>All locations</Button>
                    <Button>By regions</Button>
                    <TouchableOpacity
                    onPress={()=>this._onPress()}
                    style = {styles.NQButton}>
              <Text style = {{color:textColor, textAlign:'center'}} >{NQTextValue}</Text>
          </TouchableOpacity>
          <TouchableOpacity
                    onPress={()=>this._onPress()}
                    style = {styles.CQButton}>
              <Text style = {{color:textColor, textAlign:'center'}} >{CQTextValue}</Text>
          </TouchableOpacity>
          <TouchableOpacity
                    onPress={()=>this._onPress()}
                    style = {styles.SQButton}>
              <Text style = {{color:textColor, textAlign:'center'}} >{SQTextValue}</Text>
          </TouchableOpacity>
                    <Button>By water Supply Scheme</Button>
                    <Switch
        onValueChange={this.handlerToggleSwitch}
        value = {this.state.switchValue}/><Text>Push Notifications</Text>

        <Switch
        onValueChange={this.handlerToggleSwitch}
        value = {this.state.switchValue}/><Text>Email Notifications</Text>
                </View>
            </View>

)


    }

    _renderHeader() {
        const { onItemSelected } = this.props
        return (
            <View style={ styles.header }>
                <View style={ styles.userInfosHolder }>

                    </View>
                </View>

        )
    }
}

const styles = StyleSheet.create({
    sideMenu: {
        position: 'absolute',
        top:0,
        right: 0,
        bottom: 0,
        left: 300,
        width: PushMenuWidth,
        backgroundColor: 'transparent'
    },
    header: {
        marginTop: 20,
        marginBottom: 20,

    },
    userInfosHolder: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    menu: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    avatar: {
        width: 60,
        height: 60,
        borderRadius: 30
    },
    NQButton:{
        marginLeft: 90,
        backgroundColor:buttonBG,
        flex:1,
        top: 200,
        height: 60,
        width:200,
        position: 'absolute',
        justifyContent: 'center',
        borderColor: 'dodgerblue',
        borderWidth: 2,
    },
    CQButton:{
        marginLeft: 90,
        backgroundColor:buttonBG,
        flex:1,
        top: 300,
        height: 60,
        width:200,
        position: 'absolute',
        justifyContent: 'center',
        borderColor: 'dodgerblue',
        borderWidth: 2,
    },
    SQButton:{
        marginLeft: 90,
        backgroundColor:buttonBG,
        flex:1,
        top: 400,
        height: 60,
        width:200,
        position: 'absolute',
        justifyContent: 'center',
        borderColor: 'dodgerblue',
        borderWidth: 2,
    },

})
