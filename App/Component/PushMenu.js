import React, {Component} from 'react'

import {
    StyleSheet, View, Switch, TouchableOpacity, Text, Image, Button
} from 'react-native'

import FAIcon from 'react-native-vector-icons/FontAwesome'

let PushMenuWidth = 300

export default class pushMenu extends Component {

    state ={
        toggle: false
    }
    _onPress(){
        const newState = !this.state.toggle;
        this.setState({toggle:newState})
        console.log('test');
    }

    render() {
        const { onItemSelected } = this.props
        const {toggle} = this.state;
        const NQTextValue = toggle?"North Queensland Selected":"North Queensland";
        const CQTextValue = toggle?"Central Queensland Selected":"Central Queensland";
        const SQTextValue = toggle?"South Queensland Selected":"South Queensland";
        const textColor = toggle?"white":"dodgerblue";
        return (

           <View style = {[styles.sideMenu, this.props.style || {}]}>
                <View style={{ paddingHorizontal: 30 }}>
                    { this._renderHeader() }
                    <Button title = "All locations"></Button>
                    <Button title = "By regions"></Button>
                    <TouchableOpacity
                    onPress={()=>this._onPress()}
                    style = {toggle?styles.NQButton1:styles.NQButton2}>
              <Text style = {{color:textColor, textAlign:'center'}} >{NQTextValue}</Text>
          </TouchableOpacity>
          <TouchableOpacity
                    onPress={()=>this._onPress()}
                    style = {styles.CQButton}>
              <Text style = {{color:textColor, textAlign:'center'}} >{CQTextValue}</Text>
          </TouchableOpacity>
          <TouchableOpacity
                    onPress={()=>this._onPress()}
                    style = {styles.SQButton}>
              <Text style = {{color:textColor, textAlign:'center'}} >{SQTextValue}</Text>
          </TouchableOpacity>
                    <Button title="By water Supply Scheme"></Button>
                    <Switch
        onValueChange={this.handlerToggleSwitch}
        value = {this.state.switchValue}/><Text>Push Notifications</Text>

        <Switch
        onValueChange={this.handlerToggleSwitch}
        value = {this.state.switchValue}/><Text>Email Notifications</Text>
                </View>
            </View>

)


    }

    _renderHeader() {
        const { onItemSelected } = this.props
        return (
            <View style={ styles.header }>
                <View style={ styles.userInfosHolder }>

                    </View>
                </View>

        )
    }
}

const styles = StyleSheet.create({
    sideMenu: {
        top:0,
        right: 0,
        bottom: 0,
        left: 300,
        width: PushMenuWidth
    },
    header: {
        marginTop: 20,
        marginBottom: 20,

    },
    userInfosHolder: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    menu: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    avatar: {
        width: 60,
        height: 60,
        borderRadius: 30
    },
    NQButton1:{
        marginLeft: 90,
        backgroundColor:'dodgerblue',
        flex:1,
        top: 200,
        height: 60,
        width:200,
        position: 'absolute',
        justifyContent: 'center',
        borderColor: 'dodgerblue',
        borderWidth: 2,
    },
    NQButton2:{
        marginLeft: 90,
        backgroundColor:'white',
        flex:1,
        top: 200,
        height: 60,
        width:200,
        position: 'absolute',
        justifyContent: 'center',
        borderColor: 'dodgerblue',
        borderWidth: 2,
    },
    CQButton1:{
        marginLeft: 90,
        backgroundColor:'dodgerblue',
        flex:1,
        top: 300,
        height: 60,
        width:200,
        position: 'absolute',
        justifyContent: 'center',
        borderColor: 'dodgerblue',
        borderWidth: 2,
    },
    CQButton2:{
        marginLeft: 90,
        backgroundColor:'white',
        flex:1,
        top: 300,
        height: 60,
        width:200,
        position: 'absolute',
        justifyContent: 'center',
        borderColor: 'dodgerblue',
        borderWidth: 2,
    },
    SQButton1:{
        marginLeft: 90,
        backgroundColor:'dodgerblue',
        flex:1,
        top: 400,
        height: 60,
        width:200,
        position: 'absolute',
        justifyContent: 'center',
        borderColor: 'dodgerblue',
        borderWidth: 2,
    },
    SQButton2:{
        marginLeft: 90,
        backgroundColor:'white',
        flex:1,
        top: 400,
        height: 60,
        width:200,
        position: 'absolute',
        justifyContent: 'center',
        borderColor: 'dodgerblue',
        borderWidth: 2,
    },

})

