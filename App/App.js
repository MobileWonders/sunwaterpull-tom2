'use strict'
import React, { Component } from 'react'
import { AppRegistry } from 'react-native'
import OneSignal from 'react-native-onesignal'
import { Platform } from 'react-native';


import AppWithNavigationState from './Component/Navigation';
import * as firebase from 'firebase';

firebase.initializeApp({
    apiKey: "AIzaSyBdgf6a6tgDON_doT1_kmnq2T0Xa8qYMyA",
    authDomain: "sunwater-38a15.firebaseapp.com",
    databaseURL: "https://sunwater-38a15.firebaseio.com/",
    //storageBucket: "projName-d0c3e.appspot.com"
});

class App extends Component {
    render() {
        return <AppWithNavigationState />
    }

    componentWillMount() {
        OneSignal.addEventListener('opened', this.onOpened);
        //OneSignal.addEventListener('registered', this.onRegistered);
        OneSignal.addEventListener('ids', this.onIds);
    }

    componentWillUnmount() {
        OneSignal.removeEventListener('opened', this.onOpened);
      //  OneSignal.removeEventListener('registered', this.onRegistered);
        OneSignal.removeEventListener('ids', this.onIds);
    }

    onOpened = (openResult) => {

    };
    onRegistered = (notifData) => {

    };
    onIds = (device) => {
        console.log('Device Info', device);
        //alert(JSON.stringify(device))
    };
}



export default class PushNotificationController extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    FCM.requestPermissions();

    FCM.getFCMToken().then(token => {
      console.log("TOKEN (getFCMToken)", token);

    });

    FCM.getInitialNotification().then(notif => {
      console.log("INITIAL NOTIFICATION", notif)
    });

    this.notificationListner = FCM.on(FCMEvent.Notification, notif => {
      console.log("Notification", notif);
      if(notif.local_notification){
        return;
      }
      if(notif.opened_from_tray){
        return;
      }

      if(Platform.OS ==='ios'){
              //optional
              //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see the above documentation link.
              //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
              //notif._notificationType is available for iOS platfrom
              switch(notif._notificationType){
                case NotificationType.Remote:
                  notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
                  break;
                case NotificationType.NotificationResponse:
                  notif.finish();
                  break;
                case NotificationType.WillPresent:
                  notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
                  break;
              }
            }
      this.showLocalNotification(notif); //TODO
    });

    this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken, token => {
      console.log("TOKEN (refreshUnsubscribe)", token);
    });
  }

  showLocalNotification(notif) {
    FCM.presentLocalNotification({
      title: notif.title,
      body: notif.body,
      priority: "high",
      click_action: notif.click_action,
      show_in_foreground: true,
      local: true
    });
  }

  componentWillUnmount() {
    this.notificationListner.remove();
    this.refreshTokenListener.remove();
  }


  render() {
    return null;
  }
}

AppRegistry.registerComponent('sideMenu', () => App);
